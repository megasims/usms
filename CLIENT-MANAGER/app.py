import time
import socket
import sys, os
import logging
import threading
from logging.handlers import RotatingFileHandler

from lib.core import Socket
from lib.core import Client
from lib.core import SerialHandler
from lib.core import ClientHandler
from lib.decorator import Decorator

class App:
	clients = []
	def __init__(self, myID, manager):
		self.id = myID
		self.shStatus = False
		self.manager = manager

		log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
		CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
		sys.path.append(os.path.dirname(CURRENT_DIR))
		logFile = CURRENT_DIR + '/log/log.txt'
		log_handler = RotatingFileHandler(logFile, mode='a', maxBytes=5*1024, backupCount=2, encoding=None, delay=0)
		log_handler.setFormatter(log_formatter)
		log_handler.setLevel(logging.DEBUG)
		self.log = logging.getLogger('root')
		self.log.setLevel(logging.DEBUG)
		self.log.addHandler(log_handler)

	def handleSerial(self):
		if not self.shStatus:
			self.shStatus = True
			for i in range(2):
				self.sh.onGreen()
				time.sleep(0.3)
				self.sh.offGreen()
				time.sleep(0.3)
			self.shStatus = False


	def serialHandler(self):
		serialHandlerThread = threading.Thread(target = self.handleSerial)
		serialHandlerThread.start()
	
	def start(self):
		#try:
			decorator = Decorator(self.log)
			if not decorator.decorate():
				sys.stderr.write('Decorator Error!\n')
				self.log.error('Decorator Error!\n')
				return
			manager = Socket(self.manager, self.log)
			cfgString = manager.get("CLIENTMANAGER\tGETSTARTUP\t" + str(self.id))
			if cfgString == None:
				sys.stderr.write('Server is not working!\n')
				self.log.error('Server is not working!\n')
				return
			if not self.readConfig(cfgString):
				sys.stderr.write("Can't recognize configuration!\n")
				#self.log("Can't recognize configuration!\n")
				return
			self.clientHandler = ClientHandler(self.clients, manager, self.log)
			self.sh = SerialHandler('/dev/ttyUSB12', self.log)
			self.run()
		#except:
		#	sys.stderr.write("Can't start App")
		#	return

	def readConfig(self, cfgString):
		#try:
			print (cfgString)
			cfgString = str(cfgString)
			arr = cfgString.split("\n")
			for i in range(len(arr)):
				arr[i] = arr[i].strip()

			temp = arr[0].split("\t")
			self.listen = (temp[0], int(temp[1]))
			myIP = temp[0]
			
			for i in range(1, len(arr)):
				temp = arr[i].split("\t")
				c = Client(int(temp[0]), self.manager, (myIP, int(temp[1])), temp[2], self.log)
				if c.id != None:
					self.clients.append(c)
				else:
					return False
			return True
		#except:
		#	return False

	def handleClients(self):
		while self.running:
			self.clientHandler.timeInterval()
			time.sleep(10)
		sys.stderr.write("Interval event listner is stopped!\n")
		self.running = False

	def run(self):
		try:
			decorator = Decorator(self.log)
			if not decorator.decorate():
				self.log.error('Decorator')
			self.running = True
			mainSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			mainSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
			mainSocket.bind(self.listen)
			mainSocket.listen(10)
			print ('Running...')
			
			intervalEventThread = threading.Thread(target = self.handleClients)
			intervalEventThread.start()

			for i in range(len(self.clientHandler.clients)):
				try:
					self.clientHandler.clients[i].stop()
				except:
					temp = None

			while self.running:
				conn, addr = mainSocket.accept()
				cmd = conn.recv(128).decode();
				cmd = cmd.strip()
				res = None
				print (cmd)

				if cmd == 'RESTART':
					res = "OK"
					os.system('/sbin/shutdown -r now')
					sys.stderr.write('Restart command\n');
					self.log.info('Restart command\n')
				elif cmd[:7] == 'INSTALL':
					print('Happy coding :)!')
				elif cmd[:6] == 'UPDATE':
					print('Happy coding :)!')
				elif cmd[:11] == 'STARTCLIENT':
					temp = cmd.split('\t')
					res = self.clientHandler.start(int(temp[1]), self.manager)
				elif cmd[:10] == 'STOPCLIENT':
					temp = cmd.split('\t')
					res = self.clientHandler.stop(int(temp[1]))
				elif cmd == 'ALIVE':
					#self.serialHandler()
					res = 'OK'
				else:
					res = 'UNKNOWN_COMMAND'
					self.log.error('UNKNOWN_COMMAND\n')
					sys.stderr.write('UNKNOWN_COMMAND\n')

				res = res.strip() + "\n"
				conn.send(res.encode())
				conn.close()

			mainSocket.close()
			sys.stderr.write('APP is stopped!\n')
		except:
			self.log.error('Runtime error App\n')
			sys.stderr.write('Runtime error App\n')
		