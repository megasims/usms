import sys, os
from app import App

MANAGER_ID = None
MANAGER_IP = None
MANAGER_PORT = None

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
fo = open(CURRENT_DIR + "/init/init", "r")
initData = fo.readline().strip();
initArray = initData.split('\t')
MY_ID = int(initArray[0])
MANAGER = (initArray[1], int(initArray[2]))
app = App(MY_ID, MANAGER)
app.start()
