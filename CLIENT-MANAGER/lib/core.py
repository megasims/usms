import sys, os
import socket
import importlib.machinery

class Socket:
	def __init__(self, host, log):
		self.host = host
		self.log = log

	def send(self, data):
		try:
			data = data.strip() + "\n"
			conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			conn.connect(self.host)
			conn.send(data.encode())
			conn.close()
		except:
			sys.stderr.write("Socket error-0!\n")
			self.log.error("Socker error-0!\n")
		
	def get(self, data):
		try:
			data = data.strip() + "\n"
			conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			conn.connect(self.host)
			conn.send(data.encode())
			ret = conn.recv(1024).decode().strip();
			conn.close()
			return ret
		except:
			sys.stderr.write("Socket error-1!\n")
			self.log.error("Socker error-1!\n")
			return None


class Client:
	def __init__(self, id, manager, host, appName, log):
		self.id = id
		self.host = host
		self.appName = appName
		self.running = False
		self.log = log

		#try:
		CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
		print (CURRENT_DIR)
		if os.path.exists(CURRENT_DIR + "/../clients/" + appName + "/module.py"):
			module = __import__("CLIENT-MANAGER.clients." + appName + ".module", fromlist=["MANAGER.clients"])
			print (2);
			self.app = module.Main(id, manager, host)
		else:

			self.app = None
		#except:
			#self.app = None

	def alive(self):
		try:
			conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			conn.connect(self.host)
			cmd = "ALIVE"
			conn.send(cmd.encode())
			ret = conn.recv(16).decode().strip();
			conn.close()
			if ret == 'OK':
				return True
			return False
		except:
			return False
		
	def start(self, managerHost):
		if self.running:
			return 'ALREADY_RUNNING'
		else:
			if self.app == None:
				return 'CLIENT_IS_NOT_INSTALLED'
			status = self.app.start()
			if status[0]:
				return "OK"
			else:
				return 'CLIENT_IS_NOT_STARTED'

	def stop(self):
		try:
			conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			conn.connect(self.host)
			cmd = "STOP"
			conn.send(cmd.encode())
			ret = conn.recv(64).decode().strip();
			conn.close()
			return ret
		except:
			return False

class ClientHandler:
	def __init__(self, clients, toManager, log):
		self.clients = clients
		self.log = log
		self.toManager = toManager
	
	def timeInterval(self):
		for i in range(len(self.clients)):
			alive1 = self.clients[i].running
			alive2 = self.clients[i].alive()
			if alive1 != alive2:
				if alive2:
					self.toManager.send("CLIENTMANAGER\t" + "STARTEDCLIENT\t" + str(self.clients[i].id))
				else:
					self.toManager.send("CLIENTMANAGER\t" + "STOPPEDCLIENT\t" + str(self.clients[i].id))
			self.clients[i].running = alive2

	def start(self, id, managerHost):
		ret = 'CLIENT_IS_MISSING_IN_NETSCADA_CONFIGURATION'
		for i in range(len(self.clients)):
			if self.clients[i].id == id:
				ret = self.clients[i].start(managerHost)
		return ret

	def stop(self, id):
		ret = 'CLIENT_IS_MISSING_IN_NETSCADA_CONFIGURATION'
		for i in range(len(self.clients)):
			if self.clients[i].id == id:
				if self.clients[i].running:
					ret = self.clients[i].stop()
				else:
					ret = 'ALREADY_STOPPED'
		return ret

class SerialHandler:
	def __init__(self, serialPort, log):
		self.serialPort = serialPort
		self.log = log
		self.socket = Socket(('127.0.0.1', 5123), log)

	def handle(self):
		try:
			return True
		except:
			return False

	def send(self, led, val):
		try:
			cmd = self.serialPort + "\t" + str(led) + "\t" + str(val)
			self.socket.send(cmd)
		except Exception as e:
			print (str(e))

	def onYellow(self):
		self.send(1, 1)

	def offYellow(self):
		self.send(1, 0)

	def onGreen(self):
		self.send(0, 1)

	def offGreen(self):
		self.send(0, 0)
