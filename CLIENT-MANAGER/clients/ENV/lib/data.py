import sys, os
import serial, time


class DataManager:
        def __init__(self, log):
                self.log = log
        def init(self, serialPort):
                try:
                        self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0)
                        print('Connected to: ' + serialPort)
                        return True
                except:
                        # self.log.error("XE05 Can't enable serial port\n")
                        print('Serial error')
                        return 'XE05'

# =========================== GET =======================
        def getData(self):
                # inWait() here
                self.ser.flushOutput() # serialiin read buffer-iig tseverleh
                self.ser.flushInput()
                #self.ser.write('\x00'.encode())
                val = (0).to_bytes(1, byteorder='big')
                print ("Write: ",val)
                self.ser.write(val)
                time.sleep(1)
                #print(self.ser.read(12))
                ret = []
                for i in range(1, 7):
                        try:
                                tagName = 'v3sens' + str(i)
                                tagValue = ord(self.ser.read(1).decode())
                                #print(ret.append((tagName, tagValue)))
                                ret.append((tagName, tagValue))
                        except:
                               tagName = 'v3sens' + str(i)
                               tagValue = 'ERROR_ENV_RSE'
                               ret.append((tagName, tagValue))
                print(ret)
                return ret

#test = DataManager(None)
#test.init('/dev/ttyUSB7')
#print (test.getData())
#print(test.getData())

