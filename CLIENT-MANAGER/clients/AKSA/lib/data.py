import sys, os, serial, time
from multiprocessing import Process
import struct, binascii
import string
import copy
import array
from time import sleep

class DataManager:

    def __init__(self, log):
        self.log = log
        self.trybuffer = 3
        self.autoMode = [0x0A, 0x10, 0x10, 0x08, 0x00, 0x02, 0x04, 0x8B, 0x75, 0x74, 0x8A] #
        self.stop = [0x0A, 0x10, 0x10, 0x08, 0x00, 0x02, 0x04, 0x8B, 0x74, 0x74, 0x8B] #
        self.manual = [0x0A, 0x10, 0x10, 0x08, 0x00, 0x02, 0x04, 0x8B, 0x76, 0x74, 0x89] #
        self.asaakh = [0x0A, 0x10, 0x10, 0x08, 0x00, 0x02, 0x04, 0x8B, 0x79, 0x74, 0x86] #
        self.En_Oil_Press = [0x0A, 0x03, 0x04, 0x00, 0x00, 0x01] #
        self.En_Cool_Temp = [0x0A, 0x03, 0x04, 0x01, 0x00, 0x01] #
        self.Fuel_level = [0x0A, 0x03, 0x04, 0x03, 0x00, 0x01] #
        self.Gen_Freq = [0x0A, 0x03, 0x04, 0x07, 0x00, 0x01] #
        self.Gen_inFreq = [0x0A, 0x03, 0x04, 0x23, 0x00, 0x01] #
        self.En_OpMode = [0x0A, 0x03, 0x03, 0x04, 0x00, 0x01]  #
        self.BatAlternate_volt = [0x0A, 0x03, 0x04, 0x04, 0x00, 0x01] #
        self.Bat_volt = [0x0A, 0x03, 0x04, 0x05, 0x00, 0x01] #
        self.En_rpm = [0x0A, 0x03, 0x04, 0x06, 0x00, 0x01] #

        self.genaVol = [0x0A, 0x03, 0x04, 0x08, 0x00, 0x02]
        self.genbVol = [0x0A, 0x03, 0x04, 0x0A, 0x00, 0x02]
        self.gencVol = [0x0A, 0x03, 0x04, 0x0C, 0x00, 0x02]

        self.genInaVol = [0x0A, 0x03, 0x04, 0x24, 0x00, 0x02]
        self.genInbVol = [0x0A, 0x03, 0x04, 0x26, 0x00, 0x02]
        self.genIncVol = [0x0A, 0x03, 0x04, 0x28, 0x00, 0x02]

        self.genaCur = [0x0A, 0x03, 0x04, 0x14, 0x00, 0x02]
        self.genbCur = [0x0A, 0x03, 0x04, 0x16, 0x00, 0x02]
        self.gencCur = [0x0A, 0x03, 0x04, 0x18, 0x00, 0x02]

        self.genInaCur = [0x0A, 0x03, 0x04, 0x34, 0x00, 0x02]
        self.genInbCur = [0x0A, 0x03, 0x04, 0x36, 0x00, 0x02]
        self.genIncCur = [0x0A, 0x03, 0x04, 0x38, 0x00, 0x02]

        self.genLoadaCur = [0x0A, 0x03, 0x04, 0xB4, 0x00, 0x02]
        self.genLoadbCur = [0x0A, 0x03, 0x04, 0xB6, 0x00, 0x02]
        self.genLoadcCur = [0x0A, 0x03, 0x04, 0xB8, 0x00, 0x02]

        self.motorRuntime =[0x0A, 0x03, 0x00, 0xCB, 0x00, 0x02] # 0.05 = 4 byte

        self.SETLOGMOD = [0x01, 0x06, 0x05, 0xD9, 0x00, 0x00]
        self.GETALARMS = [0x0A, 0x03, 0x9A, 0x01, 0x00, 0x19]
        self.GETDIGALARMS = [0x0A, 0x03, 0xAA, 0x05, 0x00, 0x01]

        self.getByteLen = 7
        self.setByteLen = 8

    def init(self, serialPort):
        # try:
        self.ser = serial.Serial(port=serialPort, baudrate=19200, parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS,timeout=1)
        print(self.ser.portstr)
        i = 0
        while not self.ser.isOpen():
            try:
                print("NowClosed")
                self.ser.open()
                print("NowOpened")
                break
            except:
                sleep(0.01)
            i = i + 1
            if i > 10:
                return False
        # self.ser.isOpen()
        self.startByte = b"~"
        self.stopByte = b"\r"
        return True

    def convertByteToInt(self, arr):
        result = []
        for idx in range(len(arr)):
            try:
                result.append(struct.unpack('B', arr[idx])[0])
            except:
                result.append(0)
                print("Convert error")
        return result

    def readSerial(self, timeout, len):
        ret = []
        cnt = 0
        #print("readSerial")
        while timeout > 0:
            if cnt >= len:
                return self.convertByteToInt(ret)
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    print('read byte', b)
                except Exception as ex:
                    b = 0
                    print('serial error decode')
                    continue

                ret.append(b)
                cnt += 1
            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def readSerial1(self, timeout):
        ret = []
        started = False
        #print("readSerial")
        while timeout > 0:
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    # print('read byte', b)
                except Exception as ex:
                    b = 0
                    print('serial error decode')
                    continue

                if b == self.stopByte:  #stopbyte orj irsen esehiig shalgana
                    return ret

                if started:  # herev ehelsen bol stopbyte orj irtel buh byte-uudiig avna
                    # print('append', b)
                    ret.append(b)
                    continue
                if b == self.startByte:  # startbyte orj irseniig shalgana
                    started = True
                if not started:  # ehelsen esehiig shalgana
                    continue
            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def genFRAME(self, bufData): # CRC bodoj command-iin ard zalgaj baigaa function
        result = []
        CRC = 0xFFFF
        POLYNOMIAL = 0xA001
        for item in bufData:
            CRC ^= item
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        result.extend(bufData)

        CRCLo = CRC & 0x00ff;
        CRCHi = CRC >> 8;
        result.append(CRCLo)
        result.append(CRCHi)

        print(result)

        return result

    def checkCRC(self, bufData):

        CRC = 0xFFFF
        POLYNOMIAL = 0xA001

        temp = bufData[:-2]

        for item in temp:
            CRC ^= item
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        CRCLo = CRC & 0x00ff
        CRCHi = CRC >> 8

        crcAr=[CRCLo, CRCHi]

        if crcAr==bufData[-2:] :
           return True
        else:
            return False

    def getGenaVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genaVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GenaVol', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenaVol except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getGenbVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genbVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GenbVol', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenbVol except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getGencVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.gencVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencVol', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencVol except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getGenaCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genaCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GenaCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenaCur except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getGenbCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genbCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GenbCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenbCur except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getGencCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.gencCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenInaVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genInaVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenInbVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genInbVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenIncVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genIncVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenInaCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genInaCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenInbCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genInbCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenIncCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genIncCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenLoadaCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genLoadaCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GenLoadaCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenLoadaCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getGenLoadbCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genLoadbCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256+rcv[4]*256+rcv[5]*256+rcv[6]
                res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GenLoadbCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenLoadbCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getDigitalInputAlarms(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.GETDIGALARMS)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
               # print(rcv[3],"------", rcv(4))                
                res_too_float = rcv[3]*256+rcv[4]
                #bin = int2bin(res_too_float)
                #print(bin) 
                #res_too_float = float(res_too_float)/10 # scale = 0.1
                print(('GenLoadcCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenLoadbCur except')
                trybuffer=trybuffer-1
        return 'X0E1'
    def getCoolTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_Cool_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 + rcv[4]
                print(('cool', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('cool except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getoilP(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_Oil_Press)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(10, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4] # scale = 1
                print(('oilP', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('oilP except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getfuel(self):

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Fuel_level)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                print(('fuel', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('fuel except')
                trybuffer=trybuffer-1
        return 'X0E1'

    def getfreq(self):

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Gen_Freq)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = float(res_too_float) / 10 # scale = 0.1
                print(('freq', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('freq except')
                trybuffer=trybuffer-1
        return  'X0E1'

    def getinfreq(self):

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Gen_inFreq)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = float(res_too_float) / 10 # scale = 0.1
                print(('infreq', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('freq except')
                trybuffer=trybuffer-1
        return  'X0E1'

    def getopMd(self):

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_OpMode)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)
                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUT'
                res_too_float = rcv[3]*256 + rcv[4]
                print(('engine_state', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('engine_state except')
                trybuffer=trybuffer-1
        return  'X0E1'

    def getBatAlternateVolt(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.BatAlternate_volt)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUTBA'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = float(res_too_float) / 10 # scale = 0.1
                print(('BatAltVolt', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('BatVolt except')
                trybuffer=trybuffer-1
        return  'X0E1'

    def getBatVolt(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Bat_volt)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUTBA'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = float(res_too_float) / 10 # scale = 0.1
                print(('BatVolt', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('BatVolt except')
                trybuffer=trybuffer-1
        return  'X0E1'

    def getEngineRPM(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_rpm)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                print(('EngineRPM', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('EngineRPM except')
                trybuffer=trybuffer-1
        return  'X0E1'

    def getLogMOD(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.SETLOGMOD)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(1)
                rcv = self.readSerial(5, 8)
                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if rcv==result:
                    return True
                else:
                    return False
            except:
                print('CoolDuration except')
                trybuffer=trybuffer-1
        return 'Connection Error'

    def toTimeText(self, val):
        if val<10:
            return str('0')+str(val)
        else:
            return  str(val)

    def isReturnAlart(self, value):
        if value in [2, 3, 4, 9,10]:
            return True
        else:
            return False

    def alarms(self):
        stts=[
                    "Disabled digital input",
                    "Not active alarm",
                    "Warning alarm",
                    "Shutdown alarm",
                    "Electrical trip alarm",
                    "Reserved",
                    "Reserved",
                    "Reserved",
                    "Inactive indication (no string)",
                    "Inactive indication (displayed string)",
                    "Active indication",
                    "Reserved",
                    "Reserved",
                    "Reserved",
                    "Reserved",
                    "Unimplemented alarm"
                ]
        trybuffer = self.trybuffer
        while True:
            if trybuffer <= 0:
                return 'XE05'
            try:

                res = self.genFRAME(self.GETALARMS)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', res).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.03)
                #rcv =  [10, 3, 50, 33, 31, 17, 17, 17, 17, 17, 255, 241, 255, 241, 241, 17, 31, 17, 255, 255, 255, 255, 255, 255, 255, 241, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 17, 17, 241, 255, 255, 255, 255, 255, 255, 255, 255, 255, 118, 239]#
                rcv = self.readSerial(5, 55)
                length=len(rcv)
                print(length)
                print('readfinish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'
                if not self.checkCRC(rcv):
                    print('CRC error')
                    trybuffer = trybuffer - 1

                alarmTxt = self.getAlarmTxt()
                print("Here Result")
                resultAlarm = ""
                for idx in range(50):
                    temp = rcv[idx+3]
                    n = int(temp/16)
                    if(self.isReturnAlart(n)):
                        resultAlarm=resultAlarm+alarmTxt[2*temp]+"|"

                    n = int(temp % 16)
                    if(self.isReturnAlart(n)):
                        resultAlarm=resultAlarm+alarmTxt[2*idx+1]+"|"
                return resultAlarm.strip("|")
            except:
                print('Alarm except')
                trybuffer = trybuffer - 1

    def getAlarm(self):
        return self.alarms()

    def getData(self):
        funcs = {
            #'cool': self.getCoolTemp,
            'oilP': self.getoilP,
            'engine_fuel': self.getfuel,
            'freq': self.getfreq,
            'engine_state': self.getopMd,
            'batVolt': self.getBatVolt,
	    'batAltVolt':self.getBatAlternateVolt,
            'engineRPM': self.getEngineRPM,
            'genaCur':self.getGenaCur,
            'genbCur':self.getGenbCur,
            'gencCur':self.getGencCur,
            'genaVol':self.getGenaVol,
            'genbVol':self.getGenbVol,
            'gencVol':self.getGencVol,
            'mainaVol':self.getGenInaVol,
            'mainbVol':self.getGenInbVol,
            'maincVol':self.getGenIncVol,
            'digital_in_alarm':self.getDigitalInputAlarms,
            #'genInaCur':self.getGenInaCur,
            #'genInbCur':self.getGenInbCur,
            #'genIncCur':self.getGenIncCur,
            #'genLoadaCur':self.getGenLoadaCur,
            #'genLoadbCur':self.getGenLoadbCur,
            #'genLoadcCur':self.getGenLoadcCur,
            'mainfreq': self.getinfreq,
            'alarm':self.getAlarm,
        }

        ret = [];
        for tag, func in funcs.items():
            try:
                ret.append((tag,func()))
            except:
                ret.append((tag, 'XE01'))

        print('Done')
        print(ret)
        return ret

    def setError(self, value):
        return "XE00"

    def setOnOff(self, value):
        command = ''
        try:
            val = int(value)
        except:
            return 'XE02'
        if val==5:
            command = self.asaakh
        if val==2:
            command = self.manual
        elif val==0:
            command = self.stop
        elif val==1:
            command = self.autoMode
            print(command)
        else:
            return "XE01"

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(command)
                print(result)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.3)
                rcv = self.readSerial(5, self.setByteLen)
                print(rcv[4], "||" , rcv[5])
                rcv = (rcv[4] + rcv[5])
                rcv1=format(rcv,'b')
                print(rcv1)
                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                if rcv==2:
                    return 'OK'
                else:
                    return 'Error'
            except:
                print('Set except')
                trybuffer=trybuffer-1
        return 'XE05'

    def int2bin(val):
        res=''
        while val>0:
            res += str(val&1)
            val=val>>1     # or val=val/2
        return res[::-1]   # invert the string

    def set(self, tag, value):
        funcs = {
            'engine_state': self.setOnOff
        }
        func = funcs.get(tag, self.setError)
        return func(value)

    def getAlarmTxt(self):
        alarmTxt=[
            "Emergency stop",
            "Low oil pressure",
            "High coolant temperature",
            "High oil temperature",
            "Under speed",
            "Over speed",
            "Fail to start",
            "Fail to come to rest",
            "Loss of speed sensing",
            "Generator low voltage",
            "Generator high voltage",
            "Generator low frequency",
            "Generator high frequency",
            "Generator high current",
            "Generator earth fault",
            "Generator reverse power",
            "Air flap",
            "Oil pressure sender fault",
            "Coolant temperature sender fault",
            "Oil temperature sender fault",
            "Fuel level sender fault",
            "Magnetic pickup fault",
            "Loss of AC speed signal",
            "Charge alternator failure",
            "Low battery voltage",
            "High battery voltage",
            "Low fuel level",
            "High fuel level",
            "Generator failed to close",
            "Mains failed to close",
            "Generator failed to open",
            "Mains failed to open",
            "Mains low voltage",
            "Mains high voltage",
            "Bus failed to close",
            "Bus failed to open",
            "Mains low frequency",
            "Mains high frequency",
            "Mains failed",
            "Mains phase rotation wrong",
            "Generator phase rotation wrong",
            "Maintenance due",
            "Clock not set",
            "Local LCD configuration lost",
            "Local telemetry configuration lost",
            "Control unit not calibrated",
            "Modem power fault",
            "Generator short circuit",
            "Failure to synchronise",
            "Bus live",
            "Scheduled run",
            "Bus phase rotation wrong",
            "Priority selection error",
            "Multiset communications (MSC) data error",
            "Multiset communications (MSC) ID error",
            "Multiset communications (MSC) failure",
            "Multiset communications (MSC) too few sets",
            "Multiset communications (MSC) alarms inhibited",
            "Multiset communications (MSC) old version units",
            "Mains reverse power",
            "Minimum sets not reached",
            "Insufficient capacity available",
            "Expansion input unit not calibrated",
            "Expansion input unit failure",
            "Auxiliary sender 1 low",
            "Auxiliary sender 1 high",
            "Auxiliary sender 1 fault",
            "Auxiliary sender 2 low",
            "Auxiliary sender 2 high",
            "Auxiliary sender 2 fault",
            "Auxiliary sender 3 low",
            "Auxiliary sender 3 high",
            "Auxiliary sender 3 fault",
            "Auxiliary sender 4 low",
            "Auxiliary sender 4 high",
            "Auxiliary sender 4 fault",
            "Engine control unit (ECU) link lost",
            "Engine control unit (ECU) failure",
            "Engine control unit (ECU) error",
            "Low coolant temperature",
            "Out of sync",
            "Low Oil Pressure Switch",
            "Alternative Auxiliary Mains Fail",
            "Loss of excitation",
            "Mains kW Limit",
            "Negative phase sequence",
            "Mains ROCOF",
            "Mains vector shift",
            "Mains G59 low frequency",
            "Mains G59 high frequency",
            "Mains G59 low voltage",
            "Mains G59 high voltage",
            "Mains G59 trip",
            "Generator kW Overload",
            "Engine Inlet Temperature high",
            "Bus 1 live",
            "Bus 1 phase rotation wrong",
            "Bus 2 live",
            "Bus 2 phase rotation wrong",
            "Reserved"
        ]
        return alarmTxt

#dm = DataManager(None)
#dm.init("/dev/ttyUSB0")
#print(dm.setOnOff(2))
#sleep(20)
#print(dm.setOnOff(0))
#print(dm.getopMd())
#print(dm.getData())
#print(dm.getDigitalInputAlarms())
#print(dm.getCoolTemp())

#print(dm.getGenInaVol())
#print(dm.getAlarm())
#print(dm.getopMd())
#print(dm.getBatAlternateVolt())
#print(dm.getAlarmTxt()[0])
