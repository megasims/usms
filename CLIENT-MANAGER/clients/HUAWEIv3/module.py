import sys, os
import importlib.machinery

import time
import socket
import sqlite3
import logging
import threading
from logging.handlers import RotatingFileHandler

from .app import App

class Main:

	def __init__(self, id, manager, listen):
		self.CLIENT_ID = id
		self.MANAGER = manager
		self.LISTEN = listen

	def start(self):
		app = App(self.CLIENT_ID, self.MANAGER, self.LISTEN)
		return app.start()
		
