import sys, os, serial, time
from multiprocessing import Process
import struct, binascii
import string
import array
from time import sleep
#import Adafruit_BBIO.GPIO as GPIO
#GPIO.setup("P9_23", GPIO.OUT)
class DataManager:
    def __init__(self, log):
        self.log = log
        self.trybuffer = 3

        self.asaakh = [0x01, 0x05, 0x00, 0x7E, 0xFF, 0x00]
        self.untraakh = [0x01, 0x05, 0x00, 0x7E, 0x00, 0x00]
        self.Set_Temp = [0x01, 0x06, 0x00, 0x09, 0x00, 0x01]

        self.Get_Alarm = [0x01, 0x02, 0x00, 0x0D, 0x00, 0x18]
        self.Get_OnOff = [0x01, 0x02, 0x00, 0x0C, 0x00, 0x01]
        self.Amb_Temp = [0x01, 0x04, 0x00, 0x03, 0x00, 0x01]
        self.Amb_Hum = [0x01, 0x04, 0x00, 0x04, 0x00, 0x01]
        self.Cond_Pres = [0x01, 0x04, 0x00, 0x05, 0x00, 0x01]
        self.Cond_Temp = [0x01, 0x04, 0x00, 0x06, 0x00, 0x01]
        self.Room_Temp = [0x01, 0x04, 0x00, 0x07, 0x00, 0x01]
        self.Supp_Temp = [0x01, 0x04, 0x00, 0x08, 0x00, 0x01]
        self.Cool_Temp = [0x01, 0x04, 0x00, 0x09, 0x00, 0x01]

        self.alarmsTxt=["Compressor OL", "Low Pressure", "High Pressure", "Cond Fan OL", "SMOKE/FIRE", "PHASE alarm", "High room temperature", "Low room temperature", "Power fault", "Evap. fan OL", "Black Out","Comp.Start alarm", "Air flow stopped", "Heater 1 overload", "Dirty air filter", "EVD Fault","pLAN alarm", "Alarm Clock", "Amb. humid. probe broke", "Room T. probe broke", "Supply T. probe broke","Amb. T. probe broke", "Press.1 probe broke", "Cond. Temp. broke"]

        self.getByteLen = 7
        self.setByteLen = 8

    def init(self, serialPort):
        # try:
        self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
        print(self.ser.portstr)
        i = 0
        while not self.ser.isOpen():
            try:
                print("NowClosed")
                self.ser.open()
                print("NowOpened")
                break
            except:
                sleep(0.01)
            i = i + 1
            if i > 10:
                return False
        # self.ser.isOpen()
        self.startByte = b"~"
        self.stopByte = b"\r"
        return True

    def convertByteToInt(self, arr):
        result = []
        for idx in range(len(arr)):
            try:
                result.append(struct.unpack('B', arr[idx])[0])
            except:
                result.append(0)
                print("Convert error")
        return result

    def readSerial(self, timeout, len):
        ret = []
        cnt = 0
        #print("readSerial")
        while timeout > 0:
            if cnt >= len:
                return self.convertByteToInt(ret)
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    print('read byte', b)
                except Exception as ex:
                    b = 0
                    print('serial error decode')
                    continue

                ret.append(b)
                cnt += 1


            else:
                print('TIMED_OUT1')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def readSerial1(self, timeout):
        ret = []
        started = False
        #print("readSerial")
        while timeout > 0:
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    # print('read byte', b)
                except Exception as ex:
                    b = ''
                    print('serial error decode')
                    continue

                if b == self.stopByte:  #stopbyte orj irsen esehiig shalgana
                    return self.convertByteToInt(ret)

                if started:  # herev ehelsen bol stopbyte orj irtel buh byte-uudiig avna
                    # print('append', b)
                    ret.append(b)
                    continue
                if b == self.startByte:  # startbyte orj irseniig shalgana
                    started = True
                if not started:  # ehelsen esehiig shalgana
                    continue
            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def genFRAME(self, bufData): # CRC bodoj command-iin ard zalgaj baigaa function
        result = []
        CRC = 0xFFFF
        POLYNOMIAL = 0xA001
        for item in bufData:
            CRC ^= item
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        result.extend(bufData)

        CRCLo = CRC & 0x00ff
        CRCHi = CRC >> 8
        result.append(CRCLo)
        result.append(CRCHi)

        print(result)

        return result

    def checkCRC(self, bufData, buffsize=0):

        buffsize = len(bufData)

        CRC = 0xFFFF
        POLYNOMIAL = 0xA001
        print("check CRC")
        for i in range(buffsize-2):
            CRC ^= bufData[i]
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        CRCLo = CRC & 0x00ff
        CRCHi = CRC >> 8

        crcAr=[CRCLo, CRCHi]
        print(crcAr)
        print(buffsize)
#        return True
        if crcAr[0] != bufData[buffsize-2]:
           print("TRUE")
           return False
        else:
           return True

    def getCoolTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Cool_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                if self.checkCRC(rcv, self.getByteLen):
                	res_too_float = rcv[3]*256 +  rcv[4]
                	res_too_float = res_too_float* 0.1 # 1 bit ni iim gradustai tentsuu
                	print(('temp', res_too_float),('rcv', rcv))
                	return res_too_float
                else:
                	return 'XE04'
            except:
                print('temp except')
                trybuffer=trybuffer-1
        return 'XE06'

    def getAlarmInfo(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:

                result = self.genFRAME(self.Get_Alarm)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, 8)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, 9):
                    alarmTxt = ''
                    sum = rcv[3]*256*256 + rcv[4]*256 + rcv[5]
                    if sum==0:
                        return None
                    for i in range(25):
                        if ((sum & (1<<i))>>i)>0 :
                            alarmTxt += (self.alarmsTxt[i] + '|')
                    return alarmTxt[:-1] # |  suuliin zuraas hasaj baina
                else:
                    return 'XE04'
            except:
                print('alarm except')
                trybuffer=trybuffer-1
        return 'XE01'

    def getRoomTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Room_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, self.getByteLen):
                    res_too_float = rcv[3]*256 +  rcv[4]
                    res_too_float = res_too_float* 0.1 # 1 bit ni iim gradustai tentsuu
                    print(('temp', res_too_float),('rcv', rcv))
                    return res_too_float
                else:
                    return 'XE04'
            except:
                print('room_temp except')
                trybuffer=trybuffer-1
        return 'XE06'

    def getAmbTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Amb_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, self.getByteLen):
                    res_too_float = rcv[3]*256 +  rcv[4]
                    res_too_float = res_too_float* 0.1 # 1 bit ni iim gradustai tentsuu
                    print(('temp', res_too_float),('rcv', rcv))
                    return res_too_float
                else:
                    return 'XE04'
            except:
                print('amb_temp except')
                trybuffer=trybuffer-1
        return 'XE06'

    def getAmbHum(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Amb_Hum)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, self.getByteLen):
                    res_too_float = rcv[3]*256 +  rcv[4]
                    res_too_float = res_too_float* 0.1 # 1 bit ni iim gradustai tentsuu
                    print(('temp', res_too_float),('rcv', rcv))
                    return res_too_float
                else:
                    return 'XE04'
            except:
                print('amb_hum except')
                trybuffer=trybuffer-1
        return 'XE06'

    def getCondPres(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Cond_Pres)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, self.getByteLen):
                    res_too_float = rcv[3]*256 +  rcv[4]
                    res_too_float = res_too_float* 0.1 # 1 bit ni iim gradustai tentsuu
                    print(('temp', res_too_float),('rcv', rcv))
                    return res_too_float
                else:
                    return 'XE04'
            except:
                print('cond_pres except')
                trybuffer=trybuffer-1
        return 'XE06'

    def getCondTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Cond_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, self.getByteLen):
                    res_too_float = rcv[3]*256 +  rcv[4]
                    res_too_float = res_too_float* 0.1 # 1 bit ni iim gradustai tentsuu
                    print(('temp', res_too_float),('rcv', rcv))
                    return res_too_float
                else:
                    return 'XE04'
            except:
                print('cond_temp except')
                trybuffer=trybuffer-1
        return 'XE06'

    def getSuppTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Cond_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)
          
                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, self.getByteLen):
                    print(self.getByteLen)
                    res_too_float = rcv[3]*256 +  rcv[4]
                    print(res_too_float)
                    res_too_float = res_too_float* 0.1 # 1 bit ni iim gradustai tentsuu
                    print(('temp', res_too_float),('rcv', rcv))
                    return res_too_float
                else:
                    return 'XE04'
            except:
                print('supp_temp except')
                trybuffer=trybuffer-1
        return 'XE06'

    def setSuppTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Cond_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, self.getByteLen)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, self.getByteLen):
                    if rcv[3]%2==1:
                        return 1
                    else:
                        return 0
                else:
                    return 'XE04'
            except:
                print('on_off except')
                trybuffer=trybuffer-1
        return 'XE06'

    def getOnOff(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Get_OnOff)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, 6)

                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if self.checkCRC(rcv, 7):
                    if rcv[3]%2==1:
                        return 1
                    else:
                        return 0
                else:
                    return 'XE04'
            except:
                print('on_off except')
                trybuffer=trybuffer-1
        return 'XE06'


    def getError(self):
        return "XE01"

    def getAlarm(self):
        try:
            return self.getAlarmInfo()
        except:
            self.log.error("Can't get alarm!")
            return 'ERROR101'

    def get(self, tag):
        funcs = {
            'temp': self.getCoolTemp,
            'alarm': self.getAlarmInfo,
            'room_temp': self.getRoomTemp,
            'amb_temp': self.getAmbTemp,
            'amb_hum': self.getAmbHum,
            'cond_pres': self.getCondPres,
            'cond_temp': self.getCondTemp,
            'supp_temp': self.getSuppTemp,
            'on_off':self.getOnOff
            }
        func = funcs.get(tag, self.getError)
        return (tag, func())


    def getData(self):
      #GPIO.output("P9_23", GPIO.HIGH)
      #print('GPIO-HIGH')
      #GPIO.output("P9_23", GPIO.LOW)
      #print('GPIO-Low')
      #GPIO.cleanup()
        funcs  =  {
            'temp': self.getCoolTemp,
            'alarm': self.getAlarmInfo,
            'room_temp': self.getRoomTemp,
           # 'amb_temp': self.getAmbTemp,
            'amb_hum': self.getAmbHum,
           # 'cond_pres': self.getCondPres,
           # 'cond_temp': self.getCondTemp,
           # 'supp_temp': self.getSuppTemp,
            'on_off':self.getOnOff
        }
        ret = [];
        for tag, func in funcs.items():
            try:
                ret.append((tag,func()))
            except:
                ret.append((tag, 'E1'))

        print('Done')
        print(ret)
        return ret

    def setError(self, value):
        return "XE00"

    def setOnOff(self, value):
        command = ''
        val = 0
        try:
            val = int(float(value))
        except:
            return 'XE02'
        if val==1:
            command = self.asaakh.copy()
        elif val==0:
            command = self.untraakh.copy()
        else:
            return "XE01"

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(command)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(50, self.setByteLen)
                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if rcv==result:
                    return 'OK'
                else:
                    return 'XE105'

            except:
                print('on_off except')
                trybuffer=trybuffer-1
        return 'XE05'

    def setCoolTemp(self, value):
        temperature = 0
        try:
            temperature = int(float(value)*10)
        except:
            return 'XE02'

        self.Set_Temp[4] = int(temperature / 256)
        self.Set_Temp[5] = temperature % 256
        setTemp = self.Set_Temp.copy()
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(setTemp)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(50, self.setByteLen)
                print('read finish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if rcv==result:
                    return 'OK'
                else:
                    return 'XE105'

            except:
                print('on_off except')
                trybuffer=trybuffer-1
        return 'XE05'



    def set(self, tag, value):
        funcs = {
            'on_off':self.setOnOff,
            'temp':self.setCoolTemp,
        }
        func = funcs.get(tag, self.setError)
        return func(value)
#dataManager = DataManager(None)
#dataManager.init('/dev/ttyUSB2')
#print(dataManager.getSuppTemp())
#print(dataManager.getRoomTemp())
#print(dataManager.getCoolTemp())
#print(dataManager.getData())
