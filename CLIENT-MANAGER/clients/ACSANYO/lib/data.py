import sys, os, serial, time
from multiprocessing import Process
import struct, binascii
import string
import array
from time import sleep

class DataManager:
    def __init__(self, log):
        self.log = log
        self.trybuffer = 5

        self.ON = [0x7E, 0x0C, 0x00, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x7A]
        self.OFF = [0x7E, 0x0C, 0x00, 0x09, 0x02, 0x00, 0x00, 0x00, 0x00, 0x79]
        self.Get_Addr = [0x7E, 0x01, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78]
        self.Set_Addr = [0x7E, 0x0C, 0x00, 0x02, 0x05, 0x0C, 0x00, 0x00, 0x00, 0x79]

        self.Swing = [0x7E, 0x0C, 0x00, 0x02, 0x02, 0x01, 0x00, 0x00, 0x00, 0x73]
        self.Swing_stop = [0x7E, 0x0C, 0x00, 0x02, 0x02, 0x02, 0x00, 0x00, 0x00, 0x70]

        self.Mode_Fan = [0x7E, 0x0C, 0x00, 0x02, 0x03, 0x03, 0x00, 0x00, 0x00, 0x70]
        self.Mode_Dry = [0x7E, 0x0C, 0x00, 0x02, 0x03, 0x04, 0x00, 0x00, 0x00, 0x77]
        self.Mode_Cool = [0x7E, 0x0C, 0x00, 0x02, 0x03, 0x02, 0x00, 0x00, 0x00, 0x71]
        self.Mode_Heat = [0x7E, 0x0C, 0x00, 0x02, 0x03, 0x05, 0x00, 0x00, 0x00, 0x76]
        self.Mode_Auto = [0x7E, 0x0C, 0x00, 0x02, 0x03, 0x05, 0x00, 0x00, 0x00, 0x76]

        self.Speed_auto = [0x7E, 0x0C, 0x00, 0x02, 0x04, 0x01, 0x00, 0x00, 0x00, 0x75]
        self.Speed_High = [0x7E, 0x0C, 0x00, 0x02, 0x04, 0x02, 0x00, 0x00, 0x00, 0x76]
        self.Speed_Mid = [0x7E, 0x0C, 0x00, 0x02, 0x04, 0x03, 0x00, 0x00, 0x00, 0x77]
        self.Speed_Low = [0x7E, 0x0C, 0x00, 0x02, 0x04, 0x04, 0x00, 0x00, 0x00, 0x70]

        self.Set_temp = [0x7E, 0x0C, 0x00, 0x02, 0x01, 0x17, 0x00, 0x00, 0x00]
        self.Get_temp = [0x7E, 0x0C, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x77]

        self.Get1 = [0x7E, 0x0C, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x71]

        self.Alarms = [0x00, 0x00, 0x00,0x00, 0x45, 0x00, 0x46, 0x00, 0x48, 0x00, 0x00, 0x00, 0x4C, 0x00, 0x50, 0x00]
        # dah byte-aas ehleed
        # -status-1on
        # -2off
        # -swing- 1swing
        #       2 stop
        # -mode  -1 heat 2cool 3fan 4dry 5auto
        # -speed -1auto 2high 3mid 4low
        # -alarm type 4-E 6-F 8-H 0x0c-L 0x0E-P
        # -alarm code
        self.Get2 = [0x7E, 0x0C, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x77]
        # -set temp value
        # -room temp
        # -E1 temp
        # -E2 temp
        # -compr_temp
        # -C1 temp
        # -C2 temp
        # -valve state
        self.alarmsTxt = ["Compressor OL", "Low Pressure", "High Pressure", "Cond Fan OL", "SMOKE/FIRE", "PHASE alarm",
                          "High room temperature", "Low room temperature", "Power fault", "Evap. fan OL", "Black Out",
                          "Comp.Start alarm", "Air flow stopped", "Heater 1 overload", "Dirty air filter", "EVD Fault",
                          "pLAN alarm", "Alarm Clock", "Amb. humid. probe broke", "Room T. probe broke",
                          "Supply T. probe broke", "Amb. T. probe broke", "Press.1 probe broke", "Cond. Temp. broke"]

        self.getByteLen = 8
        self.setByteLen = 8

        self.ser = None

    def init(self, serialPort):
        # try:
        self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
        print(self.ser.portstr)
        i = 0
        while not self.ser.isOpen():
            try:
                print("NowClosed")
                self.ser.open()
                print("NowOpened")
                break
            except:
                sleep(0.01)
            i = i + 1
            if i > 10:
                return False
        # self.ser.isOpen()
        self.startByte = b"~"
        self.stopByte = b"\r"
        return True

    def convertByteToInt(self, arr):
        result = []
        for idx in range(len(arr)):
            try:
                result.append(struct.unpack('B', arr[idx])[0])
            except:
                result.append(0)
                print("Convert error")
        return result

    def readSerial(self, timeout, len):
        ret = []
        cnt = 0
        # print("readSerial")
        while timeout > 0:
            if cnt >= len:
                return self.convertByteToInt(ret)
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                   # print('read byte', b)
                except Exception as ex:
                    b = 0
                    #print('serial error decode')
                    continue

                ret.append(b)
                cnt += 1


            else:
                print('READSERIAL:TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def readSerial1(self, timeout):
        ret = []
        started = False
        # print("readSerial")
        while timeout > 0:
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                # print('read byte', b)
                except Exception as ex:
                    b = ''
                    print('serial error decode')
                    continue

                if b == self.stopByte:  #stopbyte orj irsen esehiig shalgana
                    return self.convertByteToInt(ret)

                if started:  # herev ehelsen bol stopbyte orj irtel buh byte-uudiig avna
                    # print('append', b)
                    ret.append(b)
                    continue
                if b == self.startByte:  # startbyte orj irseniig shalgana
                    started = True
                if not started:  # ehelsen esehiig shalgana
                    continue
            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def genFRAME(self, bufData):  # CRC bodoj command-iin ard zalgaj baigaa function
        result = []
        CRC = 0xFFFF
        POLYNOMIAL = 0xA001
        for item in bufData:
            CRC ^= item
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        result.extend(bufData)

        CRCLo = CRC & 0x00ff;
        CRCHi = CRC >> 8;
        result.append(CRCLo)
        result.append(CRCHi)

        print(result)

        return result

    def checkCRC(self, bufData):

        CRC = 0xFFFF
        POLYNOMIAL = 0xA001

        temp = bufData[:-2]

        for item in temp:
            CRC ^= item
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        CRCLo = CRC & 0x00ff
        CRCHi = CRC >> 8

        crcAr = [CRCLo, CRCHi]

        if crcAr == bufData[-2:]:
            return True
        else:
            return False

    def CheckSUM(self, bufData):
        sum = 0x0
        for item in bufData:
            sum = sum ^ item
        if sum==0:
            return True
        else:
            return False

    def get1(self, value):
        # 7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', self.Get1).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, 14)

                #print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'
                if self.CheckSUM(rcv):
                    return rcv[value]
                else:
                    return 'XE105'
            except:
                print('temp except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def get2(self, value):
        # 7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', self.Get2).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(5, 14)

                #print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'
                if self.CheckSUM(rcv):
                    return rcv[value]
                else:
                    return 'XE105'
            except:
                print('temp except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def getError(self, value):
        return "XE01"

    def getAlarm(self,value):
    	res = []
    	trybuffer = self.trybuffer
    	while trybuffer>0:
    		try:
    			self.ser.flushOutput()
    			self.ser.flushInput()
    			temp = array.array('B',self.Get1).tostring()
    			self.ser.write(temp)
    			print("Serial write")
    			time.sleep(0.05)
    			rcv = self.readSerial(5,14)
    			print(rcv)
    			if rcv=='TIMED_OUT':
    				return 'TIME_OUT'
    			elif (self.CheckSUM(rcv)):
    				if rcv[9]<15:
    					ret=chr(self.Alarms[rcv[9]])
    					res=str(ret)
    					print(ret)    					
    					if rcv[10] == 0:
    						return ""
    					else:
    						res = res + str(rcv[10]) +'|'
    						return res
    			else:
    				return 'XE05'
    		except:
    			self.log.error("Can't get alarm!")
    			trybuffer = trybuffer-1
    			return 'XE04'
    	return 'XE01'	

    def get(self, tag):
        funcs = {
            'acOnOff': {'func': self.get1, 'param':5},
            'swing': {'func': self.get1, 'param':6},
            'mode': {'func': self.get1, 'param':7},
            'fan_speed': {'func': self.get1, 'param':8},
            'temp': {'func': self.get2, 'param':5},
            'room_temp': {'func': self.get2, 'param':6},
            'coil1_temp': {'func': self.get2, 'param':10},
            'coil2_temp': {'func': self.get2, 'param':11},
            'compr_temp': {'func': self.get2, 'param':9},
            'alarm': {'func': self.getAlarm, 'param':None}
        }
        temp = funcs.get(tag, {'func':self.getError, 'param':None})
        return (tag, temp['func'](temp['param']))

    def getData(self):
        funcs = {
            'acOnOff': {'func': self.get1, 'param':5},
            'swing': {'func': self.get1, 'param':6},
            'mode': {'func': self.get1, 'param':7},
            'fan_speed': {'func': self.get1, 'param':8},
            'temp': {'func': self.get2, 'param':5},
            'room_temp': {'func': self.get2, 'param':6},
            'coil1_temp': {'func': self.get2, 'param':10},
            'coil2_temp': {'func': self.get2, 'param':11},
            'compr_temp': {'func': self.get2, 'param':9},
            'alarm':{'func':self.getAlarm, 'param':1}

        }
        self.setAddr(12)
        ret = []
        for tag, func in funcs.items():
            try:
                ret.append((tag, func['func'](func['param'])))
            except:
                ret.append((tag, 'E1'))

        print('Done')
        print(ret)
        return ret

    def setError(self, value):
        return "XE00"

    def setOnOff(self, value):
        command = ''
        val = 0
        try:
            val = int(float(value))
        except:
            return 'XE02'
        if val == 1:
            command = self.ON
        elif val == 2:
            command = self.OFF
        else:
            return "XE01"

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', command).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(10, 14)
                print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'

                if self.CheckSUM(rcv):
                    return "OK"
                else:
                    return 'XE105'

            except:
                print('setOnOff except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def setMode(self, value):
        val = 0
        try:
            val = int(float(value))
        except:
            return 'XE02'

        if val == 1:
            command = self.Mode_Heat
        elif val == 2:
            command = self.Mode_Cool
        elif val == 3:
            command = self.Mode_Fan
        elif val == 4:
            command = self.Mode_Dry
        elif val == 5:
            command = self.Mode_Auto
        else:
            return "XE01"

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', command).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(10, 14)
                print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'

                if self.CheckSUM(rcv):
                    return "OK"
                else:
                    return 'XE105'

            except:
                print('setMode except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def setAddr(self, value):
        setAddr = self.Set_Addr
        setAddr[5] = value
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh
                temp = array.array('B', self.Get_Addr).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')
        
                time.sleep(0.5)
                rcv = self.readSerial(10, 14)
                print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    print('setAddr TIMED_OUT')
                    trybuffer = trybuffer - 1
                    continue
                    
                setAddr[1] = rcv[5]
                address = 0
                for i in range(9):
                    address ^= setAddr[i]
                setAddr[9] = address 
                
                temp = array.array('B', setAddr).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.5)
                rcv = self.readSerial(10, 14)
                
                if self.CheckSUM(rcv):
                    return "OK"
                else:
                    return 'XE105'

            except:
                print('setAddr except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def setTemp(self, value):
        val = 0
        try:
            val = int(float(value))
        except:
            return 'XE02'
        temp = self.Set_temp.copy()
        temp[5] = val
        s = 0x0
        for item in temp:
            s ^= item
        temp.append(s)

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', temp).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(10, 14)
                print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'

                if self.CheckSUM(rcv):
                    return "OK"
                else:
                    return 'XE105'

            except:
                print('setTemp except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def setSwing(self, value):
        command = ''
        val = 0
        try:
            val = int(float(value))
        except:
            return 'XE02'
        if val == 1:
            command = self.Swing
        elif val == 2:
            command = self.Swing_stop
        else:
            return "XE01"

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', command).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(10, 14)
                print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'

                if self.CheckSUM(rcv):
                    return "OK"
                else:
                    return 'XE105'

            except:
                print('on_off except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def setFanSpeed(self,value):
        val = 0
        try:
            val = int(float(value))
        except:
            return 'XE02'

        if val == 1:
            command = self.Speed_auto
        elif val == 2:
            command = self.Speed_High
        elif val == 3:
            command = self.Speed_Mid
        elif val == 4:
            command = self.Speed_Low
        else:
            return "XE01"

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', command).tostring()  # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.05)
                rcv = self.readSerial(10, 14)
                print('read finish', rcv)
                if rcv == 'TIMED_OUT':
                    return 'TIME_OUT'

                if self.CheckSUM(rcv):
                    return "OK"
                else:
                    return 'XE105'

            except:
                print('setFanSpeed except')
                trybuffer = trybuffer - 1
        return 'XE05'

    def set(self, tag, value):
        funcs = {
            'acOnOff': self.setOnOff,
            'mode': self.setMode,
            'temp':self.setTemp,
            'swing':self.setSwing,
            'fan_speed':self.setFanSpeed,
        }
        func = funcs.get(tag, self.setError)
        return func(value)


#dataManager = DataManager(None)#
#ataManager.init('/dev/ttyUSB2')
#print(dataManager.setAddr(12))
#print(dataManager.set('acOnOff', 1)#)#
#print(dataManager.getData())
