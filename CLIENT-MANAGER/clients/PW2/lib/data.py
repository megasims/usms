import sys, os, serial, time
from multiprocessing import Process
import struct, binascii
import string
import array
from time import sleep

class DataManager:

    def __init__(self, log):
        self.log = log
        self.trybuffer = 3
        self.autoMode = [0x01, 0x06, 0x01, 0x2D, 0x00, 0x01]
        self.asaakh = [0x01, 0x06, 0x01, 0x2D, 0x00, 0x02]
        self.untraakh = [0x01, 0x06, 0x01, 0x2D, 0x00, 0x00]
        self.En_Oil_Press = [0x01, 0x03, 0x00, 0xC7, 0x00, 0x01]
        self.En_Cool_Temp = [0x01, 0x03, 0x00, 0xC8, 0x00, 0x01]
        self.Fuel_level = [0x01, 0x03, 0x01, 0x01, 0x00, 0x01]
        self.Gen_Freq = [0x01, 0x03, 0x00, 0x65, 0x00, 0x01]
        self.En_OpMode = [0x01, 0x03, 0x01, 0x2C, 0x00, 0x01]
        self.En_AutoState = [0x01, 0x03, 0x00, 0xCD, 0x00, 0x01]
        self.Bat_volt = [0x01, 0x03, 0x00, 0xC9, 0x00, 0x01]
        self.En_rpm = [0x01, 0x03, 0x00, 0xCA, 0x00, 0x01]
        self.cool_dur_time = [0x01, 0x03, 0x04, 0x1D, 0x00, 0x01]

        self.genaVol = [0x01, 0x03, 0x00, 0x6B, 0x00, 0x01]
        self.genbVol = [0x01, 0x03, 0x00, 0x6C, 0x00, 0x01]
        self.gencVol = [0x01, 0x03, 0x00, 0x6D, 0x00, 0x01]

        self.genaCur = [0x01, 0x03, 0x00, 0x6E, 0x00, 0x01]
        self.genbCur = [0x01, 0x03, 0x00, 0x6F, 0x00, 0x01]
        self.gencCur = [0x01, 0x03, 0x00, 0x70, 0x00, 0x01]

        self.motorRuntime =[0x01, 0x03, 0x00, 0xCB, 0x00, 0x02]#0.05 = 4 byte

        self.SETLOGMOD = [0x01, 0x06, 0x05, 0xD9, 0x00, 0x00]
        self.GETEVENT = [0x01, 0x03, 0x05, 0xDB, 0x00, 0x0E]

        self.getByteLen = 7
        self.setByteLen = 8

    def init(self, serialPort):
        # try:
        self.ser = serial.Serial(port=serialPort, baudrate=19200, parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS,timeout=1)
        print(self.ser.portstr)
        i = 0
        while not self.ser.isOpen():
            try:
                print("NowClosed")
                self.ser.open()
                print("NowOpened")
                break
            except:
                sleep(0.01)
            i = i + 1
            if i > 10:
                return False
        # self.ser.isOpen()
        self.startByte = b"~"
        self.stopByte = b"\r"
        return True

    def convertByteToInt(self, arr):
        result = []
        for idx in range(len(arr)):
            try:
                result.append(struct.unpack('B', arr[idx])[0])
            except:
                result.append(0)
                print("Convert error")
        return result

    def readSerial(self, timeout, len):
        ret = []
        cnt = 0
        #print("readSerial")
        while timeout > 0:
            if cnt >= len:
                return self.convertByteToInt(ret)
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    # print('read byte', b)
                except Exception as ex:
                    b = 0
                    print('serial error decode')
                    continue

                ret.append(b)
                cnt += 1
            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def readSerial1(self, timeout):
        ret = []
        started = False
        #print("readSerial")
        while timeout > 0:
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    # print('read byte', b)
                except Exception as ex:
                    b = 0
                    print('serial error decode')
                    continue

                if b == self.stopByte:  #stopbyte orj irsen esehiig shalgana
                    return ret

                if started:  # herev ehelsen bol stopbyte orj irtel buh byte-uudiig avna
                    # print('append', b)
                    ret.append(b)
                    continue
                if b == self.startByte:  # startbyte orj irseniig shalgana
                    started = True
                if not started:  # ehelsen esehiig shalgana
                    continue
            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def genFRAME(self, bufData): # CRC bodoj command-iin ard zalgaj baigaa function
        result = []
        CRC = 0xFFFF
        POLYNOMIAL = 0xA001
        for item in bufData:
            CRC ^= item
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        result.extend(bufData)

        CRCLo = CRC & 0x00ff;
        CRCHi = CRC >> 8;
        result.append(CRCLo)
        result.append(CRCHi)
        print(result)
        return result

    def checkCRC(self, bufData):

        CRC = 0xFFFF
        POLYNOMIAL = 0xA001

        temp = bufData[:-2]

        for item in temp:
            CRC ^= item
            for j in range(8):
                if CRC & 0x0001:
                    CRC >>= 1
                    CRC ^= POLYNOMIAL
                else:
                    CRC >>= 1

        CRCLo = CRC & 0x00ff
        CRCHi = CRC >> 8

        crcAr=[CRCLo, CRCHi]

        if crcAr==bufData[-2:] :
           return True
        else:
            return False

    def getGenaVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genaVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +  rcv[4]
                print(('GenaVol', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenaVol except')
                trybuffer=trybuffer-1
        return 'E1'

    def getGenbVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genbVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +  rcv[4]
                print(('GenbVol', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenbVol except')
                trybuffer=trybuffer-1
        return 'E1'

    def getGencVol(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.gencVol)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +  rcv[4]
                print(('GencVol', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencVol except')
                trybuffer=trybuffer-1
        return 'E1'

    def getGenaCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genaCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +  rcv[4]
                print(('GenaCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenaCur except')
                trybuffer=trybuffer-1
        return 'E1'

    def getGenbCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.genbCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +  rcv[4]
                print(('GenbCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GenbCur except')
                trybuffer=trybuffer-1
        return 'E1'

    def getGencCur(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.gencCur)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +  rcv[4]
                print(('GencCur', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('GencCur except')
                trybuffer=trybuffer-1
        return 'E1'

    def getMotorRuntime(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.motorRuntime)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, 9)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = ((rcv[3]*256 +  rcv[4])*256+rcv[5])*256+rcv[6]
                print(('motorRuntime', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('motorRuntime except')
                trybuffer=trybuffer-1
        return 'E1'

    def getCoolTemp(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_Cool_Temp)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                res_too_float = rcv[3]*256 +  rcv[4]

                res_too_float = res_too_float* 0.03125 - 273 # 1 bit ni iim gradustai tentsuu
                print(('cool', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('cool except')
                trybuffer=trybuffer-1
        return 'E1'

    def getoilP(self):
        #7E 01 03 02 00 00 B8 44 0D
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_Oil_Press)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = res_too_float* 0.125 # 1 bit ni iim gradustai tentsuu
                print(('oilP', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('oilP except')
                trybuffer=trybuffer-1
        return 'E1'

    def getfuel(self):

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Fuel_level)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = res_too_float* 0.0078125 # 1 bit ni iim gradustai tentsuu
                print(('fuel', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('fuel except')
                trybuffer=trybuffer-1
        return 'E1'

    def getfreq(self):

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Gen_Freq)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = res_too_float * 1.0/ 128 # 1 bit ni iim gradustai tentsuu
                print(('freq', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('freq except')
                trybuffer=trybuffer-1
        return  'E1'

    def getopMd(self):

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_OpMode)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUT'
                res_too_float = rcv[4]
                print(('opMd', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('opMd except')
                trybuffer=trybuffer-1
        return  'E1'

    def getAust(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_AutoState)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUT'
                res_too_float = rcv[4]
                print(('Aust', res_too_float),('rcv', rcv))
                return res_too_float
            except:
                print('Aust except')
                trybuffer=trybuffer-1
        return  'E1'

    def getBatVolt(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.Bat_volt)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return  'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = float(res_too_float) / 20 # 1 bit ni iim gradustai tentsuu
                print(('BatVolt', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('BatVolt except')
                trybuffer=trybuffer-1
        return  'E1'

    def getEngineRPM(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.En_rpm)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(1)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                res_too_float = int(res_too_float*0.125) # 1 bit ni iim gradustai tentsuu
                print(('EngineRPM', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('EngineRPM except')
                trybuffer=trybuffer-1
        return  'E1'

    def getCoolDuration(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.cool_dur_time)

                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                #time.sleep(0.005)
                rcv = self.readSerial(5, self.getByteLen)

                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'
                res_too_float = rcv[3]*256 +  rcv[4]
                print(('CoolDuration', res_too_float),('rcv', rcv))
                return  res_too_float
            except:
                print('CoolDuration except')
                trybuffer=trybuffer-1
        return 'E5'

    def getLogMOD(self):
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(self.SETLOGMOD)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(1)
                rcv = self.readSerial(5, 8)
                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if rcv==result:
                    return True
                else:
                    return False
            except:
                print('CoolDuration except')
                trybuffer=trybuffer-1
        return 'Connection Error'

    def toTimeText(self, val):
        if val<10:
            return str('0')+str(val)
        else:
            return  str(val)

    def oneAlarm(self, buff):
        trybuffer = self.trybuffer
        while True:
            if trybuffer <= 0:
                return  {"alarm": False, 'msg': 'ConnectionError'}
            try:
                res1 = self.genFRAME(buff)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', res1).tostring()  # byte array to string
                self.ser.write(temp)
                # print('Serial write')

                #time.sleep(0.03)
                rcv = self.readSerial(5, 33)
                print('readfinish', rcv)
                if rcv == 'TIMED_OUT':
                    return  {"alarm": False, 'msg': 'TIME_OUT'}
                if not self.checkCRC(rcv):
                    print('CRC error')
                    trybuffer = trybuffer - 1

                R11_12 = (rcv[26] * 256 + rcv[27]) * 256 + rcv[28]

                R10_hi = rcv[23]
                R10_lo = rcv[24]

                R0_hi = rcv[3]
                R0_lo = rcv[4]

                R1_hi = rcv[5]
                R1_lo = rcv[6]

                R2_hi = rcv[7]
                R2_lo = rcv[8]

                timeTmp = str(1985 + R2_lo) + '-' + self.toTimeText(R1_lo) + '-' + self.toTimeText(
                    int(R2_hi / 4)) + " " + self.toTimeText(R1_hi) + ":" + self.toTimeText(
                    R0_lo) + ":" + self.toTimeText(int(R0_hi / 4))

                # R13 = rcv[29]*256+rcv[30]
                # print("idx", R13)
                print("SPN", int(R11_12 / 32))
                print("FMI", int(R11_12) % 32)
                print("OCC", R10_lo)
                print("STATUS", R10_hi % 16)
                print("timeTmp", timeTmp)
                return  {"alarm": True,
                        "R11_12": R11_12,
                               # 'idx':R13 ,
                        "SPN": int(R11_12 / 32),
                        "FMI": int(R11_12 % 32),
                        "OCC": R10_lo,
                        "STATUS": R10_hi % 16,
                        "TIME": timeTmp}

            except:
                print('Alarm except')
                trybuffer = trybuffer - 1

    def alarms(self):
        trybuffer = self.trybuffer

        while not self.getLogMOD():
            trybuffer-=1
            if(trybuffer <= 0):
                return None

        getEv = self.GETEVENT
        startID = 0x05*256+0xDB
        result=[]
        for idx in range(20):
            getEv[2] = int(startID / 256)
            getEv[3] = int(startID % 256)
            result.append(self.oneAlarm(getEv))
            startID += 14
        return result

    def getAlarm(self):

        stts={
            0:'inactive',
            4:'active',
            5:'Present',
            7:'Unavailable'
        }

        try:
            alarms = self.alarms()
            text = ""

            alarmTxt = self.getAlarmTxt()
            
            for alarm in alarms:
                try:
                    if alarm.get("alarm",False):
                        tt = alarmTxt.get(alarm.get('R11_12',0), "Unknown alarm")
                        #result.append({"alarm":True, "R11_12":R11_12, "SPN": int(R11_12/32), "FMI":int(R11_12%32), "OCC":R10_lo, "STATUS":R10_hi%16})
                        text+=tt+" 'OCC':"+str(alarm.get("OCC",0))+" "+stts.get(alarm.get("STATUS",0),"invalid")+" "+alarm.get("TIME","")+"|"
                    else:
                        text+=alarm.get("msg")
                except:
                    print ("alarm")
            return ('alarm',text.strip('|'))
        except:
            self.log.error("Can't get alarm!")
            return 'ERROR101'

    def getData(self):
        funcs = {
            'cool': self.getCoolTemp,
            'oilP': self.getoilP,
            'fuel': self.getfuel,
            'freq': self.getfreq,
            'on_off_auto': self.getopMd,
            'aust': self.getAust,
            'batVolt': self.getBatVolt,
            'engineRPM': self.getEngineRPM,
            'coolDuration':self.getCoolDuration,
            'genaCur':self.getGenaCur,
            'genbCur':self.getGenbCur,
            'gencCur':self.getGencCur,
            'genaVol':self.getGenaVol,
            'genbVol':self.getGenbVol,
            'gencVol':self.getGencVol,
            'motorRuntime':self.getMotorRuntime,
           # 'alarm':self.getAlarm
        }
	
        ret = [];
        for tag, func in funcs.items():
            try:
                ret.append((tag,func()))
            except:
                ret.append((tag, 'E1'))
        ret.append(self.getAlarm())
        print('-------------------getData result------------------')
        print(ret)
        print('---------------------------------------------------')
        return ret

    def setError(self, value):
        return "E0"

    def setOnOff(self, value):
        command = ''
        try:
            val = int(value)
        except:
            return 'XE02'

        if val==2:
            command = self.asaakh.copy()
        elif val==0:
            command = self.untraakh.copy()
        elif val==1:
            command = self.autoMode.copy()
        else:
            return "XE01"

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                result = self.genFRAME(command)
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', result).tostring() # byte array to string
                self.ser.write(temp)
                print('Serial write')

                time.sleep(0.3)
                rcv = self.readSerial(5, self.setByteLen)
                print('readfinish', rcv)
                if rcv=='TIMED_OUT':
                    return 'TIME_OUT'

                if rcv==result:
                    return 'OK'
                else:
                    return 'Error'

            except:
                print('CoolDuration except')
                trybuffer=trybuffer-1
        return 'Connection Error'

    def set(self, tag, value):
        funcs = {
            'on_off_auto': self.setOnOff
        }
        func = funcs.get(tag, self.setError)
        return func(value)
    
    def getAlarmTxt(self):
        alarmTxt={
            1216:"External Tank High Fuel Level Shutdown",
            1217:"External Tank Low Fuel Level Shutdown",
            1219:"External Tank Fuel Level Sensor Short High",
            1220:"External Tank Fuel Level Sensor Short Low",
            1231:"External Tank High Fuel Level Warning",
            1233:"External Tank Low Fuel Level Warning",
            2624:"High Starting Air Pressure Shutdown",
            2625:"Low Starting Air Pressure Shutdown",
            2627:"Starting Air Pressure Sensor Short High",
            2628:"Starting Air Pressure Sensor Short Low",
            2639:"High Starting Air Pressure Warning",
            2641:"Low Starting Air Pressure Warning",
            3040:"High Fuel Filter Differential Pressure Shutdown",
            3041:"Low Fuel Filter Differential Pressure Shutdown",
            3043:"Fuel Filter Differential Pressure Sensor Short High",
            3044:"Fuel Filter Differential Pressure Sensor Short Low",
            3055:"High Fuel Filter Differential Pressure Warning",
            3057:"Low Fuel Filter Differential Pressure Warning",
            3072:"High Fuel Level Shutdown",
            3073:"Low Fuel Level Shutdown",
            3075:"Fuel Level Sensor Short High",
            3076:"Fuel Level Sensor Short Low",
            3087:"High Fuel Level Warning",
            3089:"Low Fuel Level Warning",
            3136:"High Engine Oil Level Shutdown",
            3137:"Low Engine Oil Level Shutdown",
            3139:"Engine Oil Level Sensor Short High",
            3140:"Engine Oil Level Sensor Short Low",
            3151:"High Engine Oil Level Warning",
            3153:"Low Engine Oil Level Warning",
            3168:"High Engine Oil Filter Differential Pressure Shutdown",
            3169:"Low Engine Oil Filter Differential Pressure Shutdown",
            3171:"Engine Oil Filter Differential Pressure Sensor Short High",
            3172:"Engine Oil Filter Differential Pressure Sensor Short Low",
            3183:"High Engine Oil Filter Differential Pressure Warning",
            3185:"Low Engine Oil Filter Differential Pressure Warning",
            3201:"Low Engine Oil Pressure Shutdown",
            3203:"Engine Oil Pressure Sensor Short High",
            3204:"Engine Oil Pressure Sensor Short Low",
            3217:"Low Engine Oil Pressure Warning",
            3424:"High Air Filter Differential Pressure Shutdown",
            3425:"Low Air Filter Differential Pressure Shutdown",
            3427:"Air Filter Differential Pressure Sensor Short High",
            3428:"Air Filter Differential Pressure Sensor Short Low",
            3439:"High Air Filter Differential Pressure Warning",
            3441:"Low Air Filter Differential Pressure Warning",
            3520:"High Engine Coolant Temperature Shutdown",
            3523:"Engine Coolant Temperature Sensor Short High",
            3524:"Engine Coolant Temperature Sensor Short Low",
            3535:"High Engine Coolant Temperature Warning",
            3537:"Low Engine Coolant Temperature Warning",
            3552:"High Engine Coolant Level Shutdown",
            3553:"Low Engine Coolant Level Shutdown",
            3555:"Engine Coolant Level Sensor Short High",
            3556:"Engine Coolant Level Sensor Short Low",
            3567:"High Engine Coolant Level Warning",
            3569:"Low Engine Coolant Level Warning",
            4384:"High Fire Extinguisher Pressure Shutdown",
            4385:"Low Fire Extinguisher Pressure Shutdown",
            4387:"Fire Extinguisher Pressure Sensor Short High",
            4388:"Fire Extinguisher Pressure Sensor Short Low",
            4399:"High Fire Extinguisher Pressure Warning",
            4401:"Low Fire Extinguisher Pressure Warning",
            5355:"Battery Charger Failure",
            5375:"Battery Charger Failure",
            5361:"Low Battery Charging System Voltage Warning",
            5376:"High Battey Voltage Shutdown",
            5391:"High Battery Voltage Warning",
            5393:"Low Battery Voltage Warning",
            5472:"High Ambient Air Temperature Shutdown",
            5473:"Low Ambient Air Temperature Shutdown",
            5475:"Ambient Air Temperature Sensor Short High",
            5476:"Ambient Air Temperature Sensor Short Low",
            5487:"High Ambient Air Temperature Warning",
            5489:"Low Ambient Air Temperature Warning",
            5536:"High Exhaust Temperature Shutdown",
            5537:"Low Exhaust Temperature Shutdown",
            5539:"Exhaust Temperature Sensor Short High",
            5540:"Exhaust Temperature Sensor Short Low",
            5551:"High Exhaust Temperature Warning",
            5553:"Low Exhaust Temperature Warning",
            5600:"High Engine Oil Temperature Shutdown",
            5601:"Low Enginer Oil Temperature Shutdown",
            5603:"Engine Oil Temperature Sensor Short High",
            5604:"Engine Oil Temperature Sensor Short Low",
            5615:"High Engine Oil Temperature Warning",
            5617:"Low Engine Oil Temperature Warning",
            6080:"Engine Over Speed Shutdown",
            6081:"Engine Under Speed Shutdown",
            6082:"Engine Speed Sensor Erratic or Not Present",
            6085:"Engine Speed Sensor Open",
            6097:"Engine Under Speed Warning",
            20002:"Modbus Data Link Configuration Error",
            20011:"Modbus Data Link Fault",
            20459:"Primary Data Link Fault",
            22432:"Custom Event #1 High Shutdown",
            22433:"Custom Event #1 Low Shutdown",
            22447:"Custom Event #1 High Warning",
            22449:"Custom Event #1 Low Warning",
            22463:"Custom Event #1 Status",
            22464:"Custom Event #2 High Shutdown",
            22465:"Custom Event #2 Low Shutdown",
            22479:"Custom Event #2 High Warning",
            22481:"Custom Event #2 Low Warning",
            22495:"Custom Event #2 Status",
            22496:"Custom Event #3 High Shutdown",
            22497:"Custom Event #3 Low Shutdown",
            22511:"Custom Event #3 High Warning",
            22513:"Custom Event #3 Low Warning",
            22527:"Custom Event #3 Status",
            22528:"Custom Event #4 High Shutdown",
            22529:"Custom Event #4 Low Shutdown",
            22543:"Custom Event #4 High Warning",
            22545:"Custom Event #4 Low Warning",
            22559:"Custom Event #4 Status",
            22560:"Custom Event #5 High Shutdown",
            22561:"Custom Event #5 Low Shutdown",
            22575:"Custom Event #5 High Warning",
            22577:"Custom Event #5 Low Warning",
            22591:"Custom Event #5 Status",
            22592:"Custom Event #6 High Shutdown",
            22593:"Custom Event #6 Low Shutdown",
            22607:"Custom Event #6 High Warning",
            22609:"Custom Event #6 Low Warning",
            22623:"Custom Event #6 Status",
            22624:"Custom Event #7 High Shutdown",
            22625:"Custom Event #7 Low Shutdown",
            22639:"Custom Event #7 High Warning",
            22641:"Custom Event #7 Low Warning",
            22655:"Custom Event #7 Status",
            22656:"Custom Event #8 High Shutdown",
            22673:"Custom Event #8 Low Warning",
            22657:"Custom Event #8 Low Shutdown",
            22671:"Custom Event #8 High Warning",
            22687:"Custom Event #8 Status",
            22688:"Custom Event #9 High Shutdowm",
            22689:"Custom Event #9 Low Shutdown",
            22703:"Custom Event #9 High Warning",
            22705:"Custom Event #9 Low Warning",
            22719:"Custom Event #9 Status",
            22720:"Custom Event #10 High Shutdown",
            22721:"Custom Event #10 Low Shutdown",
            22735:"Custom Event #10 High Warning",
            22737:"Custom Event #10 Low Warning",
            22751:"Custom Event #10 Status",
            22752:"Custom Event #11 High Shutdown",
            22753:"Custom Event #11 Low Shutdown",
            22767:"Custom Event #11 High Warning",
            22769:"Custom Event #11 Low Warning",
            22783:"Custom Event #11 Status",
            22784:"Custom Event #12 High Shutdown",
            22785:"Custom Event #12 Low Shutdown",
            22799:"Custom Event #12 High Warning",
            22801:"Custom Event #12 Low Warning",
            22815:"Custom Event #12 Status",
            22816:"Custom Event #13 High Shutdown",
            22817:"Custom Event #13 Low Shutdown",
            22831:"Custom Event #13 High Warning",
            22833:"Custom Event #13 Low Warning",
            22847:"Custom Event #13 Status",
            22848:"Custom Event #14 High Shutdown",
            22849:"Custom Event #14 Low Shutdown",
            22863:"Custom Event #14 High Warning",
            22865:"Custom Event #14 Low Warning",
            22879:"Custom Event #14 Status",
            22880:"Custom Event #15 High Shutdown",
            22881:"Custom Event #15 Low Shutdown",
            22895:"Custom Event #15 High Warning",
            22897:"Custom Event #15 Low Warning",
            22911:"Custom Event #15 Status",
            22912:"Custom Event #16 High Shutdown",
            22913:"Custom Event #16 Low Shutdown",
            22927:"Custom Event #16 High Warning",
            22929:"Custom Event #16 Low Warning",
            22943:"Custom Event #16 Status",
            29571:"Digital Output #1 Short High",
            31071:"Emergency Stop Switch Activated",
            29603:"Digital Output #2 Short High",
            29635:"Digital Output #3 Short High",
            35904:"High Generating Set Rear Bearing Temperature Shutdown",
            35905:"Low Generating Set Rear Bearing Temperature Shutdown",
            35907:"Generating Set Rear Bearing Temperature Sensor Short High",
            35908:"Generating Set Rear Bearing Temperature Sensor Short Low",
            35919:"High Generating Set Rear Bearing Temperature Warning",
            35921:"Low Generating Set Rear Bearing Temperature Warning",
            39403:"Accessory Data Link Fault",
            39615:"Emergency Shutdown Override Mode Active",
            39679:"Fuel Tank Leak",
            44267:"Unexpected Engine Shutdown",
            53279:"Engine Failure to Start",
            77856:"High Right Exhaust Temperature Shutdown",
            77857:"Low Right Exhaust Temperature Shutdown",
            77859:"Right Exhaust Temperature Sensor Short High",
            77860:"Right Exhaust Temperature Sensor Short Low",
            77871:"High Right Exhaust Temperature Warning",
            77873:"Low Right Exhaust Temperature Warning",
            77888:"High Left Exhaust Temperature Warning",
            77889:"Low Left Exhaust Temperature Warning",
            77891:"Left Exhaust Temperature Sensor Short High",
            77892:"Left Exhaust Temperature Sensor Short Low",
            77903:"High Left Exhaust Temperature Warning",
            77905:"Low Left Exhaust Temperature Warning",
            77952:"Generating Set Over Frequency Shutdown",
            77953:"Generating Set Under Frequency Shutdown",
            77954:"Engine Speed-Generating Set Output Frequency Mismatch Warning",
            77964:"Generating Set Output Sensing System Failure",
            77967:"Generating Set Over Frequency Warning",
            77969:"Generating Set Under Frequency Warning",
            78080:"Generating Set Over Voltage Shutdown",
            78081:"Generating Set Under Voltage Shutdown",
            78095:"Generating Set Over Voltage Warning",
            78097:"Generating Set Under Voltage Warning",
            78336:"Generating Set Over Current Shutdown",
            78351:"Generating Set Over Current Warning",
            78465:"Generating Set Reverse Power Shutdown",
            78481:"Generating Set Reverse Power Warning",
            80962:"Dead Bus Inconsistent Sensing Warning",
            84675:"Digital Output #4 Short High",
            84767:"Service Maintenance Interval Warning",
            114175:"Generating Set Control Not in Automatic Warning",
            114272:"Generating Set Breaker Failure to Open",
            114368:"Utility Breaker Failure to Close",
            114336:"Utility Breaker Failure to Open",
            114304:"Generating Set Breaker Failure to Close",
            114400:"Utility to Generating Set Transfer Failure Shutdown",
            114415:"Utility to Generating Set Transfer Failure Warning",
            114447:"Generating Set to Utility Transfer Failure Warning",
            114495:"Loss of Utility",
            114239:"Earth Fault",
            114271:"Earth Leakage",
            114496:"Generating Set to Bus Synchronization Failure Shutdown",
            114560:"Generating Set Soft Unload Failure Shutdown",
            114559:"Generating Set to Bus Phase Sequence Mismatch Warning",
            114789:"Ether Hold Relay Open Circuit",
            114821:"Ether Start Relay Open Circuit",
            114853:"Pre-Lube Relay Open Circuit",
            114790:"Ether Hold Relay Short Circuit",
            114822:"Ether Start Relay Short Circuit",
            114854:"Pre-Lube Relay Short Circuit",
            114511:"Generating Set to Bus Synchronization Failure Warning",
            114575:"Generating Set Soft Unload Failure Warning",
            114603:"SCADA Data Link Fault",
            128031:"Air Damper Closed",
            128063:"ATS in Normal Position",
            128095:"ATS in Emergency Position",
            128127:"Battery Charger Failure",
            128159:"Generating Set Breaker Closed",
            128191:"Utility Breaker Closed",
            128223:"Engine in Cooldown",
            128255:"Generator Control Not in Automatic Warning",
            128319:"Generator Breaker Failure to Open",
            128351:"Utility Breaker Failure to Open",
            128383:"Generator Breaker Failure to Close",
            128415:"Utility Breaker Failure to Close",
            128447:"Generating Set Circuit Breaker Open",
            128479:"Utility Breaker Open",
            128480:"Utility to Generator Transfer Failure Shutdown",
            128495:"Utility to Generator Transfer Failure Warning",
            128527:"Generator to Utility Transfer Failure Warning ",
            128575:"Loss of Utility",
            128607:"Generating Set Breaker Locked Out",
            128639:"Utility Breaker Locked Out",
            128927:"Earth Fault",
            128959:"Earth Leakage"
        }
        return alarmTxt

#dm = DataManager(None)
#dm.init("/dev/ttyUSB1")
#print(dm.setOnOff(1))
#print(dm.getData())
#print(dm.getAlarm())
#print(dm.getData())
