
import sys, os
import serial, time


class DataManager:
        def __init__(self, log):
                self.log = log
        def init(self, serialPort):
                try:
                        self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0)
                        print('Connected to: ' + serialPort)
                        return True
                except:
                        # self.log.error("XE05 Can't enable serial port\n")
                        print('Serial error')
                        return 'XE05'
        def readSerial(self, bytecount):
                ret = ""
                timeout = 2
                while timeout > 0:
                        if self.ser.inWaiting(): # serial huleelt
                                try:
                                        b = self.ser.read(1).decode() # serial read zaaval decode hiih yostoi
                                        ret += b
                                        bytecount = bytecount - 1
                                except Exception as ex:
                                        print('serial error')
                                if(bytecount==0):
                                        return ret
                        else:
                                time.sleep(1)
                                timeout = timeout - 1
                return "TIMED_OUT"
# =========================== GET =======================
        def getData(self):
                # inWait() here
                self.ser.flushOutput() # serialiin read buffer-iig tseverleh
                self.ser.flushInput()
                #self.ser.write('\x00'.encode())
                val = (0).to_bytes(1, byteorder='big')
                print (val)
                self.ser.write(val)
                time.sleep(1)
                # self.ser.read(12)
                ret = []
                for i in range(1, 13):
                        try:
                                tagName = 'sens' + str(i)
                                tagValue = ord(self.readSerial(1))
                                ret.append((tagName, tagValue))
                        except:
                               tagName = 'sens' + str(i)
                               tagValue = 'ERROR_ENV_RSE'
                               ret.append((tagName, tagValue))
                return ret

        def set(self, tag, value):
                # inWait() here
                self.ser.flushOutput() # serialiin read buffer-iig tseverleh
                self.ser.flushInput()
                ret = []
                if(tag == "fire"):
                        ret.append((tag, self.setFire(value)))
                elif(tag == "ac"):
                        ret.append((tag, self.setAC(value)))
                else:
                        ret.append((tag, "E01"))
                return  ret

        def setFire(self, value):
                self.ser.flushOutput()
                self.ser.flushInput()
                buffer = 0
                trybuffer = 3
                while trybuffer>0:
                        self.ser.write('\x22'.encode())
                        if(value == 0):
                                self.ser.write('\x01'.encode())
                        elif(value == 1):
                                self.ser.write('\x00'.encode())
                        time.sleep(1)
                        try:
                                buffer = self.readSerial(1)
                        except:
                                buffer = "TIMED_OUT"
                        print("readfinish")
                        print(buffer)
                        if(buffer=='TIMED_OUT'):
                                trybuffer-=1
                        else:
                                trybuffer=0

                if	buffer=='\x01':
                        return "OK"
                elif buffer=='TIMED_OUT':
                        return "E01"
                else:
                        return 'E02'


        def setAC(self, value):
                buffer = 0
                trybuffer = 3
                while trybuffer>0:
                        self.ser.write('\x21'.encode())
                        if(value == 0):
                                self.ser.write('\x01'.encode())
                        elif(value == 1):
                                self.ser.write('\x00'.encode())
                        time.sleep(0.1)
                        try:
                                buffer = self.readSerial(1)
                        except:
                                buffer = "TIMED_OUT"
                        print("readfinish")
                        print(buffer)
                        if(buffer=='TIMED_OUT'):
                                trybuffer-=1
                        else:
                                trybuffer=0

                if	buffer=="\x01":
                        return "OK"
                elif buffer=='TIMED_OUT':
                        return "E01"
                else:
                        return 'E02'
#test = DataManager(None)
#test.init('/dev/ttyUSB7')
#print (test.set("AC","ON"))
#print(test.getData())

