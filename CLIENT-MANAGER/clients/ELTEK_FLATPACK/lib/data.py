import sys, os, serial, time
from multiprocessing import Process
import struct, binascii
import string

buffSize = 5

conf = (b'\x01\xA2\x80\x02\x01\x00\x02\xE5\xC2')
cons = (b'\x01\xA5\x80\x02\x01\x00\x02\x52\xC3')
cont = (b'\x01\xC9\x80\x02\x01\x00\x02\x3E\xCA')
confo = (b'\x01\xCA\x80\x02\x01\x00\x02\x0D\xCA')
confi = (b'\x01\xCB\x80\x02\x01\x00\x02\xDC\xCB')
com = (b'\x01\xA7\x80\x02\x01\x00\x02\xB0\xC2')
#equ = (b'\x01\xBD\x80\x02\x01\x14\x02\x15\xE0\x11\xF8\x10\xCC\x00\x00\x15\x18\x16\x44\x16\xA8\x00\x00\x13\x88\x17\x70\x25\x6A')

class DataManager:

	def __init__(self, log):
		self.log = log
		self.equ =[0x01, 0xBD, 0x80, 0x02, 0x01, 0x14, 0x02, 0x16, 0x44, 0x11, 0xF8, 0x10, 0xCC, 0x00, 0x00, 0x16, 0xA8, 0x15, 0x18, 0x16, 0xA8, 0x00, 0x00, 0x13, 0x88, 0x17, 0x70, 0x00,0x00]

	def init(self, serialPort):
		#try:
		self.ser = serial.Serial(port=serialPort, baudrate=9600)
		print(self.ser.portstr)
		if self.ser.isOpen():
			print("AlreadyOpened")
			self.ser.close()
			print("NowClosed")
			self.ser.open()
			print("NowOpened")
		self.ser.isOpen()
		#self.startByte = '\x01'
		self.con()
		return True
		#except:
		#	self.log.error("Can't enable serial port\n")
		#	sys.stderr.write("Can't enable serial port\n")
		#	return False
	def CRC(self, sendD):
		result = []
		result = sendD
		crcint = 0xffff
		pos = 0
		lmt = 27
		for pos in range(27):
			crcint ^= sendD[pos]
			for cr in range(8):
				if crcint & 0x0001:
					crcint >>= 1
					crcint ^= 0xA001

				else:
					crcint = crcint >> 1
		result[27] = crcint >> 8
		result[28] = crcint & 0x00ff
		return result

	def Convert(self, convert):
		ret = ord(convert)
		#print (ret)
		#print ("za")
		return ret

	def convertByteToInt(self, arr):
		result = []
		for idx in range(len(arr)):
			try:
				result.append(struct.unpack('B', arr[idx])[0])
			except:
				result.append(0)
				print("Convert error")
		return result

	def readSerial(self, timeout, len):
		ret = []
		cnt = 0
		while timeout > 0:
			 if cnt >= len:
				 return self.convertByteToInt(ret)
			 if self.ser.inWaiting():
				 try:
						 b = self.ser.read(1)
						 #print('read byte', b)
				 except Exception as ex:
						 b = 0
						 print('serial error decode')
						 continue
				 ret.append(b)
				 cnt += 1
			 else:
				 print('TIMED_OUT')
				 time.sleep(1)
				 timeout = timeout - 1
		return "TIMED_OUT"
	def con(self):
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(conf)
		time.sleep(1)
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(cons)
		time.sleep(1)
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(cont)
		time.sleep(1)
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(confo)
		time.sleep(1)
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(confi)
		time.sleep(1)
		return 0
	def get(self):
		retData = []
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(com)
		time.sleep(0.1)
		buffer = self.readSerial(5, 30)
		if buffer == "TIMED_OUT":
			return buffer
		else :
		#buffer[13]
			tag = "dcouVol"
			a = buffer[15]#self.Convert(buffer[15])
			b = buffer[16]#self.Convert(buffer[16])
			val = (a*256+b)*0.01
			retData.append((tag, val))

			tag = "dcouCur"
			a = buffer[13]#self.Convert(buffer[13])
			b = buffer[14]#self.Convert(buffer[14])
			val = (a*256+b)*0.1
			retData.append((tag, val))

			tag = "batc1"
			a = buffer[17]#self.Convert(buffer[17])
			b = buffer[18]#self.Convert(buffer[18])
			val = (a*256+b)*0.1
			retData.append((tag, val))

			tag = "loadCur"
			a = buffer[19]#self.Convert(buffer[19])
			b = buffer[20]#self.Convert(buffer[20])
			val = (a*256+b)*0.1
			retData.append((tag, val))

			tag = "batt1"
			a = buffer[21]#self.Convert(buffer[21])
			b = buffer[22]#self.Convert(buffer[22])
			val = (a*256+b)*0.01
			retData.append((tag, val))

			val = ""
			#1 dehi heseg
			#bit 0
			chek = buffer[26]%2#self.Convert(buffer[26])%2
			if chek == 1:
				val += "Bat Pre Alarm|"
			datab = buffer[26]/2#self.Convert(buffer[26])/2
			#bit 1
			chek = datab%2
			if chek == 1:
				val += "Bat Fault|"
			datab = datab/2
			#bit 2
			datab = datab/2
			#bit 3
			datab = datab/2
			#bit 4
			chek = datab%2
			if chek == 1:
				val += "Loud Fuse|"
			datab = datab/2
			#bit 5
			chek = datab%2
			if chek == 1:
				val += "Symmetry|"
			datab = datab/2
			#bit 6
			chek = datab%2
			if chek == 1:
				val += "High 1|"
			datab = datab/2
			#bit 7
			chek = datab%2
			if chek == 1:
				val += "High 2|"

			# 2dahi heseg
			#bit 0
			datab = buffer[27]/2#self.Convert(buffer[27])/2
			#bit 1
			chek = datab%2
			if chek == 1:
				val += "High Bat|"
			datab = datab/2
			#bit 2
			chek = datab%2
			if chek == 1:
				val += "Low Bat|"
			datab = datab/2
			#bit 3
			chek = datab%2
			if chek == 1:
				val += "LVD|"
			datab = datab/2
			#bit 4
			chek = datab%2
			if chek == 1:
				val += "Bat Fuse|"
			datab = datab/2
			#bit 5
			chek = datab%2
			if chek == 1:
				val += "Rectifier|"
			datab = datab/2
			#bit 6
			chek = datab%2
			if chek == 1:
				val += "Mains|"
			datab = datab/2
			#bit 7
			chek = datab%2
			if chek == 1:
				val += "Bat Test|"

			if len(val) > 0:
				val = val[:-1]
				retData.append(("alarm", val))

			return retData
	def setFCHV(self, volt):
		val = float(volt)
		buh = int(val/2.56)
		self.equ [15] = buh
		self.equ [16] = int((val-(float(buh)*2.56))*100)
		self.equ = self.CRC(self.equ)
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(self.equ)

	def setECHV(self, volt):
		val = float(volt)
		buh = int(val/2.56)
		self.equ [17] = buh
		self.equ [18] = int((val-(float(buh)*2.56))*100)
		self.equ = self.CRC(self.equ)
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(self.equ)

	def set(self, tag, new_value):
		if tag == "fchv":
			return self.setFCHV(new_value)
		elif tag=="bchv":
			ans=self.setBCHV(new_value)
			return ans

	def getData(self):
		ret =[]
		print("get data")
		ret = self.get()
		print (ret)
		print('Done')
		return ret

#test = DataManager(None)
#test.init('/dev/ttyUSB6')
#test.getData()
#test.setFCHV(50)
#test.setECHV(60)
