import sys, os
import serial, time

# chosenData = [("\xF0\x41\x7E".encode(), '0a', 'genaVol'),
# 			  ("\xF0\x42\x7E".encode(), '0b', 'genbVol'),
# 			  ("\xF0\x43\x7E".encode(), '0c', 'gencVol'),
# 			  ("\xF0\x44\x7E".encode(), 'ab', 'genabVol'),
# 			  ("\xF0\x45\x7E".encode(), 'ac', 'genacVol'),
# 			  ("\xF0\x46\x7E".encode(), 'bc', 'genbcVol'),
# 			  ("\xF0\x47\x7E".encode(), '0a', 'genaCur'),
# 			  ("\xF0\x48\x7E".encode(), '0b', 'genbCur'),
# 			  ("\xF0\x49\x7E".encode(), '0c', 'gencCur'),
#
# 			  ("\xF0\x51\x7E".encode(), '51', 'freq'),
# 			  ("\xF0\x52\x7E".encode(), '52', 'batVolt'),
# 			  ("\xF0\x53\x7E".encode(), '53', 'motorRuntime')]
#
# chosenDataSet = [('\xF0\x54\x7E'.encode(), 2),
# 				 ('\xF0\x55\x7E'.encode(), 0),
# 				 ('\xF0\x56\x7E'.encode(), 1),
# 				 ('\xF0\x57\x7E'.encode(), 0),
# 				 ('\xF0\x58\x7E'.encode(), 1)]

msg_getAlldata =   (15745150).to_bytes(3, byteorder='big')
msg_alarm =        (15749246).to_bytes(3, byteorder='big')
msg_getDisplay =   (15753342).to_bytes(3, byteorder='big')
msg_butSupsel =    (1032671330686).to_bytes(5, byteorder='big')
msg_butStop =      (1032671396222).to_bytes(5, byteorder='big')
msg_butStart =     (1032671461758).to_bytes(5, byteorder='big')
msg_butPhase =     (1032671527294).to_bytes(5, byteorder='big')
msg_butSelect =    (1032671592830).to_bytes(5, byteorder='big')
msg_butManAuto =   (1032671658366).to_bytes(5, byteorder='big')

# chosen_On_Off_Auto = [('1', '0', 0),('0', '1', 1),('1', '0', 2)]
class DataManager:
	def __init__(self, log):
		self.log = log
	def init(self, serialPort):
		try:
			self.ser = serial.Serial(port=serialPort, baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0)
			print('Serial port connected')
			return True
		except:
			print('Serial can''t open')
			self.log.error("Can't enable serial port\n")
			return 'XE01'
	def dataConvertToInt(self, inp):
		try:
			temp = int(inp[4:6],16)
			# ter = temp[2:].zfill(8)
		except:
			try:
				if '\\n' in inp:
					temp = int('0a',16)
					# ter = temp[2:].zfill(8)
				else:
					temp = int(format(ord(inp[2:3]),'x'),16)
					# ter = temp[2:].zfill(8)
			except:
				ter = 'XE04'
				self.log.error("dataConvertToInt error\n")
		return temp
	def dataConvertToBin8(self, inp):
		try:
			temp = bin(int(inp[4:6],16))
			ter = temp[2:].zfill(8)
		except:
			try:
				if '\\n' in inp:
					temp = bin(int('0a',16))
					ter = temp[2:].zfill(8)
				else:
					temp = bin(int(format(ord(inp[2:3]),'x'),16))
					ter = temp[2:].zfill(8)
			except:
				self.log.error("dataConvertToBin8 error\n")
				ter = 'XE04'
		return ter
	def dataConvertToBin4(self, inp):
		try:
			temp = bin(int(inp[4:5],16))
			ter = temp[2:].zfill(4)
		except:
			try:
				if '\\n' in inp:
					temp = bin(int('0',16))
					ter = temp[2:].zfill(4)
				else:
					num = format(ord(inp[2:3]),'x')
					temp = bin(int(num[0],16))
					ter = temp[2:].zfill(4)
			except:
				self.log.error("dataConvertToBin4 error\n")
				ter = 'XE04'
		return ter

# # =========================== GET =======================
	def getAlarm(self):
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput()
		self.ser.write(msg_alarm)
		time.sleep(4)
		try:
			if self.ser.inWaiting():
				rcv = self.ser.read(1)
				if 'f0' in str(rcv):
					print('f0 detected')
				else:
					rcv = self.ser.read(1)
					if 'f0' in str(rcv):
						print('f0 detected')
					else:
						self.log.error("f0 is not found in received data\n")
						return 'XE02-01'
			else:
				return 'XE03'
			if 'Q' in str(self.ser.read(1)):
				rcv = self.ser.read(1)
				if '~' == rcv.decode():
					return 'noalarm'
				else:
					stat = ''
					while '~' != rcv.decode():
						try:
							rcv = self.ser.read(2)
							stat += '|P-' + rcv.decode()
							rcv = self.ser.read(1)
						except:
							self.log.error("data format is not recognized\n")
							return 'XE02-01'
					return ('alarm' , stat[1:])
			else:
				return 'XE02-01'
		except:
			return 'XE02-01E'
	def getData(self):
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput()
		self.ser.write(msg_getAlldata)
		time.sleep(5)
		data = [('empty', 'empty')]
		#rcv = self.ser.read(1)
		try:
			if self.ser.inWaiting():
				rcv = self.ser.read(1)
				#print(str(rcv))
				if 'f0' in str(rcv):
					a = 1
					#print('f0 detected')
				else:
					rcv = self.ser.read(1)
					if 'f0' in str(rcv):
						#print('f0 detected')
						a = 1
					else:
						self.log.error("f0 is not found in received data\n")
						return 'XE02-02'
			else:
				return 'XE03-01'
			if 'A' in str(self.ser.read(1)):
				data.append(('genaVol' , self.ser.read(3).decode()))
			if 'B' in str(self.ser.read(1)):
				data.append(('genbVol' , self.ser.read(3).decode()))
			if 'C' in str(self.ser.read(1)):
				data.append(('gencVol' , self.ser.read(3).decode()))
			if 'D' in str(self.ser.read(1)):
				data.append(('genabVol' , self.ser.read(3).decode()))
			if 'E' in str(self.ser.read(1)):
				data.append(('genbcVol' , self.ser.read(3).decode()))
			if 'F' in str(self.ser.read(1)):
				data.append(('genacVol' , self.ser.read(3).decode()))
			if 'G' in str(self.ser.read(1)):
				rcv = self.ser.read(3).decode()
				data.append(('genaCur' , rcv[0:2]+'.'+rcv[2:3])) # 10-t huvaah
			if 'H' in str(self.ser.read(1)):
				rcv = self.ser.read(3).decode()
				data.append(('genbCur' , rcv[0:2]+'.'+rcv[2:3])) # 10-t huvaah
			if 'I' in str(self.ser.read(1)):
				rcv = self.ser.read(3).decode()
				data.append(('gencCur' , rcv[0:2]+'.'+rcv[2:3])) # 10-t huvaah
			if 'J' in str(self.ser.read(1)):
				rcv = self.ser.read(3).decode()
				data.append(('freq' , rcv[0:2]+'.'+rcv[2:3])) # 10-t huvaah
			if 'K' in str(self.ser.read(1)):
				data.append(('motorRuntime' , self.ser.read(5).decode()))
			if 'L' in str(self.ser.read(1)):
				rcv = self.ser.read(3).decode()
				print(rcv)
				data.append(('batVolt' , rcv[0:2]+'.'+rcv[2:3])) # 10-t huvaah
			if 'M' in str(self.ser.read(1)):
				rcv = self.ser.read(3).decode()
				data.append(('lineIn' , rcv))
				print(rcv)
				leds = str(self.dataConvertToBin8(str(self.ser.read(1))))
				print(leds[1])
				# if leds[7]=='1':
				# 	if leds[6]=='1':
				# 		if '~'==self.ser.read(1).decode():
				# 			self.ser.flushOutput() # serialiin read buffer-iig tseverleh
				# 			self.ser.flushInput()
				# 			# self.ser.write((msg_butManAuto+'\x02\x7E').encode())
				# 			self.ser.write(msg_butManAuto)
				# 			time.sleep(3)
				# 			if self.ser.inWaiting():
				# 				rcv = self.ser.read(1)
				# 				print(rcv)
				# 				if 'f0' in str(rcv):
				# 					print('f0 detected')
				# 				else:
				# 					rcv = self.ser.read(1)
				# 					if 'f0' in str(rcv):
				# 						print('f0 detected')
				# 					else:
				# 						self.log.error("f0 is not found in received data\n")
				# 						return 'XE02-02'
				# 			else:
				# 				return 'XE03'
				# 			if 'a' in str(self.ser.read(1)):
				# 				rcv = self.ser.read(5)
				# 				# screen += rcv.decode()
				# 				# print('DISP:\n'+rcv.decode())
				# 				led1 = str(self.dataConvertToBin8(str(self.ser.read(1))))
				# 				led2 = str(self.dataConvertToBin4(str(self.ser.read(1))))
				# 				if led2[3]=='1' and led2[2]=='0':
				# 					data.append(('on_off_auto', 1))
				# 				else:
				# 					self.log.error("Auto, Manual indicators ON\n")
				# 					return 'XE06' # auto manual zereg assan
				# 			else:
				# 				self.log.error("data format is not recognized\n")
				# 				return 'XE02-02'
				# 		else:
				# 			self.log.error("data format is not recognized\n")
				# 			return 'XE02-02'
				# 	else:
				# 		# if leds[1]=='1':
				# 		# 	data.append(('on_off_auto', 2))
				# 		# else:
				# 		data.append(('on_off_auto', 1))
				# else:
				# 	if leds[6]=='1':
				# 		# data.append(('on_off_auto', 1))
				# 		if leds[1]=='1':
				# 			data.append(('on_off_auto', 2))
				# 		else:
				# 			data.append(('on_off_auto', 0))
			end = self.ser.read(1)
			#print(str(end))
			#print(len(data))
			#print(data)
			if '~'==end.decode():
				if len(data) > 12:
					data.remove(('empty', 'empty'))
					return data
				else:
					self.log.error("data format is not recognized\n")
					return 'XE02-02' # utga buruu
			else:
				self.log.error("data format is not recognized\n")
				return 'XE02-02' # utga buruu
		except:
			self.log.error("data format is not recognized\n")
			return 'XE02-02E'
	def getDispAfterButton(self, button_code):
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput()
		self.ser.write(button_code)
		try:
			time.sleep(4)
			if self.ser.inWaiting():
				rcv = self.ser.read(1)
				if 'f0' in str(rcv):
					print('f0 detected')
				else:
					rcv = self.ser.read(1)
					if 'f0' in str(rcv):
						print('f0 detected')
					else:
						return 'XE02'
			else:
				return 'XE03'
			if 'a' in str(self.ser.read(1)):
				rcv = self.ser.read(5)
				# screen += rcv.decode()
				print('DISP:\n'+rcv.decode())
				led1 = str(self.dataConvertToBin8(str(self.ser.read(1))))
				led2 = str(self.dataConvertToBin4(str(self.ser.read(1))))
				print('LEDS:\n 1 2 3 4 5 6 7 8 9 A B C')
				print(' '+led1[0]+' '+led1[1]+' '+led1[2]+' '+led1[3]+' '+led1[4]+' '+led1[5]+' '+led1[6]+' '+led1[7]+' '+led2[0]+' '+led2[1]+' '+led2[2]+' '+led2[3])

				print('1. Volt\n2. Current\n3. Frequency\n4. Runtime\n5. Bat.Volt\n6. Run\n7. Ele.Supply')
				print('8. Gen.Supply\n9. Ele.Volt\nA. Run Fault\nB. Manual\nC. Auto')
				rcv = self.ser.read(1)
				return led1,led2
		except:
			return 'XE02'
	def getDisplay(self):
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput()
		try:
			self.ser.write(msg_getDisplay)
			time.sleep(2)
			if self.ser.inWaiting():
				rcv = self.ser.read(1)
				if 'f0' in str(rcv):
					print('f0 detected')
				else:
					rcv = self.ser.read(1)
					if 'f0' in str(rcv):
						print('f0 detected')
					else:
						self.log.error("f0 is not found in received data\n")
						return 'XE02'
			else:
				self.log.error("No response from serial port\n")
				return 'XE03'
			if 'a' in str(self.ser.read(1)):
				rcv = self.ser.read(5)
				# screen += rcv.decode()
				print('DISP:\n'+rcv.decode())
				led1 = str(self.dataConvertToBin8(str(self.ser.read(1))))
				led2 = str(self.dataConvertToBin4(str(self.ser.read(1))))
				print('LEDS:\n 1 2 3 4 5 6 7 8 9 A B C')
				print(' '+led1[0]+' '+led1[1]+' '+led1[2]+' '+led1[3]+' '+led1[4]+' '+led1[5]+' '+led1[6]+' '+led1[7]+' '+led2[0]+' '+led2[1]+' '+led2[2]+' '+led2[3])

				print('1. Volt\n2. Current\n3. Frequency\n4. Runtime\n5. Bat.Volt\n6. Run\n7. Ele.Supply')
				print('8. Gen.Supply\n9. Ele.Volt\nA. Run Fault\nB. Manual\nC. Auto')
				rcv = self.ser.read(1)
				return led1,led2
		except:
			self.log.error("data format is not recognized\n")
			return 'XE02-E'
	def clickButton(self):
		key = input('[1] Select\n[2] Phase/SW\n[3] Start/Scan\n[4] Stop/CLR\n[5] Sup.Select\n[6] M/A\n[q] Quit\n>> ')
		try:
			self.ser.flushOutput() # serialiin read buffer-iig tseverleh
			self.ser.flushInput()
			while key != 'q':
				if key=='1':
					self.ser.write(msg_butSelect)
				elif key=='e':
					print(self.getData())
					key = input('[1] Select\n[2] Phase/SW\n[3] Start/Scan\n[4] Stop/CLR\n[5] Sup.Select\n[6] M/A\n[q] Quit\n>> ')
					continue
				elif key=='2':
					self.ser.write(msg_butPhase)
				elif key=='3':
					self.ser.write(msg_butStart)
				elif key=='4':
					self.ser.write(msg_butStop)
				elif key=='5':
					self.ser.write(msg_butSupsel)
				elif key=='6':
					self.ser.write(msg_butManAuto)
				elif key=='q':
					return 'Exited debug mode'
				else:
					print('Unknown code')
					self.clickButton()
				time.sleep(2)
				if self.ser.inWaiting():
					rcv = self.ser.read(1)
					if 'f0' in str(rcv):
						print('f0 detected')
					else:
						rcv = self.ser.read(1)
						if 'f0' in str(rcv):
							print('f0 detected')
						else:

							return 'XE02'
				else:
					self.log.error("No response from serial port\n")
					return 'XE03'
				if 'a' in str(self.ser.read(1)):
					rcv = self.ser.read(5)
					# screen += rcv.decode()
					print('DISP:\n'+rcv.decode())
					led1 = str(self.dataConvertToBin8(str(self.ser.read(1))))
					led2 = str(self.dataConvertToBin4(str(self.ser.read(1))))
					print('LEDS:\n 1 2 3 4 5 6 7 8 9 A B C')
					print(' '+led1[0]+' '+led1[1]+' '+led1[2]+' '+led1[3]+' '+led1[4]+' '+led1[5]+' '+led1[6]+' '+led1[7]+' '+led2[0]+' '+led2[1]+' '+led2[2]+' '+led2[3])

					print('1. Volt\n2. Current\n3. Frequency\n4. Runtime\n5. Bat.Volt\n6. Run\n7. Ele.Supply')
					print('8. Gen.Supply\n9. Ele.Volt\nA. Run Fault\nB. Manual\nC. Auto')

				key = input('[1] Select\n[2] Phase/SW\n[3] Start/Scan\n[4] Stop/CLR\n[5] Sup.Select\n[6] M/A\n[q] Quit\n>> ')
		except:
			return 'XE02'

	def set(self, tag, value):
		if tag=='on_off_auto':
			return self.setOnOffAuto(value)
		elif tag=='selSupply':
			return self.setSupply(value)
		else:
			self.log.error("Wrong tag! :"+tag+'\n')
			return 'XE07'

	def setOnOffAuto(self, value):
		self.ser.flushOutput()
		self.ser.flushInput()
		trycount = 5
		try:
			#print ('SET: ' + value)
			led1,led2 = self.getDisplay()
			# for i in range(0,3):
			if value==0:
				if led2[2]=='1' and led2[3]=='0':
					if led1[5]=='0':
						return 'OK'
					else:
						self.ser.flushOutput()
						self.ser.flushInput()
						self.ser.write(msg_butStart)
						time.sleep(5)
						led1,led2 = self.getDisplay()
						if led2[2]=='1' and led2[3]=='0' and led1[5]=='0':
							return 'OK'
						else:
							self.log.error("Unsuccessful set\n")
							return 'XE02-03'
				else:
					for i in range(0,trycount):
						self.ser.flushOutput()
						self.ser.flushInput()
						self.ser.write(msg_butManAuto)
						time.sleep(1)
						led1,led2 = self.getDisplay()
						if led2[2]=='1' and led2[3]=='0':
							if led1[5]=='0':
								return 'OK'
							else:
								self.ser.flushOutput()
								self.ser.flushInput()
								self.ser.write(msg_butStart)
								time.sleep(1)
								led1,led2 = self.getDisplay()
								if led2[2]=='1' and led2[3]=='0' and led1[5]=='0':
									return 'OK'
								else:
									self.log.error("Unsuccessful set\n")
									return 'XE02-03'
					return 'XE02-03'
			elif value==2:
				if led2[2]=='1' and led2[3]=='0':
					if led1[5]=='1':
						return 'OK'
					else:
						self.ser.flushOutput()
						self.ser.flushInput()
						self.ser.write(msg_butStart)
						time.sleep(3)
						led1,led2 = self.getDisplay()
						if led2[2]=='1' and led2[3]=='0' and led1[5]=='1':
							return 'OK'
						else:
							self.log.error("Unsuccessful set\n")
							return 'XE02-03'
				else:
					for i in range(0,trycount):
						self.ser.flushOutput()
						self.ser.flushInput()
						self.ser.write(msg_butManAuto)
						time.sleep(1)
						led1,led2 = self.getDisplay()
						if led2[2]=='1' and led2[3]=='0':
							if led1[5]=='1':
								return 'OK'
							else:
								self.ser.flushOutput()
								self.ser.flushInput()
								self.ser.write(msg_butStart)
								time.sleep(1)
								led1,led2 = self.getDisplay()
								if led2[2]=='1' and led2[3]=='0' and led1[5]=='1':
									return 'OK'
								else:
									self.log.error("Unsuccessful set\n")
									return 'XE02-03'
					return 'XE02-03'
			elif value==1:
				if led2[2]=='0' and led2[3]=='1':
					return 'OK'
				else:
					for i in range(0,trycount):
						self.ser.flushOutput()
						self.ser.flushInput()
						self.ser.write(msg_butManAuto)
						time.sleep(1)
						led1,led2 = self.getDisplay()
						if led2[2]=='0' and led2[3]=='1':
							return 'OK'
					self.log.error("Unsuccessful set\n")
					return 'XE02-03'
			else:
				self.log.error("Wrong value to set : "+ str(value) +'\n')
				return 'XE08'
		except:
			self.log.error("data format is not recognized\n")
			return 'XE02-03E'

	def setSupply(self, value):
		self.ser.flushOutput()
		self.ser.flushInput()
		try:
			led1,led2 = self.getDisplay()
			if not(led2[2]=='1' and led2[3]=='0'):
				self.log.error("Unsuccessful set: This setting is not possible when KIPOR in auto mode.\n")
				return 'XE09'
			else:
				if led1[6]=='1':
					if value==1:
						self.ser.write(msg_butSupsel)
					else:
						return 'OK'
				elif led1[7]=='1':
					if value==0:
						self.ser.write(msg_butSupsel)
					else:
						return 'OK'
				time.sleep(2)
				if self.ser.inWaiting():
					rcv = self.ser.read(1)
					print(rcv)
					if 'f0' in str(rcv):
						print('f0 detected')
					else:
						rcv = self.ser.read(1)
						if 'f0' in str(rcv):
							print('f0 detected')
						else:
							self.log.error("f0 is not detected in received data\n")
							return 'XE02-04'
				else:
					self.log.error("No response from serial port\n")
					return 'XE03'
				if 'a' in str(self.ser.read(1)):
					rcv = self.ser.read(5)
					# screen += rcv.decode()
					# print('DISP:\n'+rcv.decode())
					led1 = str(self.dataConvertToBin8(str(self.ser.read(1))))
					led2 = str(self.dataConvertToBin4(str(self.ser.read(1))))
					if value==0:
						if led1[6]=='1':
							return 'OK'
						else:
							self.log.error("Unsuccessful set\n")
							return 'XE10'
					else:
						if led1[7]=='1':
							return 'OK'
						else:
							self.log.error("Unsuccessful set\n")
							return 'XE10'
				else:
					self.log.error("Data format is not recognized\n")
					return 'XE02-04'
		except:
			self.log.error("Unsuccessful set\n")
			return 'XE02-04E'
# ========================== END =============================
#test = DataManager(None)
#test.init('/dev/ttyUSB4')
#print(test.setOnOffAuto(2)) # o manual off manual deer 1 auto mode 2 manual der tawiad on mode
# print(test.GeneralGet())


#print(test.getDisplay())



#print (test.getData())
#test.clickButton()


# print(test.getDisplay())
#print (test.getData())
# time.sleep(1)

# print(test.getDisplay())
# print (test.getData())
# time.sleep(1)

# print(test.getDisplay())
# print (test.getData())
# time.sleep(1)
#print(test.set('selSupply',1))
