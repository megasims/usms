import time
import socket
import sys, os
import sqlite3
import logging
import threading
import importlib.machinery
from logging.handlers import RotatingFileHandler

from .lib.data import DataManager

from .lib.core import Tag
from .lib.core import Socket
from .lib.core import DataHandler
from .lib.core import SerialHandler

from .lib.decorator import Decorator

class App:
	tags = []
	worker = None
	def __init__(self, clientID, manager, listen):
		self.id = clientID
		self.manager = manager
		self.listen = listen
		self.running = False
		self.serialPort = None

		log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
		CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
		logFile = CURRENT_DIR + '/log/log.txt'
		log_handler = RotatingFileHandler(logFile, mode='a', maxBytes=5*1024, backupCount=2, encoding=None, delay=0)
		log_handler.setFormatter(log_formatter)
		log_handler.setLevel(logging.DEBUG)
		self.log = logging.getLogger('root')
		self.log.setLevel(logging.DEBUG)
		self.log.addHandler(log_handler)

		self.sqlConn = sqlite3.connect(CURRENT_DIR + '/init/local.db')

		self.clientID = clientID
		Tag.intervalStep = 10
		self.isBusy = False

	def getData(self):
		while self.running:
			cnt = 0
			while self.isBusy:
				time.sleep(1)
				cnt = cnt + 1
				if cnt == 1000:
					self.running = False
					sys.stderr.write("BUSY!\n")
					self.log.error('BUSY')
					break

			self.serialHandler.onYellow()
			self.isBusy = True
			#self.dataHandler.handle(self.dataManager.getTestData())
			self.dataHandler.handle(self.dataManager.getData())
			self.isBusy = False
			self.serialHandler.offYellow()
			#time.sleep(self.dataInterval)
		self.serialHandler.offGreen()
		sys.stderr.write("getData thread is stopped!\n")
		self.running = False

	def intervalEvent(self):
		while self.running:
			self.dataHandler.timeInterval()
			time.sleep(Tag.intervalStep)
		sys.stderr.write("Interval event listner is stopped!\n")
		self.running = False

	def main(self):
		try:
			mainSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			mainSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			mainSocket.bind(self.listen)
			mainSocket.listen(10)
			while self.running:
				conn, addr = mainSocket.accept()
				cmd = conn.recv(128).decode();
				cmd = cmd.strip()
				res = None
				
				if cmd == 'STOP':
					self.running = False
					res = 'OK'
					sys.stderr.write('Stop command\n');
					self.log.info('Stop command\n')
				elif cmd[:3] == 'GET':
					temp = cmd.split("\t")
					if (len(temp) < 2):
						res = 'ERROR_GET_COMMAND_FORMAT'
						self.log.error('ERROR_GET_COMMAND_FORMAT\n')
						sys.stderr.write('ERROR_GET_COMMAND_FORMAT\n')
					else:
						res = self.dataHandler.getTagValue(temp[1])
						if res == None:
							cnt = 0
							while self.isBusy:
								time.sleep(1)
								cnt = cnt + 1
								if cnt == 6:
									break
							if cnt == 6:
								res = 'BUSY'
							else:
								self.serialHandler.onYellow()
								self.isBusy = True
								res = self.dataHandler.get(temp[1])
								self.isBusy = False
								self.serialHandler.offYellow()

							res = 'ERROR_GET_COMMAND_INVALID_TAG\n'
							self.log.error('ERROR_GET_COMMAND_INVALID_TAG\n')
							sys.stderr.write('ERROR_GET_COMMAND_INVALID_TAG\n')

				elif cmd[:3] == 'SET':
					temp = cmd.split("\t")
					if (len(temp) < 3):
						res = 'ERROR_SET_COMMAND_FORMAT'
						self.log.error('ERROR_SET_COMMAND_FORMAT\n')
						sys.stderr.write('ERROR_SET_COMMAND_FORMAT\n')
					else:
						tagName = self.dataHandler.getTagName(temp[2])
						if tagName == None:
							res = 'ERROR_SET_COMMAND_INVALID_TAG\n'
							self.log.error('ERROR_SET_COMMAND_INVALID_TAG\n')
							sys.stderr.write('ERROR_SET_COMMAND_INVALID_TAG\n')
						else:
							cnt = 0
							while self.isBusy:
								time.sleep(1)
								cnt = cnt + 1
								if cnt == 6:
									break
							if cnt == 6:
								res = 'BUSY'
							else:
								self.isBusy = True
								try:
									val = int(temp[3])
								except ValueError:
									try:
										val = float(temp[3])
									except:
										val = 'WRONG_VALUE'

								if val == 'WRONG_VALUE':
									res = val
								else:
									res = self.dataManager.set(tagName, val)
								self.isBusy = False

				elif cmd == 'ALIVE':
					res = 'OK'
				else:
					res = 'UNKNOWN_COMMAND'
					self.log.error('UNKNOWN_COMMAND: ' + res + '\n')
					sys.stderr.write('UNKNOWN_COMMAND: ' + res + '\n')

				res = res.strip() + "\n"
				conn.send(res.encode())
				conn.close()
			mainSocket.close()
		except:
			sys.stderr.write('main()' + str(sys.exc_info()[0]) + '\n')
			self.log.error('main()' + str(sys.exc_info()[0]) + '\n')

	def run(self):
		self.running = True
		try:
			getDataThread = threading.Thread(target = self.getData)
			getDataThread.start()

			intervalEventThread = threading.Thread(target = self.intervalEvent)
			intervalEventThread.start()

			mainThread = threading.Thread(target = self.main)
			mainThread.start()
			return (True, None)
		except:
			return (False, str(sys.exc_info()[0]))
		
		
	def start(self):
		try:
			manager = Socket(self.manager, self.log)
			cfgString = manager.get("CLIENT\tGETSTARTUP\t" + str(self.id))

			decorator = Decorator(self.log)
			if not decorator.decorate():
				sys.stderr.write('Decorator Error!\n')
				return (False, "Docorator Error!")
			if cfgString == None:
				sys.stderr.write('Server is not working!\n')
				return (False, "Can't connect to server!")
			if not self.readConfig(cfgString):
				sys.stderr.write("Can't recognize configuration!\n")
				return (False, "Can't recognize configuration!!!")
			else:
				self.dataHandler = DataHandler(self.tags, self.log)
				self.dataManager = DataManager(self.log)
				self.serialHandler = SerialHandler(self.serialPort, self.log)
				if self.dataManager.init(self.serialPort):
					self.serialHandler.onGreen()
					return self.run()
				else:
					return (False, "Can't enable SERIAL PORT!")
		except:
			return (False, str(sys.exc_info()[0]))

	def readConfig(self, cfgString):
		try:
			print (cfgString)
			CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
			sys.path.append(os.path.dirname(CURRENT_DIR))
			conn = sqlite3.connect(CURRENT_DIR + '/init/local.db')
			c = conn.cursor()
			c.execute('CREATE TABLE IF NOT EXISTS tags (id INTEGER, value text)')
			cfgString = str(cfgString)
			arr = cfgString.split("\n")
			for i in range(len(arr)):
				arr[i] = arr[i].strip()

			temp = arr[0].split("\t")
			workerHost = (temp[0].strip(), int(temp[1].strip()))
			self.serialPort = temp[2]
			print (self.serialPort)
			self.dataInterval = int(temp[3])

			for i in range(1, len(arr)):
				temp = arr[i].split("\t")
				oldValue = None
				exist = False
				for row in c.execute('SELECT * FROM tags WHERE id = ' + temp[0]):
					if row[1] != 'None':
						oldValue = row[1]
					exist = True
				if not exist:
					c.execute("INSERT INTO tags(id, value) VALUES (%d, 'None')" %(int(temp[0])))

				t = Tag(self.clientID, temp, oldValue, workerHost, self.log)
				if t.id != None:
					self.tags.append(t)
				else:
					conn.commit()
					c.close()
					return False
			conn.commit()
			c.close()
			return True
		except:
			e = sys.exc_info()[0]
			self.log.error(str(e) + "\n")
			sys.stderr.write(str(e) + "\n")
			return False
