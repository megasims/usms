import sys, os
import serial, time
import binascii
import array
import struct
import string

class DataManager:
        def __init__(self, log):
                self.getAC = [0x55]
                self.log = log
        def init(self, serialPort):
                try:
                        self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0)
                        print('Connected to: ' + serialPort)
                        return True
                except:
                        # self.log.error("XE05 Can't enable serial port\n")
                        print('Serial error')
                        return 'XE05'

        def readSerial(self, bytecount):
                ret = ""
                timeout = 2
                while timeout > 0:
                        if self.ser.inWaiting(): # serial huleelt
                                try:
                                        b = self.ser.read(1).decode() # serial read zaaval decode hiih yostoi
                                        ret += b
                                        bytecount = bytecount - 1
                                except Exception as ex:
                                        print('serial error')
                                if(bytecount==0):
                                        return ret
                        else:
                                time.sleep(1)
                                timeout = timeout - 1
                return "TIMED_OUT"
        def readSerial(self, timeout, len):
                ret = []
                cnt = 0
                print("readSerial")
                while timeout > 0:
                    if cnt >= (len-1):
                        return self.convertByteToInt(ret)
                        print("Timeout")
                    if self.ser.inWaiting():  # serial huleelt
                        print("inWaiting")
                        try:
                                b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                                print('read byte', b)
                        except Exception as ex:
                                b = 0
                                print('serial error decode')
                                continue
                        ret.append(b)
                        cnt += 1
                    else:
                                print('READSERIAL:TIMED_OUT')
                                time.sleep(1)
                                timeout = timeout - 1
                return "TIMED_OUT"


        def convertByteToInt(self, arr):
                result = []
                for idx in range(len(arr)):
                        try:
                            result.append(struct.unpack('B', arr[idx])[0])
                        except:
                            result.append(0)
                            print("Convert error")
                return result

        def getData(self):
                self.ser.flushOutput() # serialiin read buffer-iig tseverleh
                self.ser.flushInput()
                temp = array.array('B', self.getAC).tostring()
                print ("Write: ",temp)
                self.ser.write(temp)
                time.sleep(1)
                #print(self.ser.read(12))
                try:
                    ret = []
                    a = self.readSerial(5,24)
                    ret.append(("PhaseA",str(a[7])))
                    ret.append(("PhaseB",str(a[15]+60)))
                    ret.append(("PhaseC",str(a[22])))
                    print(ret)
                    return ret
                except:
                        return "XE05"

#test = DataManager(None)
#test.init('/dev/ttyUSB1')
#print (test.getData())
#print(test.getData())

