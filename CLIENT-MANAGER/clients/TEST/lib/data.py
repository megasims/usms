import sys, os

class DataManager:

	def __init__(self, log):
		self.log = log
	
	def init(self, serialPort):
		return True
		#try:
		#	self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0)
		#	return True
		#except:
		#	self.log.error("Can't enable serial port\n")
		#	sys.stderr.write("Can't enable serial port\n")
		#	return False

	def getTemp(self):
		#self.ser.read()
		pass

	def getAlarm(self):
		try:
			return 'Alarm 1|Alarm 2|Alarm 3'
		except:
			self.log.error("Can't get alarm!")
			return 'ERROR101'

	def getOnOff(self):
		return 0

	def get(self, tag):
		pass
		#...etc...

	def set(self, tag, value):
		return 'OK'
	
	def getData(self):
		ret = [('on_off', 1), ('temp', 18), ('alarm', 'alarm-1|alarm-2|alarm-3')]
		#ret.append(('alarm', )
		#ret.append(('temp', 18))
		#ret.append(('alarm', 'str1|str2|str3|str4'))
		return ret;

	def getTestData(self):
		ret = []
		try:
			CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
			with open(CURRENT_DIR + "/../init/test.txt", "r") as content_file:
				content = content_file.read()
			content_file.close()

			content = content.strip()
			
			alarmString = ''
			arr = content.split("\n")
			for i in range(len(arr)):
				temp = arr[i].split("\t")
				if temp[0] == 'alarm':
					alarmString = alarmString + "|" + temp[1]
				else:
					ret.append((temp[0], temp[1]))
			ret.append(('alarm', alarmString[1:]))
		except:
			self.log.error('Data:getTest')
			sys.stderr.write("Data:getTest")
		return ret
