import sys, os, serial, time
from multiprocessing import Process
import struct, binascii
import string

Get_AlarmCommInfo    ="\x7E\x32\x30\x30\x31\x34\x30\x34\x34\x45\x30\x30\x32\x30\x30\x46\x44\x33\x41\x0D\r".encode()
Get_ModuleCurr       ="\x7E\x32\x30\x30\x31\x34\x31\x34\x31\x30\x30\x30\x30\x46\x44\x42\x33\x0D\r".encode()
Get_ModuleState      ="\x7E\x32\x30\x30\x31\x34\x31\x34\x33\x30\x30\x30\x30\x46\x44\x42\x31\x0D\r".encode()
Get_ModuleAlarm      ="\x7E\x32\x30\x30\x31\x34\x31\x34\x34\x30\x30\x30\x30\x46\x44\x42\x30\x0D\r".encode()
Get_DC               ="\x7E\x32\x30\x30\x31\x34\x32\x34\x31\x30\x30\x30\x30\x46\x44\x42\x32\x0D\r".encode()
Get_Alarm2           ="\x7E\x32\x30\x30\x31\x34\x32\x34\x34\x30\x30\x30\x30\x46\x44\x41\x46\x0D\r".encode()
Get_AcquisitiononPar ="\x7E\x32\x30\x30\x31\x34\x32\x34\x36\x30\x30\x30\x30\x46\x44\x41\x44\x0D\r".encode()
Get_AC               ="\x7E\x32\x30\x30\x31\x34\x30\x34\x31\x45\x30\x30\x32\x30\x30\x46\x44\x33\x44\x0D\r".encode()
Set_FCHV             ="\x7E\x32\x30\x30\x31\x34\x32\x34\x38\x36\x30\x30\x41\x45\x30\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0D\r".encode()
Set_ECHV             ="\x7E\x32\x30\x30\x31\x34\x32\x34\x38\x36\x30\x30\x41\x45\x31\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0D\r".encode()
Set_TSTV             ="\x7E\x32\x30\x30\x31\x34\x32\x34\x38\x36\x30\x30\x41\x45\x32\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0D\r".encode()
Set_Mode_FILL		 ="\x7E\x32\x30\x30\x31\x34\x31\x34\x35\x43\x30\x30\x34\x31\x30\x46\x44\x33\x37".encode()
Set_Mode_FLOAT		 ="\x7E\x32\x30\x30\x31\x34\x31\x34\x35\x43\x30\x30\x34\x31\x46\x46\x44\x32\x31".encode()
Set_Mode_TEST		 ="\x7E\x32\x30\x30\x31\x34\x31\x34\x35\x43\x30\x30\x34\x31\x31\x46\x44\x33\x36".encode()
Set_Mode_OPENRECT	 ="\x7E\x32\x30\x30\x31\x34\x31\x34\x35\x43\x30\x30\x34\x32\x30\x46\x44\x33\x36".encode()
Set_Mode_OFFRECT	 ="\x7E\x32\x30\x30\x31\x34\x31\x34\x35\x43\x30\x30\x34\x32\x46\x46\x44\x32\x30".encode()

start = 0
dc_volt = 17
dc_curr = 25
bat1_curr = 35
bat2_curr = 43
bat3_curr = 51
bat1_temp = 63
bat1_volt = 71
bat2_temp = 79
bat2_volt = 87
bat3_temp = 95
bat3_volt = 103
batTestDur = 127
batDisCap = 135
phaseA_volt = 17
phaseB_volt = 25
phaseC_volt = 33
ac_freq = 41
phaseA_curr = 0 #sain medehgui bna site deer ochood turshina
phaseB_curr = 0 #sain medehgui bna site deer ochood turshina
phaseC_curr = 0 #sain medehgui bna site deer ochood turshina
smr1_curr = 25
smrP_curr = 34
smr2_curr = 35
smr3_curr = 45
smr4_curr = 55
smr5_curr = 65
smr6_curr = 75
smr1_slot = 26 #online slot 2 iin ali ni online boloxiig shalgax xeregtei
smr2_slot = 36
smr3_slot = 46
smr4_slot = 56
smr5_slot = 66
smr6_slot = 76
smr1_online = 18
smr2_online = 28
smr3_online = 38
smr4_online = 48
smr5_online = 58
smr6_online = 68
smr1_alm = 18
smrP_alm = 20
smr2_alm = 22
smr3_alm = 26
smr4_alm = 30
smr5_alm = 34
smr6_alm = 38
fchv_vol = 31
echv_vol = 39
tstv_vol = 47

class DataManager:

	def __init__(self, log):
		self.log = log

	def init(self, serialPort):
		#try:
		self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
		print(self.ser.portstr)
		if self.ser.isOpen():
			print("AlreadOpened")
			self.ser.close()
			print("NowClosed")
			self.ser.open()
			print("NowOpened")
		self.ser.isOpen()
		self.startByte = '~'
		self.stopByte = '\r'
		return True
		#except:
		#	self.log.error("Can't enable serial port\n")
		#	sys.stderr.write("Can't enable serial port\n")
		#	return False

	def SETConvert(self, volt):
		ret = ""
		getBin = lambda x: x > 0 and str(bin(x))[2:] or "-" + str(bin(x))[3:]
		val = struct.unpack('L', struct.pack('f', volt))[0]
		data = "0"+str(getBin(val))
		rett = hex(int(data, 2)) # datag hexa ruu hurvuuleh
		#print(rett)
		ret = rett[8]+rett[9]+rett[6]+rett[7]+rett[4]+rett[5]+rett[2]+rett[3]  #4 byte toog zuv bairand ni oruulah ardaas ni ehleed ahlah byte ni
		return ret

	def ZTEConvert(self, buffer, sAddr):
		ret = ""
		buf = ""
		i=0
		s=0
		#print("ZTEConvert")
		for i in range(0, 8, 2): # byte-uudiig zuv bairand ni oruulna. Suuleesee ehendee bairlah yostoi
			buf+=buffer[sAddr+6-i]
			buf+=buffer[sAddr+7-i]
			#print(i)
		buff = "0x0" + buf[0] # hex toonii 0x nemj hex too bolgoh
		if int(buff,16) > 8: #hasah toog nemeh bolgoh
			s=1
			a=int(buff,16)-8
			buf=str(a)+buf[1:8]
		ret = struct.unpack(">f",struct.pack(">i",int(buf,16)))[0] # hex to floating point convert hiine
		ret = str(ret)
		if s == 1:
			ret = "-" + ret
		#print("ZTEConvertFinish")
		return ret

	def CropData(self, data):
		ret = ""
		strdata = str(data) # hex data-g string ruu hurvuulne
		sdata = strdata.split(".") # string data . - eer ni 2 salgana
		try:
			ret = sdata[0] + "." + sdata[1][0] + sdata[1][1]
		except:
			ret = sdata[0] + "." + sdata[1][0]
		return ret

	def SplitData(self, data):
		ret = ""
		strdata = str(data) # hex data-g string ruu hurvuulne
		sdata = strdata.split(", ") # string data . - eer ni 2 salgana
		return sdata

	def readSerial(self, timeout):
		ret = ''
		c=0
		start=0
		started = False
		#print("readSerial")
		while timeout > 0:
			#print("Timeout")
			#print(timeout)
			if self.ser.inWaiting(): # serial huleelt
				#print("inWaiting")
				try:
					b = self.ser.read(1).decode() # serial read zaaval decode hiih yostoi
				except Exception as ex:
					b = 0
					print('serial error')
				#print(b)
				#if(start==1):
				#	c += 1
				#	print(c)
				if b == self.startByte:# startbyte orj irseniig shalgana
					#print("startbyte")
					#start=1
					started = True
				if not started: # ehelsen esehiig shalgana
					#print("not started")
					continue
				if b == self.stopByte: #stopbyte orj irsen esehiig shalgana
					#print("stopbyte")
					return ret
				if started: # herev ehelsen bol stopbyte orj irtel buh byte-uudiig avna
					ret += b
			else:
				time.sleep(1)
				timeout = timeout - 1
		return "TIMED_OUT"

	def getDC(self):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(Get_DC)
		trybuffer = 3
		while trybuffer>0:
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		if(buffer!='TIMED_OUT'):
			ans = self.ZTEConvert(buffer,dc_volt)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,dc_curr)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat1_volt)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat1_curr)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat1_temp)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat2_volt)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat2_curr)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat2_temp)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat3_volt)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat3_curr)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,bat3_temp)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,batTestDur)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,batDisCap)
			ans = self.CropData(ans)
			ret += ans + ", "
			return ret
		else:
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			return ret

	def getAC(self):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(Get_AC)
		trybuffer = 3
		while trybuffer>0:
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		if(buffer!='TIMED_OUT'):
			ans = self.ZTEConvert(buffer,phaseA_volt)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,phaseB_volt)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,phaseC_volt)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,ac_freq)
			ans = self.CropData(ans)
			ret += ans + ", "
			return ret
		else:
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			return ret

	def getACPar(self):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(Get_AcquisitiononPar)
		trybuffer = 3
		while trybuffer>0:
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		if(buffer!='TIMED_OUT'):
			ans = self.ZTEConvert(buffer,fchv_vol)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,echv_vol)
			ans = self.CropData(ans)
			ret += ans + ", "
			ans = self.ZTEConvert(buffer,tstv_vol)
			ans = self.CropData(ans)
			ret += ans + ", "
			return ret
		else:
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			return ret

	def getSMRI(self):
		ret = ""
		P = 1
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(Get_ModuleCurr)
		trybuffer = 3
		while trybuffer>0:
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0
		
		P = ord(buffer[smrP_curr]) - 48

		if(buffer!='TIMED_OUT'):
			if(P == 0):
				print('P=0')
				ans = self.ZTEConvert(buffer,smr1_curr)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr2_curr)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr3_curr)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr4_curr)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr5_curr)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr6_curr)
				ans = self.CropData(ans)
				ret += ans + ", "
			else:
				print('P!=0')
				ans = self.ZTEConvert(buffer,smr1_curr)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr2_curr+2*P)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr3_curr+4*P)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr4_curr+6*P)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr5_curr+8*P)
				ans = self.CropData(ans)
				ret += ans + ", "
				ans = self.ZTEConvert(buffer,smr6_curr+10*P)
				ans = self.CropData(ans)
				ret += ans + ", "
			return ret
		else:
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			return ret

	def getSMRO(self):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(Get_ModuleState)
		trybuffer = 3
		while trybuffer>0:
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		if(buffer!='TIMED_OUT'):
			ret += buffer[smr1_online] + ", "
			ret += buffer[smr2_online] + ", "
			ret += buffer[smr3_online] + ", "
			ret += buffer[smr4_online] + ", "
			ret += buffer[smr5_online] + ", "
			ret += buffer[smr6_online] + ", "
			ret += buffer[smr1_slot] + ", "
			ret += buffer[smr2_slot] + ", "
			ret += buffer[smr3_slot] + ", "
			ret += buffer[smr4_slot] + ", "
			ret += buffer[smr5_slot] + ", "
			ret += buffer[smr6_slot] + ", "
			return ret
		else:
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			return ret

	def getSMRA(self):
		ret = ""
		P = 1
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		self.ser.write(Get_ModuleAlarm)
		trybuffer = 3
		while trybuffer>0:
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		P = ord(buffer[smrP_alm]) - 48

		if(buffer!='TIMED_OUT'):
			if(P == 0):
				ret += buffer[smr1_alm] + ", "
				ret += buffer[smr2_alm] + ", "
				ret += buffer[smr3_alm] + ", "
				ret += buffer[smr4_alm] + ", "
				ret += buffer[smr5_alm] + ", "
				ret += buffer[smr6_alm] + ", "
			else:
				ret += buffer[smr1_alm] + ", "
				ret += buffer[smr2_alm+2*P] + ", "
				ret += buffer[smr3_alm+4*P] + ", "
				ret += buffer[smr4_alm+6*P] + ", "
				ret += buffer[smr5_alm+8*P] + ", "
				ret += buffer[smr6_alm+10*P] + ", "
			return ret
		else:
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			ret += "XE01" + ", "
			return ret

	def getAlarm(self):
		try:
			return 'Alarm 1|Alarm 2|Alarm 3'
		except:
			self.log.error("Can't get alarm!")
			return 'ERROR101'

	def setFCHV(self, volt):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		ans = self.SETConvert(volt)
		#print(ans)
		buf1 = Set_FCHV[0:15]+ans.encode()
		buf2 = self.CRC(buf1)
		ret = buf1.decode() + buf2 + '\r\r'
		#print("write")
		edit = ret.encode()
		for i in range(27): #jijig usgiig tom useg bolgox
			if(int(edit[i]) >= 96):
				if(int(edit[i]) < 126):
					ret = ret[0:i]+(chr(edit[i]-32))+ret[i+1:27]
		#print(ret)
		ret = ret + "\r"
		print(ret)
		trybuffer = 3
		while trybuffer>0:
			self.ser.write(ret.encode())
			time.sleep(0.1)
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		if buffer[13:15]==self.setCRC(buffer):
			return "OK"
		elif buffer=='TIMED_OUT':
			return "XE03"
		else:
			return 'XXE02'

	def setECHV(self, volt):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		ans = self.SETConvert(volt)
		#print(ans)
		buf1 = Set_ECHV[0:15]+ans.encode()
		buf2 = self.CRC(buf1)
		ret = buf1.decode() + buf2 + '\r\r'
		#print("write")
		edit = ret.encode()
		for i in range(27):
			if(int(edit[i]) >= 96):
				if(int(edit[i]) < 126):
					ret = ret[0:i]+(chr(edit[i]-32))+ret[i+1:27]
		#print(ret)
		ret = ret + "\r"
		print(ret)

		trybuffer = 3
		while trybuffer>0:
			self.ser.write(ret.encode())
			time.sleep(0.05)
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0
		if buffer[13:15]==self.setCRC(buffer):
			return "OK"
		elif buffer=='TIMED_OUT':
			return "XE03"
		else:
			return 'XE02'

	def setTSTV(self, volt):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		ans = self.SETConvert(volt)
		#print(ans)
		buf1 = Set_TSTV[0:15]+ans.encode()
		buf2 = self.CRC(buf1)
		ret = buf1.decode() + buf2 + '\r\r'
		#print("write")
		edit = ret.encode()
		for i in range(27):
			if(int(edit[i]) >= 96):
				if(int(edit[i]) < 126):
					ret = ret[0:i]+(chr(edit[i]-32))+ret[i+1:27]
		#print(ret)
		ret = ret + "\r"
		print(ret)
		trybuffer = 3
		while trybuffer>0:
			self.ser.write(ret.encode())
			time.sleep(0.05)
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		if buffer[13:15]==self.setCRC(buffer):
			return "OK"
		elif buffer=='TIMED_OUT':
			return "XE03"
		else:
			return 'XE02'

	def setMODE(self, mode):
		ret = ""
		self.ser.flushOutput() # serialiin read buffer-iig tseverleh
		self.ser.flushInput() # serialiin write buffer-iig tseverleh
		ans = mode
		#print(ans)
		if(ans=='fill'):
			print('fill')
			buf1 = Set_Mode_FILL
		elif(ans=='float'):
			print('float')
			buf1 = Set_Mode_FLOAT
		elif(ans=='test'):
			print('test')
			buf1 = Set_Mode_TEST
		elif(ans=='openrect'):
			print('openrect')
			buf1 = Set_Mode_OPENRECT
		elif(ans=='offrect'):
			print('offrect')
			buf1 = Set_Mode_OFFRECT
		else:
			return 'E03'

		ret = buf1.decode() + '\r\r'
		print(ret)
		trybuffer = 3
		while trybuffer>0:
			self.ser.write(ret.encode())
			time.sleep(0.05)
			buffer = self.readSerial(2)
			print("readfinish")
			print(buffer)
			if(buffer=='TIMED_OUT'):
				trybuffer-=1
			else:
				trybuffer=0

		print(buffer)
		if	buffer=="~200142000000FDB7":
			return "OK"
		elif buffer=='TIMED_OUT':
			return "XE01"
		else:
			return 'XE02'

	def CRC(self, ans):
		ret = ""
		sum = 0
		for i in range(1, 23):
			if(int(ans[i]) < 96):
				sum = sum + int(ans[i])
			else:
				sum = sum + int(ans[i]) - 32
		q = bin(sum-1)
		inv = "11111"
		for i in range(2, 13):
			if(int(q[i]) == 1):
				inv += "0"
			else:
				inv += "1"
		buf = hex(int(inv, 2))
		ret = buf[2:6]
		return ret

	def setCRC(self, ans):
		ret = ""
		summa = 0
		for i in range(1, 13):
			if(int(ans[i]) < 96):
				summa = summa + int(ans[i]) + 30
			else:
				summa = summa + int(ans[i]) + 30
		first, second = divmod(summa,10)
		print(first)
		for i in range(0, first):
			summa += 6
		q = bin(summa-1)
		inv = "111111"
		for i in range(2, 12):
			if(int(q[i]) == 1):
				inv += "0"
			else:
				inv += "1"
		buf = hex(int(inv, 2))
		buf = buf.upper()
		ret = buf[2:4]#Exnii 2 orongoor shalgana
		return ret

	def setData(self, value, volt):
		if value=='bchv':
			return self.setECHV(volt)
		elif	value=='fchv':
			return self.setFCHV(volt)
		elif	value=='tstv':
			return self.setTSTV(volt)
		elif	value=='mode':
			if	volt==1:
				return self.setMODE('fill')
			elif	volt==2:
				return self.setMODE('float')
			elif	volt==3:
				return self.setMODE('test')
			elif	volt==4:
				return self.setMODE('openrect')
			elif	volt==5:
				return self.setMODE('offrect')
		return 'XE-QWE'

	def set(self, tag, val):
		return self.setData(tag, val)

	def getData(self):
		ret = []
		print("getDC")
		ans = self.SplitData(self.getDC())
		ret.append(('dcouVol', ans[0]))
		ret.append(('dcouCur', ans[1]))
		ret.append(('batv1', ans[2]))
		ret.append(('batc1', ans[3]))
		ret.append(('batt1', ans[4]))
		ret.append(('batv2', ans[5]))
		ret.append(('batc2', ans[6]))
		ret.append(('batt2', ans[7]))
		ret.append(('batv3', ans[8]))
		ret.append(('batc3', ans[9]))
		ret.append(('batt3', ans[10]))
		ret.append(('batTestTime', ans[11]))
		ret.append(('battdAh', ans[12]))
		print("getSMRI")
		ans = self.SplitData(self.getSMRI())
		ret.append(('smri1', ans[0]))
		ret.append(('smri2', ans[1]))
		ret.append(('smri3', ans[2]))
		ret.append(('smri4', ans[3]))
		ret.append(('smri5', ans[4]))
		ret.append(('smri6', ans[5]))
		print("getSMRO")
		ans = self.SplitData(self.getSMRO())
		#ret.append(('smro1', ans[0]))
		#ret.append(('smro2', ans[1]))
		#ret.append(('smro3', ans[2]))
		#ret.append(('smro4', ans[3]))
		#ret.append(('smro5', ans[4]))
		#ret.append(('smro6', ans[5]))
		ret.append(('smrs1', ans[6]))
		ret.append(('smrs2', ans[7]))
		ret.append(('smrs3', ans[8]))
		ret.append(('smrs4', ans[9]))
		ret.append(('smrs5', ans[10]))
		ret.append(('smrs6', ans[11]))
		print("getSMRA")
		ans = self.SplitData(self.getSMRA())
		ret.append(('smra1', ans[0]))
		ret.append(('smra2', ans[1]))
		ret.append(('smra3', ans[2]))
		ret.append(('smra4', ans[3]))
		ret.append(('smra5', ans[4]))
		ret.append(('smra6', ans[5]))
		print("getAC")
		ans = self.SplitData(self.getAC())
		ret.append(('acina', ans[0]))
		ret.append(('acinb', ans[1]))
		ret.append(('acinc', ans[2]))
		ret.append(('freq', ans[3]))
		print("getACPar")
		ans = self.SplitData(self.getACPar())
		ret.append(('fchv', ans[0]))
		ret.append(('bchv', ans[1]))
		ret.append(('tstv', ans[2]))
		print('Done')
		print(ret)
		return ret

	def getTestData(self):
		ret = ""
	#	try:
	#		CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
	#		with open(CURRENT_DIR + "/../init/test.txt", "r") as content_file:
	#			content = content_file.read()
	#		content_file.close()
	#
	#		content = content.strip()
	#
	#		alarmString = ''
	#		arr = content.split("\n")
	#		for i in range(len(arr)):
	#			temp = arr"i].split("\t")
	#			if temp"0] == 'alarm':
	#				alarmString = alarmString + "|" + temp"1]
	#			else:
	#				ret.append((temp"0], temp"1]))
	#		ret.append(('alarm', alarmString"1:]))
	#	except:
	#		self.log.error('Data:getTest')
	#		sys.stderr.write("Data:getTest")
		return ret
#dm = DataManager(None)
#dm.init('/dev/ttyUSB2')
#print(dm.getData())
#print(dm.set('bchv',54))
