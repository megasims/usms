import sys, os
import serial, time, math
import Adafruit_BBIO.GPIO as GPIO

# BuffSizeHome = 1596
# BuffSizeService = 60
# BuffSizeWrongPass = 112
# BuffSize = 136
GPIO.setup("P8_35", GPIO.OUT)
Enter = "\x0D".encode()
Home = "\x36".encode()
User = "\x31".encode()
Service = "\x32".encode()
Pass = "\x30".encode()

class DataManager:
	def __init__(self, log):
		self.log = log
	def init(self, serialport):
		try:
			self.ser = serial.Serial(port=serialport, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0)
			#self.log.error("Connected to serial port")
			#sys.stderr.write("Connected to serial port"
			return True
		except:
			self.log.error("XE05 Can't enable serial port\n")
			#sys.stderr.write("Can't enable serial port\n")
			return 'XE05'
	def readSerial(self, timeout):
		count = 0
		out = ''
		while timeout > 0:
			if self.ser.inWaiting():
				temp = self.ser.read(1)
				try:
					out += temp.decode()
				except:
					pass
				count += 1
			else:
				if out!='':
					# print("Successfully read")
					return str(out), count
				else:	
					print(timeout)
					time.sleep(1)
					timeout -= 1		
		print("Timed out")
		#self.log.error("XE03 Timed Out! No response from serial port\n")
		return 'EMPTY', 0
		
	def saveSerial(self, timeout):
		count = 0
		out = ''
		while timeout > 0:
			if self.ser.inWaiting():
				temp = self.ser.read(1)
				try:
					out += temp.decode()
				except:
					pass
				count += 1
			else:
				if out!='':
					#self.log.error("Successfully read data from serial port\n")
					#self.log.error('data: ' + str(out) + '\n size: ' + str(count))
					
					# f = open('screenfile','w')
					# f.write(str(out)+'\n')
					return str(out), count
				else:	
					print(timeout)
					time.sleep(1)
					timeout -= 1
		print('Timed out')
		#self.log.error("XE03 Timed Out! No response from serial port\n")
		return 'EMPTY', 0
		
	def testCode2(self):
		self.ser.flushInput()
		self.ser.flushOutput()
		while True:
			inkey = input('press key: ')
			if inkey=='7':
				self.ser.write('\x37'.encode())
			elif inkey=='0':
				self.ser.write('\x30'.encode())
			elif inkey=='1':
				self.ser.write('\x31'.encode())
			elif inkey=='2':
				self.ser.write('\x32'.encode())
			elif inkey=='3':
				self.ser.write('\x33'.encode())
			elif inkey=='4':
				self.ser.write('\x34'.encode())
			elif inkey=='5':
				self.ser.write('\x35'.encode())
			elif inkey=='6': 
				self.ser.write('\x36'.encode())
			else:
				self.ser.write('\x0D'.encode())
			time.sleep(2)
			rcv, count = self.saveSerial(1)
			# print('Size: ' + str(count))
			print(rcv)   
			
	def firstLogin(self, try_count):
		self.ser.flushInput()
		self.ser.flushOutput()
		self.ser.write("\x33".encode())
		time.sleep(2)
		rcv, length = self.readSerial(2)
		count = 4
		while try_count>0:
			if 'ENTER PASSWORD' in rcv:
				self.ser.write(Pass)
				time.sleep(0.5)
				rcv, count = self.readSerial(1)
				print(rcv)	
				self.ser.write(Enter)
				time.sleep(2)
				rcv, count = self.readSerial(1)
				print(rcv)	
				return True	
			else:
				print('Waiting until password window appears')
				if count>5:
					print('Try count: ' + str(try_count))
					try_count -= 1
					GPIO.output("P8_35", GPIO.HIGH)
					print('GPIO-High')
					time.sleep(1)
					GPIO.output("P8_35", GPIO.LOW)
					print('GPIO-Low')
					time.sleep(1)
					rcv, length = self.readSerial(2)
					print('Size: ' + str(length))
					print(rcv)	
					count = 0
					GPIO.cleanup()
				else:
					count += 1
				# self.ser.write(User)
				# time.sleep(8)
				# rcv, length = self.readSerial(2)
				# print('Size: ' + str(length))
				# print(rcv)
				self.ser.write(Enter)
				time.sleep(5)
				rcv, length = self.readSerial(2)
				print('Size: ' + str(length))
				print(rcv)	
		print('Login Failed')		
		return False			
# -------------------------------------------------------
	def getBatVolt(self):
		rcv = self.goHome()
		#print ('TESTING...')
		print (rcv)
		if '04;35H' in rcv:
			# print ('IF')
			index = rcv.find('04;35H')
			print ('batv1: ' + rcv[index+6:index+10])
			return [('batv1', rcv[index+6:index+10])]
		else:
			# print ('ELSE')
			self.log.error('XE04 Expected word <04;35H> is not detected from serial data. getBatVolt()')
			return 'XE04'

	def getBatCur(self):
		rcv = self.goHome()
		if '03;62H' in rcv:
			index = rcv.find('03;62H')
			print ('batc1: ' + rcv[index+6:index+12])
			return ('batc1', rcv[index+6:index+12])
		else:
			self.log.error('XE04 Expected word <03;62H> is not detected from serial data. getBatCur()')
			return 'XE04'

	def getBatTemp(self):
		rcv = self.goHome()
		#print(rcv[2:])
		print('temp')
		if '04;07H' in rcv:
			index = rcv.find('04;07H')
			print(index)
			print ('batt1: ' + rcv[index+6:index+8])
			return ('batt1', rcv[index+6:index+8])
		else:
			self.log.error('XE04 Expected word <04;07H> is not detected from serial data. getBatTemp()')
			return 'XE04'

	def getOutVolt(self):
		rcv = self.goHome()
		if '04;35H' in rcv:
			index = rcv.find('04;35H')
			print ('dcouVolt: ' + rcv[index+6:index+10])
			return ('dcouVol', rcv[index+6:index+10])
		else:
			self.log.error('XE04 Expected word <04;35H> is not detected from serial data. getOutVolt()')
			return 'XE04'

	def getOutCur(self):
		rcv = self.goHome()
		if '03;35H' in rcv:
			index = rcv.find('03;35H')
			print ('dcouCur: ' + rcv[index+6:index+11])
			return ('dcouCur', rcv[index+6:index+11])
		else:
			self.log.error('XE04 Expected word <03;35H> is not detected from serial data. getOutCur()')
			return 'XE04'

	def getFCHV(self):
		target = open('/root/ELTEKRESPONSE', 'w')
		self.ser.flushInput()
		self.ser.flushOutput()
		self.ser.write(Service)
		print('Pressed Service')
		time.sleep(2)
		rcv, count = self.readSerial(5)
		print(rcv)
		try_count = 5
		while True:
			if 'ENTER PASSWORD' in rcv:
				print('Entering password')
				break
			else:
				if try_count==0:
					self.log.error('XE04 Expected word <ENTER PASSWORD> is not detected from serial data. setFCHV()')
					return 'XE04'
				else:
					try_count -= 1
					print('Waiting until password window appears')
					self.ser.write(Enter)
					time.sleep(2)
					rcv, count = self.readSerial(3)

		self.ser.write(Pass)
		time.sleep(1)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Enter)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Service)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		target.write(rcv)
		target.close()
		
		if '12;50H' in rcv:
			index = rcv.find('12;50H')
			print ('FCHV: ' + rcv[index+6:index+11])
			self.ser.write(Home)
			time.sleep(2)
			rcvv, countt = self.readSerial(3)
			return ('fchv', rcv[index+6:index+11])
		else:
			self.log.error('XE04 Expected word <12;50H> is not detected from serial data. getFCHV()')
			return 'XE04'
			
	def getECHV(self):
		target = open('/root/ELTEKRESPONSE', 'w')
		self.ser.flushInput()
		self.ser.flushOutput()
		self.ser.write(Service)
		print('Pressed Service')
		time.sleep(2)
		rcv, count = self.readSerial(5)
		print(rcv)
		try_count = 5
		while True:
			if 'ENTER PASSWORD' in rcv:
				print('Entering password')
				break
			else:
				if try_count==0:
					self.log.error('XE04 Expected word <ENTER PASSWORD> is not detected from serial data. setFCHV()')
					return 'XE04'
				else:
					try_count -= 1
					print('Waiting until password window appears')
					self.ser.write(Enter)
					time.sleep(2)
					rcv, count = self.readSerial(3)

		self.ser.write(Pass)
		time.sleep(1)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Enter)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Service)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		target.write(rcv)
		target.close()
		
		if '13;50H' in rcv:
			index = rcv.find('13;50H')
			print ('BCHV: ' + rcv[index+6:index+11])
			self.ser.write(Home)
			time.sleep(2)
			rcvv, countt = self.readSerial(3)
			return ('bchv', rcv[index+6:index+11])
		else:
			self.log.error('XE04 Expected word <13;50H> is not detected from serial data. getECHV()')
			return 'XE04'			

	def checkAlarm(self):
		rcv = self.goHome()
		if 'RM :' in rcv:
			index = rcv.find('RM :')
			print ('ALARM: ' + rcv[index+4:index+7])
			if 'F' in rcv[index+4:index+7]:
				return ('alarm', rcv[index+4:index+7])
				print(rcv[index4:index+7])
			else:
				return self.getAlarm()
		else:
			self.log.error('XE04 Expected word <RM :> is not detected from serial data. checkAlarm()')
			return 'XE04'
	def getAlarm(self):
		self.ser.flushInput()
		self.ser.flushOutput()
		rcv = self.goHome()
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		index = 0;
		stat = ''
		for i in range(0,1):
			ind1 = rcv.find('####',index) + 4
			ind2 = rcv.find('####',ind1)
			print(ind1)
			print(ind2)
			index = ind2 + 1
			if ind1==(-1) or ind2==(-1):
				return ''
			else:
				#print(rcv[ind1:ind2])
				stat += '|' + (rcv[ind1:ind2][:-27])
		#print(rcv.find('####'))
		return ('alarm', stat[1:])
	def checkSystem(self):
		rcv = self.goHome()
		if 'SYSTEM:' in rcv:
			index = rcv.find('SYSTEM:')
			print ('ON_OFF: ' + rcv[index+7:index+9])
			if 'ON' in rcv[index+7:index+9]:
				return ('on_off', 1)
			else:
				return ('on_off', 0)
		else:
			self.log.error('XE04 Expected word <SYSTEM:> is not detected from serial data. checkSystem()')
			return 'XE04'


		

	def get(self, tag):
		funcs = {
			'batv1': self.getBatVolt,
			'batc1': self.getBatCur,
			'batt1': self.getBatTemp,
			'dcouVol': self.getOutVolt,
			'dcouCur': self.getOutCur,
			'alarm': self.checkAlarm,
			'on_off':  self.checkSystem,
			'fchv': self.getFCHV,
			'bchv': self.getECHV,
		}
	# =========================== GET =======================
	def getData(self):
		self.goHome()
		#self.getFCHV()
		#self.setFCHV(52.55)
		#self.setECHV(56.00)
		#self.testCode2()
		#ret=[]
		ret = self.getBatVolt()
		ret.append(self.getBatCur())
		ret.append(self.getBatTemp())
		ret.append(self.getOutVolt())
		ret.append(self.getOutCur())
		ret.append(self.checkAlarm())
		ret.append(self.checkSystem())
		ret.append(self.getFCHV())
		ret.append(self.getECHV())
		ret.append(self.getAlarm())
		print(ret)
		return ret
# =========================== SET =======================

	def set(self, tag, value):
		funcs = {
			'fchv': self.setFCHV(value),
			'bchv': self.setECHV(value),
		}
		func = funcs.get(tag, self.setError)
		return func(value)
	
	def convertNumber(self, temp):
		if temp==9:
			out = '\x39'.encode()
		elif temp==1:
			out = '\x31'.encode()
		elif temp==2:
			out = '\x32'.encode()
		elif temp==3:
			out = '\x33'.encode()
		elif temp==4:
			out = '\x34'.encode()
		elif temp==5:
			out = '\x35'.encode()
		elif temp==6:
			out = '\x36'.encode()
		elif temp==7:
			out = '\x37'.encode()
		elif temp==8:
			out = '\x38'.encode()
		else:
			out = '\x30'.encode()
		return out	
				
	def splitNumbers(self, temp):
		s4 = round((temp*100)%10)
		s3 = round(math.floor(temp*10)%10)
		s2 = round(math.floor(temp)%10)
		s1 = math.floor(temp/10)
		return s1,s2,s3,s4
		
	def setFCHV(self, temp):
		t1,t2,t3,t4 = self.splitNumbers(temp)
		self.ser.flushInput()
		self.ser.flushOutput()
		self.goHome()
		#self.ser.write(Home)
		#time.sleep(2)
		#rcv, count = self.readSerial(5)
		self.ser.write(Service)
		print('Pressed Service')
		time.sleep(2)
		rcv, count = self.readSerial(5)
		print(rcv)
		try_count = 5
		while True:
			if 'ENTER PASSWORD' in rcv:
				print('Entering password')
				break
			else:
				if try_count==0:
					self.log.error('XE04 Expected word <ENTER PASSWORD> is not detected from serial data. setFCHV()')
					return 'XE04'
				else:
					try_count -= 1
					print('Waiting until password window appears')
					self.ser.write(Enter)
					time.sleep(2)
					rcv, count = self.readSerial(3)

		self.ser.write(Pass)
		time.sleep(1)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Enter)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Service)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)

		if 'BATTERY VOLTAGE' in rcv:
			self.ser.write(Enter)
			time.sleep(1.5)
			self.ser.write(Enter)
			time.sleep(1.5)
			self.ser.write(Enter)
			time.sleep(1.5)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)

			self.ser.write(self.convertNumber(t1))
			time.sleep(1.5)
			self.ser.write(self.convertNumber(t2))
			time.sleep(1.5)
			self.ser.write(self.convertNumber(t3))
			time.sleep(1.5)
			self.ser.write(self.convertNumber(t4))
			time.sleep(1.5)

			self.ser.write(Enter)
			time.sleep(1.5)
			self.ser.write(Enter)
			time.sleep(1.5)
			self.ser.write(Enter)
			time.sleep(1.5)
			self.ser.write(Enter)
			time.sleep(1.5)
			# self.ser.write(Enter)
			# time.sleep(1.5)
			# rcv, count = self.readSerial(3)
			# print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			set_value = str(t1)+str(t2)+'.'+str(t3)+str(t4)
			if set_value in rcv:
				self.ser.write(Home)
				time.sleep(2)
				rcv, count = self.readSerial(3)
				print(rcv)
				return 'OK'
			else:
				self.log.error('XE04 Expected word is not detected from serial data. setFCHV()')
				return 'Failed'
		else:
			self.log.error('XE04 Expected word is not detected from serial data. setFCHV()')
			return 'XE04'

	def setECHV(self, temp):
		self.goHome()
		t1,t2,t3,t4 = self.splitNumbers(temp)
		self.ser.flushInput()
		self.ser.flushOutput()
		self.goHome()
		# self.ser.write(Home)
		# time.sleep(2)
		# rcv, count = self.readSerial(5)
		self.ser.write(Service)
		print('Pressed Service')
		time.sleep(2)
		rcv, count = self.readSerial(5)
		print(rcv)
		while True:
			if 'ENTER PASSWORD' in rcv:
				print('Entering password')
				break
			else:
				print('Waiting until password window appears')
				self.ser.write(Enter)
				time.sleep(2)
				rcv, count = self.readSerial(3)

		self.ser.write(Pass)
		time.sleep(1)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Enter)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(Service)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)
		self.ser.write(User)
		time.sleep(2)
		rcv, count = self.readSerial(3)
		print(rcv)

		if 'BATTERY VOLTAGE' in rcv:
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)

			self.ser.write(self.convertNumber(t1))
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(self.convertNumber(t2))
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(self.convertNumber(t3))
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(self.convertNumber(t4))
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)

			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			self.ser.write(Enter)
			time.sleep(1.5)
			rcv, count = self.readSerial(3)
			print(rcv)
			set_value = str(t1)+str(t2)+'.'+str(t3)+str(t4)
			if set_value in rcv:
				self.ser.write(Home)
				time.sleep(2)
				rcv, count = self.readSerial(3)
				print(rcv)
				return 'OK'
			else:
				return 'XE04 Expected word is not detected from serial data. setFCHV()'
		else:
			return 'XE04 Expected word is not detected from serial data. setFCHV()'

# ========================== OTHER ======================
	def goHome(self):
		self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
		self.ser.flushInput()  # serialiin write buffer-iig tseverleh
		self.ser.write(Home)
		time.sleep(3)
		rcv, count = self.readSerial(2)
		if 'ELTEK' in rcv:
			return rcv
		self.ser.write(User)
		# print('write 1')
		time.sleep(3)
		rcv, count = self.readSerial(2)
		#print(rcv)
		if 'USER INFO' in rcv:
			#print(rcv)
			#time.sleep(3)
			self.ser.write(Home)
			#print('write 2')
			time.sleep(2)
			rcv, count = self.readSerial(2)
			if 'OUTPUT CURRENT' in rcv:
				return rcv
			else:
				if self.firstLogin(3):
					return self.goHome()
				else:
					print()
					self.log.error('')
					return 'XE03'
		else:
			if 'PLEASE WAIT' in rcv:
				for i in range(0,5):
					time.sleep(5)
					self.ser.write(Enter)
					rcv, count = self.readSerial(2)
					if 'ENTER PASSWORD' in rcv:
						self.ser.write(Pass)
						time.sleep(0.5)
						rcv, count = self.readSerial(1)
						print(rcv)	
						self.ser.write(Enter)
						time.sleep(2)
						rcv, count = self.readSerial(1)
						print(rcv)	
						return True	
			else:
				if self.firstLogin(3):
					return self.goHome()
				else:
					print()
					self.log.error('')
					return 'XE03'
		return False


# ========================== END =============================
# test = DataManager(None)
# test.init('COM8')
# rcv, count = test.readSerial(10)
# # print(rcv)
# # type(rcv)
# # print(rcv.find('04;35H'))
# print (test.getData())

#dataManager = DataManager(None)
#dataManager.init('/dev/ttyUSB4')
#print(dataManager.getData())

#dataManager.set('fchv',52.56)
#dataManager.goHome()
#print(dataManager.testCode2())
#print(dataManager.checkAlarm())
#dataManager.getAlarm()
#dataManager.setECHV(56.01)

