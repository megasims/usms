import sys, os, serial, time
from multiprocessing import Process
import struct, binascii
import string
import array
from time import sleep
#from clients.PW1.lib.data import *

class DataManager:
    def __init__(self, log):
        self.log = log
        self.templates = None
        self.rectControl = None

    def init(self, serialPort):
        try:
            result = True
            print(serialPort)
            self.rectControl = RectControl(serialPort)
            return result
        except:
            self.log.error("Can't enable serial port\n")
            sys.stderr.write("Can't enable serial111 port\n")
            return False

    def getTemp(self):
        # self.ser.read()
        pass

    def getAlarm(self):
        try:
            return self.rectControl.getAlarm()
        except:
            self.log.error("Can't get alarm!")
            return 'ERROR101'

    def getOnOff(self):
        return 0

    def set(self, tagName, value):
        return self.rectControl.set(tagName, value)

    # ...etc...

    def getData(self):
        # ret = [('on_off', 1), ('temp', 18), ('alarm', 'alarm-1|alarm-2|alarm-3')]
        #ret.append(('alarm', )
        #ret.append(('temp', 18))
        #ret.append(('alarm', 'str1|str2|str3|str4'))
        return self.rectControl.getData();

    def getTestData(self):
        ret = []
        try:
            CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
            with open(CURRENT_DIR + "/../init/test.txt", "r") as content_file:
                content = content_file.read()
            content_file.close()

            content = content.strip()

            alarmString = ''
            arr = content.split("\n")
            for i in range(len(arr)):
                temp = arr[i].split("\t")
                if temp[0] == 'alarm':
                    alarmString = alarmString + "|" + temp[1]
                else:
                    ret.append((temp[0], temp[1]))
            ret.append(('alarm', alarmString[1:]))
        except:
            self.log.error('Data:getTest')
            sys.stderr.write("Data:getTest")
        return ret


class Letter:
    def __init__(self, col, row, cnt, height, width, type):
        self.col = col
        self.row = row
        self.cnt = cnt
        self.height = height
        self.width = width
        self.type = type
        pass


class Letters:
    def __init__(self):
        self.battery_volt1 = Letter(98, 1, 2, 8, 6, 1)
        self.battery_volt2 = Letter(113, 1, 1, 8, 6, 1)

        self.temp = Letter(63, 1, 3, 8, 6, 1)

        self.hours1 = Letter(67, 12, 4, 8, 6, 1)
        self.hours2 = Letter(94, 12, 1, 8, 6, 1)

        self.rpm = Letter(2, 12, 4, 8, 6, 1)
        self.pressure = Letter(2, 1, 4, 8, 6, 1)

        self.VA1 = Letter(49, 1, 3, 8, 6, 1)
        self.VA2 = Letter(82, 1, 3, 8, 6, 1)
        self.VA3 = Letter(115, 1, 3, 8, 6, 1)

        self.A1 = Letter(49, 12, 3, 8, 6, 1)
        self.A2 = Letter(82, 12, 3, 8, 6, 1)
        self.A3 = Letter(115, 12, 3, 8, 6, 1)

        self.Hertz1 = Letter(9, 12, 2, 8, 6, 1)
        self.Hertz2 = Letter(24, 12, 1, 8, 6, 1)

        self.alarmCNT = Letter(121, 12, 2, 8, 6, 1)
        self.alarm1 = Letter(0, 1, 50, 8, 6, 9)
        self.alarm2 = Letter(0, 12, 20, 8, 6, 9)
        self.alarm3 = Letter(0, 23, 50, 8, 6, 9)


class Template:
    def __init__(self, value, type, n, m):
        self.value = value
        self.type = type
        self.N = n
        self.M = m
        self.A = None
        pass

    def setA_append(self, row):
        if self.A == None:
            self.A = []
        self.A.append(row)

    def setA(self):
        self.A = []
        pass

    def setA(self, A):
        try:
            self.setA()
            for row in A:
                self.A.append(row)
            return True
        except:
            return False
        pass

    def disp(self):
        print("----------------TEMPLATE---------------------")
        print("Value:", self.value, "Type:", self.type)

        try:
            for row in self.A:
                for item in row:
                    print(item, end='')
                print('')
            return True
        except:
            return False
        pass

    def setElement(self, val, i, j):
        try:
            self.A[i][j] = val
            return True
        except:
            return False
        pass

    def setValue(self, value):
        try:
            self.value = value
            return True
        except:
            return False
        pass

    def getValue(self):
        return self.value

    def isType(self, type):

        if self.type == 0:
            return True

        if type == self.type:
            return True

        if type == 9:
            if self.type == 1 or self.type == 2 or self.type == 3:
                return True
            else:
                return False
        elif type == 10:
            if self.type == 2 or self.type == 3:
                return True
            else:
                return False
        elif type == 11:
            if self.type == 4 or self.type == 5:
                return True
            else:
                return False
        return False
        pass

    def isEqual(self, A, row, col, type):
        if not self.isType(type):
            return 262144  # 512*512
        try:
            s = 0
            for i in range(len(self.A)):
                for j in range(len(self.A[i])):
                    s += (self.A[i][j] ^ A[i + row][j + col])
            return s
        except:
            return 262144
        pass


class Templates:
    def __init__(self, fname):
        self.EmptyThreshold = 0
        self.templates = []
        self.init(fname)
        self.isHome = False
        pass

    def init(self, fname):
        with open(fname) as f:
            for line in f:
                try:
                    x = line.split()
                    n = int(x[2])
                    m = int(x[3])
                    t = Template(x[0], int(x[1]), n, m)

                    for i in range(n):
                        temp = []
                        for idx in range(4, m + 4):
                            temp.append(int(x[i * m + idx]))
                        t.setA_append(temp)
                    self.templates.append(t)
                except:
                    print(line)
                    print("---------------------File read error-------------------------")
        pass

    def tempMatch(self, A, letter):
        N = 0
        M = 0
        result = ""
        try:
            N = len(A)
            if N > 0:
                M = len(A[0])
                if M <= 0:
                    return None
            else:
                return None

        except:
            return None

        iter = 0
        col = letter.col
        while col < M:
            if iter >= letter.cnt:
                break
            minValue = 262144  # 512*512
            minTemplate = None
            for template in self.templates:
                r = template.isEqual(A, letter.row, col, letter.type)
                if r < minValue:
                    minValue = r
                    minTemplate = template
                    if minValue == 0:
                        break
            if minValue > self.EmptyThreshold:
                col += 1
            else:
                iter += 1
                temp1 = minTemplate.getValue()
                if temp1 == 'N':
                    pass
                if temp1 == 'sp':
                    result += ' '
                elif temp1 == 'spnum':
                    pass
                else:
                    result += temp1
                col += minTemplate.M
        result = result.strip()
        while True:
            temp = result.replace('  ', ' ')
            if temp == result:
                break
            result = temp
        return result


    def disp(self):
        for item in self.templates:
            item.disp()
        pass


class LCDControl:
    def __init__(self, rowNumber, colNumber):
        self.ser = None
        self.trybuffer = 3

        self.ESC = 0xF1
        self.UP = 0xF2
        self.DOWN = 0xF3
        self.RIGHT = 0xF4
        self.LEFT = 0xF5
        self.ENTER = 0xF6
        #self.HOME = 0xF9
        self.AC = 0xFA
        self.MOTOR = 0xFB

        self.ON = 0xFC
        self.AUTO = 0xFD
        self.OFF = 0xFE

        self.ALMLIST = 0xE0
        self.ALMRES = 0xE1
        self.WARN = 0xE2

        # = [[0]*colNumber]*rowNumber
        self.A = [[0] * colNumber for i in range(rowNumber)]

        self.errorByte = 0

    def disp(self):
        for row in self.A:
            for item in row:
                print(item, end='')
            print('')

    def init(self, serialPort):
        try:
            print(serialPort,"KKKK")
            self.ser = serial.Serial(port=serialPort, baudrate=115200, parity=serial.PARITY_NONE,
                                     stopbits=serial.STOPBITS_ONE,
                                     bytesize=serial.EIGHTBITS, timeout=0)
            print("LLLL")
            return True
        except:
            # self.log.error("Can't enable serial port\n")
            sys.stderr.write("Can't enable serial port\n")
            return False

    def convertByteToInt(self, arr):
        result = []
        for idx in range(len(arr)):
            try:
                result.append(struct.unpack('B', arr[idx])[0])
            except:
                result.append(0)
                print("Convert error")
        return result

    def readSerial(self, timeout, len):
        ret = []
        cnt = 0
        # print("readSerial")
        while timeout > 0:
            if cnt >= len:
                return self.convertByteToInt(ret)
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    #print('read byte', b)
                except Exception as ex:
                    b = 0
                    print('serial error decode')
                    continue

                ret.append(b)
                cnt += 1


            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def readSerial1(self, timeout):
        ret = []
        started = False
        # print("readSerial")
        while timeout > 0:
            #print("Timeout")
            if self.ser.inWaiting():  # serial huleelt
                #print("inWaiting")
                try:
                    b = self.ser.read(1)  # serial read zaaval decode hiih yostoi
                    # print('read byte', b)
                except Exception as ex:
                    b = 0
                    print('serial error decode')
                    continue

                if b == self.stopByte:  #stopbyte orj irsen esehiig shalgana
                    return ret

                if started:  # herev ehelsen bol stopbyte orj irtel buh byte-uudiig avna
                    # print('append', b)
                    ret.append(b)
                    continue
                if b == self.startByte:  # startbyte orj irseniig shalgana
                    started = True
                if not started:  # ehelsen esehiig shalgana
                    continue
            else:
                print('TIMED_OUT')
                time.sleep(1)
                timeout = timeout - 1
        return "TIMED_OUT"

    def click(self, val, cnt=0x01):
        buffer = [0xF0, 0x02, val, cnt]

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', buffer).tostring()  # byte array to string
                self.ser.write(temp)

                time.sleep(1)
                rcv = self.readSerial(10, 1 + self.errorByte)
                print(rcv)
                if rcv == 'TIMED_OUT':
                    return False

                if rcv[0] == 0x45:
                    rcv = self.readSerial(10, 1 + self.errorByte)
                    print(rcv)
                    if rcv == 'TIMED_OUT':
                        return False

                    if rcv[0] == 0x55:
                        return True
                    else:
                        return False
                else:
                    return False

            except:
                print('Home except')
                trybuffer = trybuffer - 1
        return False

    def ledRead(self):
        buffer = [0xF0, 0x01, 0xF8]

        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', buffer).tostring()  # byte array to string
                self.ser.write(temp)

                time.sleep(1)
                rcv = self.readSerial(10, 1 + self.errorByte)

                if rcv == 'TIMED_OUT':
                    return None

                if rcv[0] != 0x45:
                    return None

                led = self.readSerial(10, 5 + self.errorByte)
                if rcv == 'TIMED_OUT':
                    return None

                rcv = self.readSerial(10, 1 + self.errorByte)
                if rcv == 'TIMED_OUT':
                    return None

                if rcv[0] == 0x55:
                    return led
                else:
                    return None
            except:
                print('Home except')
                trybuffer = trybuffer - 1
        return None

    def goHome(self):
        return self.clickEsc(5)

    def clickEsc(self, cnt=1):
        return self.click(self.ESC)

    def clickUp(self, cnt=1):
        return self.click(self.UP, cnt)

    def clickDown(self, cnt=1):
        return self.click(self.DOWN, cnt)

    def clickRight(self, cnt=1):
        return self.click(self.RIGHT, cnt)

    def clickLeft(self, cnt=1):
        return self.click(self.LEFT, cnt)

    def clickEnter(self, cnt=1):
        return self.click(self.ENTER, cnt)

    def clickAC(self):
        return self.click(self.AC)

    def clickMotor(self):
        return self.click(self.MOTOR)

    def clickOFF(self):
        return self.click(self.OFF)

    def clickON(self):
        return self.click(self.ON)

    def clickAUTO(self):
        return self.click(self.AUTO)

    def clickALMLIST(self):
        res = self.goHome()
        res = res & self.clickEnter(2)
        return res
#self.goHome() and self.clickEnter(2)

    def readDisp(self, xa1, ya, xb1, yb):
        xa = int(xa1 / 8);
        xb = int(1 + (xb1 - 1) / 8);
        buffer = [0xF0, 5, 0xF7, xa, ya, xb, yb];
        trybuffer = self.trybuffer
        while trybuffer > 0:
            try:
                self.ser.flushOutput()  # serialiin read buffer-iig tseverleh
                self.ser.flushInput()  # serialiin write buffer-iig tseverleh

                temp = array.array('B', buffer).tostring()  # byte array to string
                self.ser.write(temp)

                time.sleep(0.5)
                rcv = self.readSerial(10, 1 + self.errorByte)

                if rcv == 'TIMED_OUT':
                    return False

                if rcv[0] != 0x45:
                    return False

                bufferSize = (xb - xa) * (yb - ya) + 1;

                rcv = self.readSerial(10, bufferSize + self.errorByte)

                checkCRC = 0

                cnt = 0
                for i in range(xa, xb):
                    for j in range(ya, yb):
                        val = int(rcv[cnt])
                        checkCRC ^= val
                        cnt += 1

                        for idx in range(8):
                            self.A[i * 8 + idx][j] = val % 2
                            val = int(val / 2)

                if checkCRC == rcv[cnt]:
                    rcv = self.readSerial(10, 1 + self.errorByte)

                    if rcv == 'TIMED_OUT':
                        return False
                    if rcv[0] == 0x55:
                        return True
                    else:
                        return False

                else:
                    print('readDisp except')
                    trybuffer = trybuffer - 1

            except:
                print('readDisp except')
                trybuffer = trybuffer - 1
        return False


class RectControl:
    def __init__(self, serPost):
        print("RECT")
        self.rowNumber = 32
        self.colNumber = 132
        self.lcd = LCDControl(self.rowNumber, self.colNumber + 1)
        self.lcd.init(serPost)
        CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
        # with open(CURRENT_DIR + "/../init/test.txt", "r") as content_file:
        self.templates = Templates("/opt/APP/CLIENT-MANAGER/clients/PW1/lib/data.txt")
        self.letters = Letters()

    def captureFullLCD(self):
        return self.lcd.readDisp(0, 0, self.rowNumber, self.colNumber)

    def captureLCD(self, letter):
        return self.lcd.readDisp(letter.row, letter.col, letter.row + letter.height,
                                 letter.col + letter.width * letter.cnt)

    def getFreq(self):
        if (not self.lcd.clickAC()):
            return "ErrorAC"

        if (not self.captureLCD(self.letters.Hertz1)):
            return "ErrorLCD"

        temp1 = self.templates.tempMatch(self.lcd.A, self.letters.Hertz1)

        if (not self.captureLCD(self.letters.Hertz2)):
            return "ErrorLCD"

        temp2 = self.templates.tempMatch(self.lcd.A, self.letters.Hertz2)

        temp = temp1.strip() + '.' + temp2.strip()
        print('freq', temp)
        return temp

    def getGenaVol(self):
        if (not self.lcd.clickAC()):
            return "ErrorAC"

        if (not self.lcd.clickDown(2)):
            return "ErrorDown"

        if (not self.captureLCD(self.letters.VA1)):
            return "ErrorLCD"

        temp = self.templates.tempMatch(self.lcd.A, self.letters.VA1)

        print('VA1', temp)
        return temp.strip()

    def getGenbVol(self):
        if (not self.lcd.clickAC()):
            return "ErrorAC"

        if (not self.lcd.clickDown(2)):
            return "ErrorAC"

        if (not self.captureLCD(self.letters.VA2)):
            return "ErrorLCD"

        temp = self.templates.tempMatch(self.lcd.A, self.letters.VA2)

        print('VA2', temp)
        return temp.strip()

    def getGencVol(self):
        if (not self.lcd.clickAC()):
            return "ErrorAC"

        if (not self.lcd.clickDown(2)):
            return "ErrorDown"

        if (not self.captureFullLCD()):
            return "ErrorLCD"

        temp = self.templates.tempMatch(self.lcd.A, self.letters.VA3)

        print('VA3', temp)
        return temp.strip()

    def getGenaCur(self):
        if (not self.lcd.clickAC()):
            return "ErrorAC"

        if (not self.lcd.clickDown(2)):
            return "ErrorDown"

        if (not self.captureLCD(self.letters.A1)):
            return "ErrorLCD"

        temp = self.templates.tempMatch(self.lcd.A, self.letters.A1)

        print('A1', temp)
        return temp.strip()

    def getGenbCur(self):
        if (not self.lcd.clickAC()):
            return "ErrorAC"

        if (not self.lcd.clickDown(2)):
            return "ErrorAC"

        if (not self.captureLCD(self.letters.A2)):
            return "ErrorLCD"

        temp = self.templates.tempMatch(self.lcd.A, self.letters.A2)

        print('A2', temp)
        return temp.strip()

    def getGencCur(self):
        if (not self.lcd.clickAC()):
            return "ErrorAC"

        if (not self.lcd.clickDown(2)):
            return "ErrorDown"

        if (not self.captureFullLCD()):
            return "ErrorLCD"

        temp = self.templates.tempMatch(self.lcd.A, self.letters.A3)

        print('A3', temp)
        return temp.strip()

    def getBatteryVolt(self):

        if (not self.lcd.clickMotor()):
            return "ErrorAC"

        if (not self.captureLCD(self.letters.battery_volt1)):
            return "ErrorLCD"

        temp1 = self.templates.tempMatch(self.lcd.A, self.letters.battery_volt1)

        if (not self.captureLCD(self.letters.battery_volt2)):
            return "ErrorLCD"

        temp2 = self.templates.tempMatch(self.lcd.A, self.letters.battery_volt2)

        temp = temp1.strip() + '.' + temp2.strip()
        print('BatteryVolt', temp)
        return temp

    def getMotorRuntime(self):

        if (not self.lcd.clickMotor()):
            return "ErrorAC"

        if (not self.captureLCD(self.letters.hours1)):
            return "ErrorLCD"

        temp1 = self.templates.tempMatch(self.lcd.A, self.letters.hours1)

        if (not self.captureLCD(self.letters.hours2)):
            return "ErrorLCD"

        temp2 = self.templates.tempMatch(self.lcd.A, self.letters.hours2)

        temp = temp1.strip() + '.' + temp2.strip()
        print('motorRuntime', temp)
        return temp

    def getCoolTemp(self):
        if (not self.lcd.clickMotor()):
            return "ErrorAC"

        if (not self.captureLCD(self.letters.temp)):
            return "ErrorLCD"
        # self.lcd.disp()
        temp = self.templates.tempMatch(self.lcd.A, self.letters.temp)

        print('Cool', temp)
        return temp.strip()

    def getOnOffMode(self):
        try:
            temp = self.lcd.ledRead()
            print('Mode', temp)
            if temp == None:
                return "XE01"
            if temp[0]:
                return 1  # auto mod
            if temp[2]:
                return 2  # on
            if temp[4]:
                return 0  # off
        except:
            return "XE01"
        return "None"

    def getOneAlarm(self):
        if (not self.captureFullLCD()):
            return "ErrorALMLIST"
        # self.lcd.disp()

        temp = self.templates.tempMatch(self.lcd.A, self.letters.alarm1)
        temp += ' ' + self.templates.tempMatch(self.lcd.A, self.letters.alarm2)
        temp += ' ' + self.templates.tempMatch(self.lcd.A, self.letters.alarm3)

        return temp.strip()

    def isAlarm(self):
        try:
            temp = self.lcd.ledRead()
            print('Mode', temp)
            if temp == None:
                return None
            if temp[1]:
                return True
            if temp[3]:
                return True
        except:
            return None
        return False


    def getAlarmCNT(self):
        try:
            res = self.isAlarm()
            if res is None:
                return 0
            if res:
                temp = self.templates.tempMatch(self.lcd.A, self.letters.alarmCNT)
                print("temp", temp)
                return 10#int(temp.strip())
            else:
                return 0
        except:
            return None

    def getAlarm(self):
        isAlrm = self.isAlarm()
        if isAlrm is None and not isAlrm:
            return "No alarm"
        if (not self.lcd.clickALMLIST()):
            return "ErrorALMLIST"
        
        #if (not self.lcd.clickEnter()):
        #    return "ErrorALMLIST"

        if (not self.captureFullLCD()):
            return "ErrorALMLIST"
        #self.lcd.disp()
        count = 20#self.getAlarmCNT()
        if count is None:
            return "No alarm"

        if count <= 0:
            return "No alarm"

        alarm = []
        for i in range(count):
            try:
                temp = self.getOneAlarm()
                print(temp)
                if len(alarm)>0 and temp==alarm[-1]:
                    break
                alarm.append(temp)
                if (not self.lcd.clickDown()):
                    print("ErrorALMLIST")
            except:
                pass
        return "|".join(str(x) for x in alarm)

    def setError(self, value):
        return "XE00"

    def setOnOff(self, value):
        command = ''
        val = 0
        try:
            val = int(value)
        except:
            return 'XE02'

        if val == 0:
            return self.lcd.clickOFF()
        elif val == 1:
            return self.lcd.clickAUTO()
        elif val == 2:
            return self.lcd.clickON()
        else:
            return "XE01"

    def set(self, tag, value):
        funcs = {
            'on_off_auto': self.setOnOff
        }
        func = funcs.get(tag, self.setError)
        return func(value)

    def getData(self):
        funcs = {
            'genaVol': self.getGenaVol,
            'genbVol': self.getGenbVol,
            'gencVol': self.getGencVol,
            'genaCur': self.getGenaCur,
            'genbCur': self.getGenbCur,
            'gencCur': self.getGencCur,
            'freq': self.getFreq,
            'batVolt': self.getBatteryVolt,
            'motorRuntime': self.getMotorRuntime,
            'on_off_auto': self.getOnOffMode,
            'cool': self.getCoolTemp
        }

        ret = [];
        for tag, func in funcs.items():
            try:
                ret.append((tag, func()))
            except:
                ret.append((tag, 'E1'))

        print('Done')
        print(ret)
        return ret

#templates = Templates("data.txt")
#templates.disp()
#dm = DataManager(None)
#dm.init('/dev/ttyUSB0')
#dm.clickDown(1)
#control.captureFullLCD()
#control.lcd.disp()
#print(dm.getData())
#print(dm.getAlarm())

#lcd = LCDControl(32, 132)
#lcd.init('/dev/ttyUSB0')
#lcd.goHome()
#lcd.clickAUTO()
#lcd.clickEnter()
#lcd.clickDown(3)
#lcd.readDisp(0,0,32, 132)
#lcd.disp()
