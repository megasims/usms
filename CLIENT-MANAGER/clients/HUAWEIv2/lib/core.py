import socket
import sys, os
import sqlite3

class Socket:
	def __init__(self, host, log):
		self.host = host
		self.log = log

	def send(self, data):
		print ("Send: " + data)
		try:
			data = data.strip() + "\n"
			conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			conn.connect(self.host)
			conn.send(data.encode())
			ret = conn.recv(16).decode().strip();
			if ret != 'U':
				sys.stderr.write("Worker not respond: " + data)
				self.log.error("Worker not respond: " + data)
			conn.close()
		except:
			sys.stderr.write("Socket error: " + data)
			self.log.error("Socket error: " + data)

	def get(self, data):
		try:
			data = data.strip() + "\n"
			conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			conn.connect(self.host)
			conn.send(data.encode())
			ret = conn.recv(1024).decode().strip();
			conn.close()
			return ret
		except:
			sys.stderr.write("Socket error: " + data)
			self.log.error("Socket error: " + data)
			return None


class Tag:
	def __init__(self, clientID, arr, oldValue, host, log):
		try:
			self.host = host
			self.id = arr[0]
			self.name = arr[1]
			self.type = arr[2]
			if arr[2] == 'variable':
				self.interval = int(arr[3])
			else:
				self.interval = 0
			self.value = oldValue
			self.log = log
			self.intervalCounter = 0
			self.intervalStep = 10
			self.clientID = clientID

		except:
			self.id = None
			log.error("Cannot create tag!\n")
			sys.stderr.write("Cannot create tag!\n")

	def send(self):
		if self.type == 'alarm':
			data = "D\t" + str(self.clientID) + "\t" + str(self.id) + "\t" + str(self.value)
		else:
			data = "D\t" + str(self.clientID) + "\t" + str(self.id) + "\t" + "0" + str(self.value)
		socket = Socket(self.host, self.log)
		socket.send(data)
		
	def handleValue(self, new_value):
		try:
			new_value = str(new_value).strip()
			CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
			sys.path.append(os.path.dirname(CURRENT_DIR))
			conn = sqlite3.connect(CURRENT_DIR + '/../init/local.db')
			c = conn.cursor()
			c.execute("UPDATE tags SET value = '"+str(self.value)+"' WHERE id = " + str(self.id))
			conn.commit()
			c.close()
			
			if self.type == 'alarm':
				if self.value == None:
					self.value = ''
				new_values = new_value.split("|")
				new_values = [var for var in new_values if var]
				for j in range(len(new_values)):
					new_values[j] = new_values[j]
				old_values = str(self.value).split("|")
				old_values = [var for var in old_values if var]
				for i in range(len(old_values)):
					for j in range(len(new_values)):
						if old_values[i] == new_values[j]:
							old_values[i] = '*'
							new_values[j] = '#'

				for i in range(len(old_values)):
					if old_values[i] != '*':
						self.value = '1' + old_values[i]
						self.send()

				for i in range(len(new_values)):
					if new_values[i] != '#':
						self.value = '0' + new_values[i]
						self.send()
				self.value = new_value
			elif self.type == 'status':
				if self.value != new_value:
					self.value = new_value
					self.send()
			elif self.type == 'variable':
				self.value = new_value
			else:
				self.log.error('Wrong tag!\n')
				sys.stderr.write('Wrong tag!\n')
		except:
			sys.stderr.write("Can't handle value("+self.name+", "+str(new_value)+")\n")
			self.log.error("Can't handle value("+self.name+", "+str(new_value)+")\n")

class DataHandler:
	def __init__(self, tags, log):
		self.tags = tags
		self.log = log
		
	def handle(self, data):
		for i in range(len(data)):
			for j in range(len(self.tags)):
				if data[i][0] == self.tags[j].name:
					self.tags[j].handleValue(data[i][1])

	def getTagValue(self, id):
		for i in range(len(self.tags)):
			if self.tags[i].id == id:
				return self.tags[i].value
		return None

	def getTagName(self, id):
		for i in range(len(self.tags)):
			if self.tags[i].id == id:
				return self.tags[i].name
		return None

	def timeInterval(self):
		for i in range(len(self.tags)):
			if self.tags[i].type == 'variable':
				if self.tags[i].intervalCounter < self.tags[i].interval:
					self.tags[i].intervalCounter += self.tags[i].intervalStep
				else:
					self.tags[i].intervalCounter = 0
					self.tags[i].send()

class SerialHandler:
	def __init__(self, serialPort, log):
		self.serialPort = serialPort
		self.log = log
		self.socket = Socket(('127.0.0.1', 5123), log)

	def handle(self):
		try:
			return True
		except:
			return False

	def send(self, led, val):
		try:
                         pass
			#cmd = self.serialPort + "\t" + str(led) + "\t" + str(val)
			#self.socket.send(cmd)
		except Exception as e:
                        pass
			#print (str(e))

	def onYellow(self):
		self.send(1, 1)

	def offYellow(self):
		self.send(1, 0)

	def onGreen(self):
		self.send(0, 1)

	def offGreen(self):
		self.send(0, 0)
