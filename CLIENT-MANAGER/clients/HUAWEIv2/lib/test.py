import sys, os 
from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto import rfc1902

'''
	Гэрээнд заагдсан авах утгууд

Load current (тухайн агшины гүйдэл)-12%
Battery current (тухайн агшины батерейн гүйдэл) - 12%
Out voltage (гаралтын хүчдэл)	- 12%
Mains voltage (оролтын хүчдэл)	- 12%
Setting цэсний тохиргоонуудыг харах болон өөрчлөх боломжтой - 35%
Аларм утгуудыг төврүү дамжуулах боломжтой - 12%
Battery discharge тестыг автоматаар хийх боломжтой байх -5%

Захиалагчын шаардалгад нийцсэн нэмэлт 5 хүртэлхи боломжуудыг санаачлан оруулсан тохиолдолд давуу талууд үүсэнэ.

'''

hwApOrAblVoltage = '1.3.6.1.4.1.2011.6.164.1.5.2.1.1.4.1' # Phase A Voltage
hwBpOrBclVoltage = '1.3.6.1.4.1.2011.6.164.1.5.2.1.1.5.1' # Phase B Voltage
hwCpOrCalVoltage = '1.3.6.1.4.1.2011.6.164.1.5.2.1.1.6.1' # Phase C Voltage
# DC Outputs
hwDcOutputVoltage = '1.3.6.1.4.1.2011.6.164.1.6.2.1.1.4.1' # DC Output Voltage
hwDcOutputCurrent = '1.3.6.1.4.1.2011.6.164.1.6.2.1.1.5.1' # DC Output Current
# Battery Get
hwBattsTemprature1 = '1.3.6.1.4.1.2011.6.164.1.4.2.5.0'
hwBattsChargeStatus = '1.3.6.1.4.1.2011.6.164.1.4.2.3.0'
hwBattsTotalCurrent = '1.3.6.1.4.1.2011.6.164.1.4.2.1.0' # 

# Test result 
hwBattsDischargeEndVoltage = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.7.1' # Unsigned 0-60V
hwBattsDischargeAvCurrent = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.8.1'
hwBattsDischargeAhValue = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.9.1'

hwRectOperStatus1 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.99.1'
hwRectOperStatus2 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.99.2'
hwRectOperStatus3 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.99.3'
hwRectOperStatus4 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.99.4'
hwRectOperStatus5 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.99.5'
hwRectOperStatus6 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.99.6'

hwBattsDischargeAhValue = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.9.1'
hwCtrlBattsTestStartStop = '1.3.6.1.4.1.2011.6.164.1.4.3.7.0'
hwSetBattsManuShortTestTime = '1.3.6.1.4.1.2011.6.164.1.4.3.5.0' # Unsigned32 1-20 minute

hwBattsTestType = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.2.1'
hwBattsTestStartTime = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.3.1'
hwBattsTestResult = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.4.1'

hwBattsDischargeAvCurrent = '1.3.6.1.4.1.2011.6.164.1.4.3.100.1.8.1'

# Battery Set
hwCtrlBattsBoostFloat = '1.3.6.1.4.1.2011.6.164.1.4.1.12.0' # float 1 boost 2 null Oper 255
hwSetBattsFloatVoltage = '1.3.6.1.4.1.2011.6.164.1.4.1.8.0'
hwSetBattsBoostVoltage = '1.3.6.1.4.1.2011.6.164.1.4.1.9.0'

# Rectifier
hwRectsTotalCurrent = '1.3.6.1.4.1.2011.6.164.1.3.1.3.0'
hwRectsTotalQuantity = '1.3.6.1.4.1.2011.6.164.1.3.1.4.0'
hwRectsRatedVoltage = '1.3.6.1.4.1.2011.6.164.1.3.1.5.0'
hwBattsPreDischargeTime = '1.3.6.1.4.1.2011.6.164.1.4.3.2.0' # minute (ZASAH)********************* 0.8 

hwRectOutputCurrent1 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.2.1' # Rects individual current
hwRectOutputCurrent2 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.2.2'
hwRectOutputCurrent3 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.2.3'
hwRectOutputCurrent4 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.2.4'
hwRectOutputCurrent5 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.2.5'
hwRectOutputCurrent6 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.2.6'

hwRectOutputVoltage1 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.3.1'
hwRectOutputVoltage2 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.3.2'
hwRectOutputVoltage3 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.3.3'
hwRectOutputVoltage4 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.3.4'
hwRectOutputVoltage5 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.3.5'
hwRectOutputVoltage6 = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.3.6'

hwRectOutputVoltage = '1.3.6.1.4.1.2011.6.164.1.3.2.2.1.3.1-4' # Rects individual voltage

hwCtrlRectsAllOnOff = '1.3.6.1.4.1.2011.6.164.1.3.1.8.0' # Rectifier all on off bolgoh
hwCtrlRectsOnOff = '1.3.6.1.4.1.2011.6.164.1.3.2.1.1.12.1'

alarm_index='1.3.6.1.4.1.2011.6.164.1.1.2.100.1.1.1' # 1 dugger index
alarm_text='1.3.6.1.4.1.2011.6.164.1.1.2.100.1.2.1'  # 1 dugeer alarm text string 2bol No responsse 
alarm_level='1.3.6.1.4.1.2011.6.164.1.1.2.100.1.3.1' # alarm level
alarm_sig_desc='1.3.6.1.4.1.2011.6.164.1.1.2.100.1.4.1' # alarm signal description

alarm_row_status='1.3.6.1.4.1.2011.6.164.1.1.2.100.1.100' # alarm row status
#"1.3.6.1.4.1.2011.6.164.1.3.2.2.1.100"
# shalgah="(MibVariable(ObjectName(1.3.6.1.4.1.2011.6.164.1.1.2.2)), NoSuchInstance('b'''))"

class DataManager:
	def __init__(self, log):
		self.log = log
	def init(self, serialPort):
		return True
			#try:
			#self.ser = serial.Serial(port=serialPort, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0)
		#return True
		#except:
			#self.log.error("Can't enable serial port\n")
			#sys.stderr.write("Can't enable serial port\n")
			#return False
	def toStr(self,var):
		try:
			b = int(var)
			data=''
			data=str((b)/10)
			return data
		except:
			return "N/A"

	def SplitData(self, data, num):
		ret = ""
		strdata = str(data) # hex data-g string ruu hurvuulne
		sdata = strdata.split(", ") # string data . - eer ni 2 salgana
		ret = sdata[num-1]
		return ret

	def readFromRectifier(self):
		print("GET START")
		cmdGen = cmdgen.CommandGenerator()
		print("AAA")
		errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
			cmdgen.CommunityData('sread'),
			cmdgen.UdpTransportTarget(('192.168.0.10', 161)),
			hwApOrAblVoltage,hwBpOrBclVoltage,hwCpOrCalVoltage, # (0-2)
			hwDcOutputVoltage,hwDcOutputCurrent, # (3-4)
			hwBattsTemprature1,hwBattsChargeStatus,hwBattsTotalCurrent,	# (5-7)
			hwSetBattsFloatVoltage,hwSetBattsBoostVoltage, # (8-9)
			hwRectOutputCurrent1,hwRectOutputCurrent2,hwRectOutputCurrent3,hwRectOutputCurrent4,hwRectOutputCurrent5,hwRectOutputCurrent6, # (10-15)
			hwBattsDischargeAhValue,hwRectsTotalQuantity, # (16-17)
			hwRectOperStatus1,hwRectOperStatus2,hwRectOperStatus3,hwRectOperStatus4,hwRectOperStatus5,hwRectOperStatus6, # (18-23)
			hwRectOutputVoltage1,hwRectOutputVoltage2,hwRectOutputVoltage3,hwRectOutputVoltage4,hwRectOutputVoltage5,hwRectOutputVoltage6, # (24-29)
			hwBattsTestType,hwBattsTestStartTime,hwBattsTestResult,hwBattsDischargeEndVoltage,hwBattsDischargeAvCurrent,hwBattsPreDischargeTime,hwSetBattsManuShortTestTime,hwCtrlBattsTestStartStop, # (31-37)
			hwCtrlRectsAllOnOff, # 38
			lookupNames=True, lookupValues=True
		)
		print("Three Phase ")
		name, value = varBinds[0]
		name, value1 = varBinds[1]
		name, value2 = varBinds[2]
		var = ""	
		var += (self.toStr(value) + ", " +  self.toStr(value1) + ", "+ self.toStr(value2) + ", ") # Three phase's values
		print("DC out voltage and current")
		name, value3 = varBinds[3] # hwDcOutputVoltage
		name, value4 = varBinds[4] # hwDcOutputCurrent
		var += (self.toStr(value3) + ", " + self.toStr(value4) + ", ")	# DC out voltage and current
		print("Batss status")
		name, value5 = varBinds[5] # hwBattsTemprature1
		name, value6 = varBinds[6] # hwBattsChargeStatus
		name, value7 = varBinds[7] # hwBattsTotalCurrent
		name, value8 = varBinds[8] # SetBattsFloatVoltage
		name, value9 = varBinds[9] # SetBattsBoostVoltage
		var += (self.toStr(value5) + ", " + str(value6) + ", " + self.toStr(value7) + ", " + self.toStr(value8) + ", " + self.toStr(value9) + ", ")
		print("SMRI Currents 1-6")
		name, value10 = varBinds[10] # SMRI 1
		name, value11 = varBinds[11] # SMRI 2
		name, value12 = varBinds[12] # SMRI 3
		name, value13 = varBinds[13] # SMRI 4
		name, value14 = varBinds[14] # SMRI 5
		name, value15 = varBinds[15] # SMRI 6 
		var += (self.toStr(value10) + ", " + self.toStr(value11) + ", " + self.toStr(value12) + ", " + self.toStr(value13) + ", " + self.toStr(value14) + ", " + self.toStr(value15) + ", ")
		name, value16 = varBinds[16] # disCharge Ah
		name, value17 = varBinds[17] # Rects Pcs
		name, value18 = varBinds[18] # Rect Oper Status 1 to
		name, value19 = varBinds[19]
		name, value20 = varBinds[20]
		name, value21 = varBinds[21]
		name, value22 = varBinds[22]
		name, value23 = varBinds[23] # Rect Oper Status 6 to		
		var += (str(value16) + ", " + str(value17) + ", " + str(value18) + ", " + str(value19) + ", " + str(value20) + ", " + str(value21) + ", " + str(value22) + ", " + str(value23) + ", ")				
		name, value24 = varBinds[24] # RectsOutputVoltage
		name, value25 = varBinds[25]
		name, value26 = varBinds[26]
		name, value27 = varBinds[27]
		name, value28 = varBinds[28]
		name, value29 = varBinds[29]	
		var += (self.toStr(value24) + ", " + self.toStr(value25) + ", " + self.toStr(value26) + ", " + self.toStr(value27) + ", " + self.toStr(value28) + ", " + self.toStr(value29) + ", ")
		name, value30 = varBinds[30] # Integer(1-Manual Test) (2-acOffAutoTest) (3-timedTest) (4-shortTest) (255-other)
		name, value31 = varBinds[31] # Start time
		name, value32 = varBinds[32]
		name, value33 = varBinds[33] # Unsigned32 String Discharge End Voltage
		name, value34 = varBinds[34]
		name, value35 = varBinds[35]
		name, value36 = varBinds[36]
		name, value37 = varBinds[37]
		name, value38 = varBinds[38]	
		var += (str(value30) + ", " + str(value31) + ", " + str(value32) + ", " + self.toStr(value33) + ", " + self.toStr(value34) + ", " + str(value35) + ", " + str(value36) + ", " + str(value37) + ", " + str(value38) + ", ")
		ret = var		
		#ret = value.prettyPrint()
		print("DC-OUT Voltage",value21)
		print("DC-OUT Current",value23)
		return ret
		#except:
			#return 'GET:CONNECTION ERROR'

	def setBattFCHV(self,value):
		try:
			print('Enter here')	
			cmdGen = cmdgen.CommandGenerator()
			errorIndication, errorStatus, errorIndex, varBinds = cmdGen.setCmd(
				cmdgen.CommunityData('swrite'),
				cmdgen.UdpTransportTarget(('192.168.0.10', 161)),
				(hwCtrlBattsBoostFloat, rfc1902.Integer(1)),
				(hwSetBattsFloatVoltage, rfc1902.Unsigned32(value)),
				lookupNames=True, lookupValues=True
			)			
			print(rfc1902.Unsigned32(value))
			name, ret  = varBinds[0]
			name, ret1 = varBinds[1]
			print(ret,'|||||',ret1)
			if ret1 == value:
				return 'OK'
			else:
				return 'XE01'
		except:
			return "XE05"


	def setBattBCHV(self, value):	
		try:
			cmdGen = cmdgen.CommandGenerator()
			errorIndication, errorStatus, errorIndex, varBinds = cmdGen.setCmd(
				cmdgen.CommunityData('swrite'),
				cmdgen.UdpTransportTarget(('192.168.0.10', 161)),
				(hwCtrlBattsBoostFloat, rfc1902.Integer(2)),
				(hwSetBattsBoostVoltage, rfc1902.Unsigned32(value)),
				lookupNames=True, lookupValues=True
			)
			name, ret  = varBinds[0]
			name, ret1 = varBinds[1]
			if ret1 -- value:
				return 'OK'
			else:
				return 'XE01'
		except:
			return 'XE05'
	def setCtrlRectsAll(self, set_value):	
		try:
			cmdGen = cmdgen.CommandGenerator()
			errorIndication, errorStatus, errorIndex, varBinds = cmdGen.setCmd(
				cmdgen.CommunityData('swrite'),
				cmdgen.UdpTransportTarget(('192.168.0.10', 161)),
				(hwCtrlRectsAllOnOff, rfc1902.Integer(set_value)),
				lookupNames=True, lookupValues=True
			)
			name, ret  = varBinds[0]
			if ret -- set_value:
				return 'OK'
			else:
				return 'XE01'
		except:
			return 'XE05'
	def setCtrlBattsTestStartStop(self, set_value):	
		try:
			cmdGen = cmdgen.CommandGenerator()
			errorIndication, errorStatus, errorIndex, varBinds = cmdGen.setCmd(
				cmdgen.CommunityData('swrite'),
				cmdgen.UdpTransportTarget(('192.168.0.10', 161)),
				(hwCtrlBattsTestStartStop, rfc1902.Integer(set_value)),
				lookupNames=True, lookupValues=True
			)
			name, ret  = varBinds[0]
			if ret -- set_value:
				return 'OK'
			else:
				return 'XE01'
		except:
			return 'XE05'
	
	def set(self, tag, new_value):
		if tag == "fchv":
			set_value = float(new_value)*10
			print("FCHV - ", set_value)
			return self.setBattFCHV(set_value)
		elif tag=="bchv":
			set_value = float(new_value)*10
			return self.setBattBCHV(set_value)
		elif tag=="batTest":
			return self.setCtrlBattsTestStartStop(new_value)
		elif tag=="ctrlRectsAll":
			return self.setCtrlRectsAll(new_value)
		
	def getAlarm(self):
		#try:
			print("GET---------ALARM")
			cmdGen = cmdgen.CommandGenerator()
			errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
			cmdgen.CommunityData('sread'),
			cmdgen.UdpTransportTarget(('192.168.0.10', 161)),
                        alarm_row_status,
			#alarm_index, alarm_text, alarm_level, alarm_sig_desc, #alarm_row_status,
			lookupNames=True, lookupValues=True
			)
			alarmi, index=varBinds[0]
			#alarmt, text=varBinds[1]
			#alarml, level=varBinds[2]
			#alarms, desc=varBinds[3]
			#alarmr, alarm=varBinds[4]
			#name.prettyPrint()
			print("Alarm Index:  " + str(alarmi.prettyPrint()) + " -- " + str(index))
			#print("Alarm Text:  " + str(alarmt.prettyPrint()) + " -- " + str(text))
			#print("Alarm Level:  " + str(alarml.prettyPrint()) + " -- " + str(level))
			#print("Alarm Desc:  " + str(alarms.prettyPrint()) + " -- " + str(desc))
			#print("Alarm RowAlarm:  " + str(alarmr.prettyPrint()) + " -- " + str(alarm))
			return  
			#return 'Alarm 1|Alarm 2|Alarm 3'
		#except:
		#	self.log.error("Can't get alarm!")
		#	return 'XE05'

	def getData(self):
		ans=self.readFromRectifier()		
		ret = []
		ret.append(('acina', self.SplitData(ans,1)))
		ret.append(('acinb', self.SplitData(ans,2)))
		ret.append(('acinc', self.SplitData(ans,3)))
		ret.append(('dcouVol', self.SplitData(ans,4)))
		ret.append(('dcouCur', self.SplitData(ans,5)))
		ret.append(('batt1', self.SplitData(ans,6)))
		ret.append(('battchst', self.SplitData(ans,7)))
		ret.append(('batc1', self.SplitData(ans,8)))
		ret.append(('fchv', self.SplitData(ans,9)))
		ret.append(('bchv', self.SplitData(ans,10)))
		
		ret.append(('smri1', self.SplitData(ans,11)))
		ret.append(('smri2', self.SplitData(ans,12)))
		ret.append(('smri3', self.SplitData(ans,13)))
		ret.append(('smri4', self.SplitData(ans,14)))
		ret.append(('smri5', self.SplitData(ans,15)))
		ret.append(('smri6', self.SplitData(ans,16)))
				
		ret.append(('battdAh', self.SplitData(ans,17)))
		ret.append(('rectstq', self.SplitData(ans,18)))

		#ret.append(('battest))
		ret.append(('rectops1', self.SplitData(ans,19)))
		ret.append(('rectops2', self.SplitData(ans,20)))
		ret.append(('rectops3', self.SplitData(ans,21)))
		ret.append(('rectops4', self.SplitData(ans,22)))
		ret.append(('rectops5', self.SplitData(ans,23)))
		ret.append(('rectops6', self.SplitData(ans,24)))
		# Rectifier modules output voltage
		ret.append(('smrv1', self.SplitData(ans,25)))
		ret.append(('smrv2', self.SplitData(ans,26)))
		ret.append(('smrv3', self.SplitData(ans,27)))
		ret.append(('smrv4', self.SplitData(ans,28)))
		ret.append(('smrv5', self.SplitData(ans,29)))
		ret.append(('smrv6', self.SplitData(ans,30)))
		# Battery test results
		ret.append(('battTestType', self.SplitData(ans,31)))
		ret.append(('battTestStartTime', self.SplitData(ans,32)))
		ret.append(('battTestResult', self.SplitData(ans,33)))
		ret.append(('batDisEndVol', self.SplitData(ans,34)))
		ret.append(('batDisAvCur', self.SplitData(ans,35)))
		ret.append(('batDisTime', self.SplitData(ans,36)))
		ret.append(('batManuTestTime', self.SplitData(ans,37)))
		ret.append(('batTest', self.SplitData(ans,38)))
		ret.append(('ctrlRectsAll', self.SplitData(ans,39)))
		print (ret)
		print("***********READ END*************")
		return ret
		
dataManager = DataManager(None)

#dataManager.init('/dev/ttyUSB0')

#print(dataManager.setAddr(12))

#print(dataManager.set('fchv', 54))

print(dataManager.getAlarm())


