import socket
import sys
import serial
import array

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

host = ('127.0.0.1', 5123)
sock.bind(host)
sock.listen(10)

ports = ['/dev/ttyUSB0', '/dev/ttyUSB1', '/dev/ttyUSB2', '/dev/ttyUSB3', '/dev/ttyUSB4', '/dev/ttyUSB5', '/dev/ttyUSB6', '/dev/ttyUSB7', '/dev/ttyUSB8', '/dev/ttyUSB9', '/dev/ttyUSB10', '/dev/ttyUSB11', '/dev/ttyUSB11']

print ("Handling...")

mySerialPort = '/dev/ttyUSB5'
ser = serial.Serial(port=mySerialPort, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS,timeout=1)

i = 0
run = True
while not ser.isOpen():
	try:
		ser.open()
		break
	except:
		sleep(0.01)
	i = i + 1
	if i > 10:
		run = False
		break

if run:
	while True:
		connection, client_address = sock.accept()
		data = connection.recv(16).decode().strip()

		arr = data.split("\t")
		for i in range(len(arr)):
			arr[i] = arr[i].strip()

		serialIndex = -1
		for i in range(len(ports)):
			if arr[0] == ports[i]:
				serialIndex = i
				break

		led = int(arr[1])
		val = int(arr[2])

		if serialIndex >= 0:
			cmd = []
			cmd.append(0xF0)
			cmd.append(serialIndex)
			cmd.append(led * 2 + val)
			ser.flushOutput()
			ser.flushInput()
			temp = array.array('B', cmd).tostring()
			ser.write(temp)
		connection.send('U'.encode())
		connection.close()
else:
	print ("Can't open serial port!")
