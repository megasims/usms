Language: python3

1. APP/CLIENT-MANAGER/clients/ дотор өөрийн client програмын нэрийг том үсгээр нэрлэн (шидэт тэмдэгт ашиглалгүйгээр) үүсгэнэ

2. EXAMPLE програмын хуулж ашиглаж болно.

3. YOUR_CLIENT/lib/data.py Энэ бол таны өөрчлөлт хийх үндсэн файл. Өөр файлыг өөрилж болохгүйг анхаарна уу.

4. DataManager класын getData функцийг тодорхой хугацааны завсартайгаар байнга дуудана.
	Буцах утган нь [('tag1', value1), ('tag2', value2), ('alarm', 'alarm-1|alarm-2|alarm-3')]
	хэлбэртэй байна.

5. DataManager класын init() функцийг програм эхлэхэд нэг удаа дуудах ба хэрэв False утга буцаавал програмыг зогсооно.
	Шаардлагатай initial үйлдлүүдийг энд хийвэл зохино.
	Шаардлагатай нэмэлт параметр хэрэгтэй бол osohoo02@gmail.com
	Шаардлагатай нэмэлт package уудыг data.py дээр импорт хийж ашиглаж болно.

6. Програм ажиллаж эхлэх нөхцлийг шалгах шаардлагатай бол YOUR_CLIENT/lib/decorator.py
	файл дээрх decorate функц дээр хэрэгжүүлж болно. Хэрэв False утга буцаавал програмыг ажиллуулахгүй.

7. self.log.error(string), self.log.warning(string), self.log.info(string) эдгээр функцуудыг ашиглаж log хөтлөх шаардлагатай.

8. Try, Except ийг заавал ашиглах шаардлагатай.

9. Нэмэлт мэдээлэл хэрэгтэй бол osohoo02@gmail.com

10. Happy coding :D

11. TEST
	1. python3 test-manager.py
	2. python3 test-worker.py
	3. python3 CLIENT-MANAGER
		start.py
		stop.py


# HOW TO GIT лэх вэ?

git status
  Таны локал дээрх өөрчлөлтүүдийн жагсаалт

git pull
  Найзуудынхаа өөрлөлтүүдийг локал дээрээ татаж авна
  Энэ үйлдлийг хийхэд conflict үүсч болзошгүйг анхаарна уу?

git add . | git add --all
git commit -m "Your name : Commit description"

git push
  Энэ тохиолдолд сервэр дээр ямар нэгэн өөрчлөлт байвал хийх боломжгүй
  Тиймээс та найзуудынхаа хийсэн өөрлөлтийг татаж авсны дараа буюу
  git pull - ийн дараа энэ үйлдлийг хийх болж дээ

Happy GIT-лээрэй :D



-----------------------------
clients-cfg
127.0.0.1	15001	serialPort
Worker-ip   Worker-port   serial-port(client)

1	       on_off	    status
(tag_id), (tag_name), (tag_type)

2	temp	variable	20
(tag_id), (tag_name), (tag_type), (time_interval)

3	alarm	alarm
(tag_id), (tag_name), (tag_type)



manager-cfg
127.0.0.1	16001
CM_IP, 	    CM_PORT

1	17001	EXAMPLE
ID  CLIENT_PORT, CLIENT_NAME

2	17002	HUAWEI
ID  CLIENT_PORT, CLIENT_NAME


## sudo apt-get install python3-serial


CFG тохируулахдаа Золоогоос асууна