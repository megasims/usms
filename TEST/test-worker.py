import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

host = ('127.0.0.1', 15001)
sock.bind(host)
sock.listen(10)
print ("waiting...")
while True:
	connection, client_address = sock.accept()
	print("From: " + client_address[0], client_address[1])
	data = connection.recv(16)
	print(data.decode())
	connection.send('U'.encode())
	connection.close()

