import socket
import sys

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ('127.0.0.1', 16001)
conn.connect(host)
cmd = 'STOPCLIENT\t3'
conn.send(cmd.encode())
data = conn.recv(64)
data = data.decode()
print(data)
conn.close()
